import React, {createContext, Component} from 'react';

export const ClickContext = createContext();

class ClickContextProvider extends Component {
    constructor(props){
        super(props);
        this.state = {
            forClickId:"",
            /* prevClick:"LMeVYvtYKJNxLKqeTf9v", */
            watchlistIdArray:[{id:"", content:{question:"",description:"", code:"",number:"",modans:"", answers:[""],author:""}}],
            search: null,
            numCompare: ">",
            numSubject: 0,
            page: "next",
            prelimNumSubject:0,
            orderBy: "timestamp",
            textSearch: [""],
            watchlist: false,
            name: [{id:"", content:{question:"",description:"", code:"",number:"",modans:[""], answers:[""],author:""}}],
            uSubjects: localStorage.getItem("localSubjects"),
            showqformButton: false,
            showSearchbar: false,
            subjectCode:"",
            edit: false
        }
    }

    click =(a)=>{
        this.setState({forClickId : a})
    }

    setEdit =(a)=>{
        this.setState({edit : a})
    }
    

    updateWatchlistArray = (a)=>{
        this.setState((state,props) => {
            return {watchlistIdArray: [...a]}
        });
    }

    updateUSubjects = (a)=>{
        this.setState((state,props) => {
            return {uSubjects: [...a]}
        });
    }

    setShowSearchbar=(a)=>{
        this.setState({showSearchbar: a})
    }


    setSubjectCode=(a)=>{
        this.setState({subjectCode: a})
    }

    setShowqformButton=(a)=>{
        this.setState({showqformButton: a})
    }

    setSearch = (a)=>{
        this.setState({search: a})
    }

    setPage = (a)=>{
        this.setState({page:a})
    }

    resetCount = ()=>{
        this.setState({count : 0})
    }

    setNumCompare = (a)=>{
        this.setState({numCompare: a})
    }

    setNumSubject = (a)=>{
        this.setState({numSubject: a})
    }

    setPrelimNumSubject = (a)=>{
        this.setState({prelimNumSubject: a})
    }

    setOrderBy = (a)=>{
        this.setState({orderBy: a})
    }

    setTextSearch = (a)=>{
        this.setState({textSearch:a})
    }

    setWatchlist = (a)=>{
        this.setState({watchlist:a})
    }

    setName = (a)=>{
        this.setState({name:a})
    }

    render(){
        return(
            <ClickContext.Provider value={{...this.state,setEdit:this.setEdit,setShowSearchbar:this.setShowSearchbar,setSubjectCode:this.setSubjectCode,setShowqformButton:this.setShowqformButton,updateUSubjects:this.updateUSubjects,setPrelimNumSubject:this.setPrelimNumSubject, setName:this.setName,setWatchlist:this.setWatchlist, setTextSearch:this.setTextSearch,setOrderBy:this.setOrderBy,setNumSubject:this.setNumSubject,setNumCompare:this.setNumCompare,setPage:this.setPage,/* resetCount: this.resetCount,setCount: this.setCount,setPage:this.setPage, setCursor: this.setCursor, */ setSearch: this.setSearch, updateWatchlistArray:this.updateWatchlistArray, 
            click:this.click}}>
                {this.props.children}
            </ClickContext.Provider>
        );
    }
}

export default ClickContextProvider;
