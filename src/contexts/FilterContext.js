import React, {createContext, Component} from 'react';

export const FilterContext = createContext();

class FilterContextProvider extends Component {
    constructor(props){
        super(props);
        this.state = {
            cursor: new Date(32534611200*1000)            , //0,
            page: "next",
            count: 0,
        }
    }

    setCursor = (a) =>{
        this.setState({cursor:a})
    }

    setPage = (a)=>{
        this.setState({page:a})
    }

    setCount = (a)=>{
        this.setState({count : this.state.count+a})
    }
    
    render(){
        return(
            <FilterContext.Provider value={{...this.state, resetCount: this.resetCount,setCount: this.setCount,setPage:this.setPage, setCursor: this.setCursor,}}>
                {this.props.children}
            </FilterContext.Provider>
        );
    }
}

export default FilterContextProvider;