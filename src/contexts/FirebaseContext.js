import React, {createContext, Component} from 'react';
import * as firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/analytics";

export const FirebaseContext = createContext();

const firebaseConfig = {
    apiKey: "AIzaSyARabhlp2WV2yixTi1QnGfsHKdn6JkDKAQ",
    authDomain: "scholarbuddy-29706.firebaseapp.com",
    databaseURL: "https://scholarbuddy-29706.firebaseio.com",
    projectId: "scholarbuddy-29706",
    storageBucket: "scholarbuddy-29706.appspot.com",
    messagingSenderId: "1070772831807",
    appId: "1:1070772831807:web:bb8eb56c6bf0820d875a8a",
    measurementId: "G-Z4G3RVH16B"
  };


class FirebaseContextProvider extends Component {
    constructor(props){
    super(props);
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }
    this.auth = firebase.auth();
    this.store = firebase.firestore();
    this.functions = firebase.functions();
    this.analytics=firebase.analytics();
    this.fieldValue = firebase.firestore.FieldValue;
    this.timestamp = firebase.firestore.Timestamp;

    this.state= {
        status: false,
        displayname: "Login with your OBF",
        admin: false,
        tutor: false,
        moderator: false,
        org: "",
        name: "",
        uid: "c"
    }
    }


    arrayUnion=(input)=> {
        firebase.firestore.FieldValue.arrayUnion(input);
    };

    arrayRemove=(input)=> {
        firebase.firestore.FieldValue.arrayRemove(input);
    };

    serverTimestamp=()=> {
        firebase.firestore.FieldValue.serverTimestamp();
    };

    watch = ()=>{
        this.auth.onAuthStateChanged(
        (user) => {
        if (user) {  
            this.setState( {status : true});
            //console.log("Watch status?",this.state.status) 
        }
        else {
            this.setState( {status : false});
            //console.log("Watch status?",this.state.status);
        }
         });
             }   

    signout= async(id) =>{
    this.setState({
        status: false,
        displayname: "Login with your OBF",
        admin:false,
        tutor: false,
        moderator: false,
        org: "",
        name: ""
    });
    await firebase.firestore().collection("users").doc(id).update({
        lastLoggedIn: firebase.firestore.Timestamp.now()
    });
    this.auth.signOut();
    localStorage.clear();
    sessionStorage.clear();
    console.log("User signs out");
    }
                    
                  
    signin= async () =>{
    var provider = new firebase.auth.GoogleAuthProvider();
    try{
    const a = await firebase.auth().signInWithPopup(provider);
    const capitalize = (str, lower = false) =>
    (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());
    ;


    a.user.getIdTokenResult().then(idTokenResult=>{
        a.user.admin = idTokenResult.claims.admin;
        this.setState({admin: a.user.admin});
    });


    const b =capitalize(a.user.displayName,true);
    this.setState( {displayname : b});
    
    }catch (error){
        if (error.code == "auth/user-disabled"){
            alert("You no longer have access to scholarbuddy. Contact Gabby the Scholar to extend your account.");
        }
        else if (error.code == "auth/popup-closed-by-user"){
            alert("Login failed because you closed the pop-up window.");
        }
        else{
            alert("Error:", error);
        }
    }

    }

    adminset =()=>{
        this.setState({
            admin:true,
        })
    }

    modset =(name)=>{
        this.setState({
            moderator:true,
            name: name
        })
    }

    tutorset =(org, name)=>{
        this.setState({
            tutor:true,
            org: org,
            name: name
        })
    }

    setUid = (uid)=>{
        this.setState({
            uid:uid
        })
    }


    render(){
        return(
            <FirebaseContext.Provider value={{...this.state,Timestamp:this.timestamp, fieldValue:this.fieldValue,serverTimestamp:this.serverTimestamp,arrayRemove:this.arrayRemove,arrayUnion:this.arrayUnion,setUid:this.setUid,tutorset:this.tutorset,modset:this.modset,adminset:this.adminset, auth:this.auth,functions:this.functions,store: this.store, watch: this.watch, signout:this.signout, signin: this.signin}}>
                {this.props.children}
            </FirebaseContext.Provider>
        );
    }
}

export default FirebaseContextProvider;