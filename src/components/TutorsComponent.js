import React, {useState,useContext,useEffect} from 'react';
import Topbar from './TopbarComponent.js';
import FooterNew from './FooterNewComponent';
import {Sidebar, NotableQuestions} from './ForumNewComponent';

import './styles/forum.css';
import './styles/tutors.css';

import pic from './img/profilepic.png';


export default function Tutors (){
    return(
        <>
            <Topbar/>
                <div className="forum-wrapper">
                    <Sidebar/>
                    <div className="tutors-wrapper">
                        <div className="tutors-heading">Tutors</div>
                        <div className="tutors-description">
                            You can ask for further help from these volunteer tutors. Don’t forget<br/>to be respectful when chatting with them.
                        </div>
                        <TutorCore/>
                </div>
                    <NotableQuestions/>
                </div>
            <FooterNew/>
        </>
    )
}

function TutorCore (){
    const array = Array.from(Array(5).keys())

    const tutors = array.map((element,i)=>
  
            <div className="tutorcore-entry">
                <div className="tutorcore-entry-row-1">
                    <img className="tutorcore-pic" src={pic}/>
                    <div className="tutorcore-name-wrapper">
                        <div className="tutorcore-name">I am Tutor</div>
                        <div className="tutorcore-course">BS CS</div>
                        <div className="tutorcore-name-ratings-wrapper">
                        <svg width="120" height="23" viewBox="0 0 120 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.5 0L14.0819 7.9463H22.4371L15.6776 12.8574L18.2595 20.8037L11.5 15.8926L4.74047 20.8037L7.32238 12.8574L0.56285 7.9463H8.91809L11.5 0Z" fill="#EBC74E"/>
                            <path d="M38.5 0L41.0819 7.9463H49.4371L42.6776 12.8574L45.2595 20.8037L38.5 15.8926L31.7405 20.8037L34.3224 12.8574L27.5628 7.9463H35.9181L38.5 0Z" fill="#EBC74E"/>
                            <path d="M61.5 0L64.0819 7.9463H72.4371L65.6776 12.8574L68.2595 20.8037L61.5 15.8926L54.7405 20.8037L57.3224 12.8574L50.5628 7.9463H58.9181L61.5 0Z" fill="#EBC74E"/>
                            <path d="M85.5 0L88.0819 7.9463H96.4371L89.6776 12.8574L92.2595 20.8037L85.5 15.8926L78.7405 20.8037L81.3224 12.8574L74.5628 7.9463H82.9181L85.5 0Z" fill="#EBC74E"/>
                            <path d="M108.5 0L111.082 7.9463H119.437L112.678 12.8574L115.26 20.8037L108.5 15.8926L101.74 20.8037L104.322 12.8574L97.5628 7.9463H105.918L108.5 0Z" fill="#EBC74E"/>
                        </svg>
                        <div className="tutorcore-ratings">20 Ratings</div>
                        </div>
                    </div>
                    <div className="tutorcore-menu-wrapper">
                        <div>
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M26.667 2.66667H5.33366C3.86699 2.66667 2.66699 3.86667 2.66699 5.33334V29.3333L8.00033 24H26.667C28.1337 24 29.3337 22.8 29.3337 21.3333V5.33334C29.3337 3.86667 28.1337 2.66667 26.667 2.66667Z" fill="#161716"/>
                            </svg>
                        </div>
                        <div>
                            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.667 4H9.33366C7.86699 4 6.66699 5.2 6.66699 6.66667V28L16.0003 24L25.3337 28V6.66667C25.3337 5.2 24.1337 4 22.667 4ZM22.667 24L16.0003 21.0933L9.33366 24V8C9.33366 7.26667 9.93366 6.66667 10.667 6.66667H21.3337C22.067 6.66667 22.667 7.26667 22.667 8V24Z" fill="#161716"/>
                            </svg>
                        </div>
                    </div>
                </div>
                <div className="tutorcore-entry-row-2">
                    <div className={`tutorcore-subject subject-color1`}>MATH21</div>
                    <div className="tutorcore-subject subject-color2">FILI11</div>
                    <div className="tutorcore-subject subject-color3">THEO11</div>
                    <div className="tutorcore-subject subject-color1">MATH30.23</div>
                </div>
                <div className="tutorcore-entry-row-3">
                    <div></div>
                    <div className="tutorcore-buttons-wrapper">
                        <div className="tutorcore-buttons-availability">Check Availability</div>
                        <div className="tutorcore-buttons-session">Schedule a Session</div>
                    </div>
                </div>
            </div>

    )

        return(
            <>      
                <div className="tutorcore-wrapper">
                    {tutors}
                </div>
            </>

        )
}


/*                     <div className={`tutorcore-subject ${i<3?`subject-color${i+1}`:`subject-color${((i+1)%3)?((i+1)%3):3}`}`}>MATH21</div>
 */