import React, {useContext,useEffect,useState,useRef} from 'react';

import logo from './img/topbar logo.png';
import{FirebaseContext} from '../contexts/FirebaseContext';
import Latex from 'react-latex';
import { Link } from 'react-router-dom';



import bag from './img/bag.png';
import book from './img/book.png';
import headphones from './img/headphones.png';
import pc from './img/pc.png';

import './styles/account.css';
import './styles/forum.css';
import './styles/topbar.css';

export default function Topbar(){

    const fire = useContext(FirebaseContext);
    const [name, setName]=useState("");
    const [pict,setPict] = useState([bag,book,headphones,pc]);
    const [showNotif, setShowNotif] = useState(false);
    const [subj,setSubj] = useState([]);
    const [ans,setAns] = useState([]);
    const [notif,setNotif] = useState([]);
    const [notifRef, setNotifRef] = useState(fire.Timestamp.now().seconds);
    const wrapperRef = useRef(null);

    useEffect(() => {
        window.addEventListener("mousedown", handleClickOutside);
        return () => {
          window.removeEventListener("mousedown", handleClickOutside);
        };
      });
    
      const handleClickOutside = event => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
          setShowNotif(false);
        }
      };


    useEffect(()=>{
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        const unsubscribe=docQuery.onSnapshot(querySnapshot => { 
            setName(querySnapshot.data());
        }
        )
        return unsubscribe
        }
    
    },[fire.status]);

    function updateNotif (){
        subj.filter(y=>(y.content.user!==fire.auth.currentUser.uid)&&y.content.timestamp>=(name.lastLoggedIn?name.lastLoggedIn:fire.Timestamp.now())).forEach(y=>fire.store.collection("users").doc(fire.auth.currentUser.uid).collection("notifs").doc(y.id).set(y.content));
        ans.filter(y=>y.content.timestamp.seconds>=notifRef||y.content.timestamp.seconds>=(name.lastLoggedIn?name.lastLoggedIn.seconds:fire.Timestamp.now().seconds)).forEach(y=>fire.store.collection("users").doc(fire.auth.currentUser.uid).collection("notifs").doc(y.id).set(y.content));
    }

    useEffect(()=>{
        if (subj.filter(y=>(y.content.user!==fire.auth.currentUser.uid)&&y.content.timestamp>=(name.lastLoggedIn?name.lastLoggedIn:fire.Timestamp.now())).length || ans.filter(y=>y.content.timestamp>=(name.lastLoggedIn?name.lastLoggedIn:fire.Timestamp.now())).length){
            updateNotif();
        }    
    },[subj,ans])

     useEffect(()=>{
        if (fire.status &&name.uSubjects){
                name.uSubjects.forEach(subject=>{
                const afterQuery = fire.store.collection("forum").where("timestamp",">=",name.lastLoggedIn?name.lastLoggedIn:fire.Timestamp.now()).where("subjectCode","==",subject).orderBy("timestamp","desc");
                const unsubscribe = afterQuery.get().then(querySnapshot => {
                    let z= [];
                    querySnapshot.forEach(queryDocumentSnapshot => {
                        z.push({tag:"subjects",id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});
                    });
                    setSubj(b=>b.concat(z));
                }
                )
                return unsubscribe
            })
            
        }

    },[fire.status]) 


/*     useEffect(()=>{
        if (fire.status&&name.uQuestions){
            const afterQuery = fire.store.collectionGroup("answers").where("questionId","in",name.uQuestions);
            const unsubscribe = afterQuery.onSnapshot(querySnapshot => {
                let z= [];
                querySnapshot.forEach(queryDocumentSnapshot => {       
                        z.push({tag:"questions",id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});
                });
                setAns(b=>b.concat(z.filter(y=>y.content.user!=fire.auth.currentUser.uid)));
            }
            )
    }
    },[fire.status,name.uQuestions]) */


    useEffect(()=>{
        if (fire.status &&name.uQuestions){
                name.uQuestions.forEach(questions=>{
                const afterQuery = fire.store.collectionGroup("answers").where("questionId","==",questions).where("timestamp",">=",name.lastLoggedIn).orderBy("timestamp","desc");
                let z= [];
                const unsubscribe = afterQuery.get().then(querySnapshot => {
                    querySnapshot.docChanges().forEach(queryDocumentSnapshot => {
                        if (queryDocumentSnapshot.type==="added"){
                        z.push({tag:"questions",id:queryDocumentSnapshot.doc.id, content:queryDocumentSnapshot.doc.data()});
                        }
                    });
                    setAns(z);
                }
                )
                return unsubscribe
            }) 
            
        }

    },[fire.status]) 
 
    //console.log("time",subj);//.filter(y=>y.content.user!==fire.auth.currentUser.uid)
   // console.log("time2",ans)
    //console.log("time3",notif);

    //notifs
   useEffect(()=>{
        if (fire.status){
                const afterQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid).collection("notifs").orderBy("timestamp","desc");
                const unsubscribe = afterQuery.onSnapshot(querySnapshot => {
                    let z= [];
                    querySnapshot.forEach(queryDocumentSnapshot => {
                        z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});
                    });
                    setNotif(b=>b.concat(z));
                }
                )
                return unsubscribe
        }
    },[fire.status,fire.store,fire.auth.currentUser.uid])
    
    return(
        <div className="topbar-wrapper">
            <div className="topbar-logo">
                <div></div>
                <Link to={"/"} style={{textDecoration:"none",color:"#000000"}}>
                <img width="204px" height="64px" src={logo}/>
                </Link>
                <div></div>
            </div>
            <div ref={wrapperRef} className="topbar-options">
                <Link to={"/dashboard"} style={{textDecoration:"none",color:"#000000"}}>
                <svg className="topbar-option" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.3335 25.3333V18.6666H18.6668V25.3333C18.6668 26.0666 19.2668 26.6666 20.0001 26.6666H24.0001C24.7335 26.6666 25.3335 26.0666 25.3335 25.3333V16H27.6001C28.2135 16 28.5068 15.24 28.0401 14.84L16.8935 4.79996C16.3868 4.34663 15.6135 4.34663 15.1068 4.79996L3.96013 14.84C3.5068 15.24 3.7868 16 4.40013 16H6.6668V25.3333C6.6668 26.0666 7.2668 26.6666 8.00013 26.6666H12.0001C12.7335 26.6666 13.3335 26.0666 13.3335 25.3333Z" fill="#EBC74E"/>
                </svg>
                </Link>
                <div className="notification-button-wrapper">
                    <svg role="button" onClick={()=>{setShowNotif(!showNotif);setNotifRef(fire.Timestamp.now().seconds);}} className="topbar-option" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M16.0001 29.3333C17.4667 29.3333 18.6667 28.1333 18.6667 26.6666H13.3334C13.3334 28.1333 14.5201 29.3333 16.0001 29.3333ZM24.0001 21.3333V14.6666C24.0001 10.5733 21.8134 7.14665 18.0001 6.23998V5.33331C18.0001 4.22665 17.1067 3.33331 16.0001 3.33331C14.8934 3.33331 14.0001 4.22665 14.0001 5.33331V6.23998C10.1734 7.14665 8.00007 10.56 8.00007 14.6666V21.3333L6.28007 23.0533C5.44007 23.8933 6.02674 25.3333 7.2134 25.3333H24.7734C25.9601 25.3333 26.5601 23.8933 25.7201 23.0533L24.0001 21.3333Z" fill="#EBC74E"/>
                    </svg>
                    <div className="notification-button-number">
                   {subj.filter(y=>y.content.user!==fire.auth.currentUser.uid&&
                   y.content.timestamp.seconds>=notifRef).length+
                   ans.filter(y=>y.content.timestamp.seconds>=notifRef).length}
                    </div>
                </div>
                <div style={{display:showNotif?"flex":"none"}} className="notification-wrapper">
                    {<div className="notification-flow">
                        <div className="notification-heading">
                            Notifications
                            {/* <hr className="notification-divider"/> .sort((a,b)=>a.content.timestamp-b.content.timestamp?-1:1)*/}
                        </div>
                        <div className={`notification-body`}>
                            {Boolean(notif.length)&&
                            notif.map((element,i)=>!element.content.questionId?
                            <Link to={`/forum/${element.content.subjectCode}/${element.id}`} style={{color:"inherit"}}>
                                <div role="button" className="notification-message">
                                    <div className="notification-message-header">New question in {element.content.subjectCode}</div>
                                    <div className="notification-message-body">
                                    {element.content.question}<br/>{(new Date(element.content.timestamp.seconds*1000)).toLocaleString()}
                                    </div>
                                </div>
                            </Link>:
                            <Link to={`/forum/${element.content.subjectCode}/${element.content.questionId}`} style={{color:"inherit"}}>
                                <div role="button" className="notification-message">
                                    <div className="notification-message-header">Your question has been answered</div>
                                    <div className="notification-message-body">
                                    {element.content.question}<br/>{(new Date(element.content.timestamp.seconds*1000)).toLocaleString()}
                                    </div>
                                </div>
                            </Link>
                            )
                            }
                        </div>
                    </div>}
                </div>

                <Link to={"/account"} style={{textDecoration:"none",color:"#000000"}}>
                <div className="topbar-pic-wrapper">
                    <img className="topbar-pic" width="48px" height="48px"src={pict[name.pic]}/>
                </div>
                </Link>
            </div>
        </div>
    )
}