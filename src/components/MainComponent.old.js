import React, {useEffect,useContext, useState} from 'react';
import Home from './HomeComponent';
import Forum from './ForumComponent';
import Longexams from './SlexComponent';
import SubjectSelector from './SubjectSelectorComponent';
import Landing from './LandingComponent'
import {Selection} from './SelectionComponent'
import {Switch,Route,Redirect} from 'react-router-dom';
import {withRouter} from "react-router-dom";
import {FirebaseContext} from '../contexts/FirebaseContext';
import {MyAccount,IndivQnA} from './MyAccountComponent';
import Navbar from './NavbarComponent';
import { ClickContext } from '../contexts/ClickContext';
import Mod from './ModeratorComponent';
import Footer from './FooterComponent';
import FooterNew from './FooterNewComponent';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Typography, Button, Tab,Tabs, CssBaseline, TextField } from '@material-ui/core';
import {MyDashboard} from './MyDashboard';
import MyAnswers from './MyAnswersComponent';
import MyQuestions from './MyQuestionsComponent';
import {ForumNew} from './ForumNewComponent';
import Questions from './QuestionsComponent';
import Exams from './ExamsComponent';
import Tutors from './TutorsComponent';
import Account from './AccountComponent';

import Topbar from './TopbarComponent.js';

  

function Main () {

        const fire = useContext(FirebaseContext);
        useEffect(()=>{
          fire.watch();
          async function yeet () { 
          if (fire.auth.currentUser){
          const alpha = await fire.auth.currentUser.getIdTokenResult().then(idTokenResult=>idTokenResult.claims);
          //console.log("Auth Inspect",alpha);
            if (alpha.admin) { 
                fire.adminset();
                fire.setUid(alpha.user_id);
            } 
            if (alpha.moderator) { 
              fire.modset(alpha.name);
              fire.setUid(alpha.user_id);
            } 
            if (alpha.tutor) { 
              fire.tutorset(alpha.org,alpha.name);
              fire.setUid(alpha.user_id);
            } 
            else{
              fire.setUid(alpha.user_id);
            }
          return alpha;
        }

        else {
          //console.log("Auth Inspect",null);
        }
      }
      yeet();

      },[fire.auth.currentUser])


      const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));
    

    if (matches){
    return (
    <Switch>
{/*         <Route exact path="/" component={Home}/> */}
        <Route exact path="/" component={Landing}/>
        <Route exact path="/selection" component={Selection}/>
        <Route exact path="/dashboard" component={MyDashboard}/>
        <Route exact path="/dashboard/savedquestions" component={MyQuestions}/>
        <Route exact path="/dashboard/savedanswers" component={MyAnswers}/>
        <Route exact path="/forum/:id" component={ForumNew}/>
        <Route exact path="/forum/:id/:qid" component={Questions}/>
        <Route exact path="/exams" component={Exams}/>
        <Route exact path="/tutors" component={Tutors}/>
        <Route exact path="/account" component={Account}/>
        <Route exact path="/mod" component={Mod}/>
        <PrivateRoute path="/ss">
          <SubjectSelector/>
        </PrivateRoute>
        <Route component={Contents}/>
    </Switch>          
    );
  }

  else{
  return(
    <Grid container style={{height:"100vh"}} justify="center" direction="column" alignItems="center">
    <Grid item>
    <svg width="121" height="120" viewBox="0 0 121 120" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M74.2195 50.6853C71.1096 47.5754 68.2195 45.6453 65.1246 44.6152C62.8945 43.8702 61.1196 42.0953 60.3797 39.8703C59.3497 36.7703 57.4198 33.8805 54.3097 30.7755L27.7948 4.26061C22.3098 -1.22443 12.7098 -1.24927 7.19995 4.26061L4.25997 7.20059C1.51499 9.95049 0 13.6055 0 17.5104C0 21.3954 1.51499 25.0505 4.25997 27.7954L30.7749 54.3103C33.8848 57.4202 36.7748 59.3503 39.8697 60.3804C42.0998 61.1254 43.8747 62.9003 44.6146 65.1253C45.6447 68.2202 47.5797 71.1151 50.7796 74.3153C51.9446 75.4853 53.4746 76.0653 55.0046 76.0653C56.5297 76.0653 58.0447 75.4853 59.1996 74.3253L74.3245 59.2005C76.6445 56.8852 76.6445 53.1103 74.2195 50.6853ZM70.7995 55.6702L55.6747 70.795C55.2896 71.17 54.6997 71.1599 54.2347 70.6949C51.6896 68.1498 50.1397 65.8799 49.3646 63.5449C48.1246 59.8298 45.1696 56.8748 41.4548 55.635C39.1249 54.8599 36.8547 53.3149 34.3148 50.775L7.79994 24.2604C5.99503 22.4604 4.99989 20.0604 4.99989 17.4905C4.99989 14.9405 5.9948 12.5406 7.79479 10.7356L10.735 7.80059C12.5399 5.99568 14.935 5.00053 17.5049 5.00053C20.0598 5.00053 22.4598 5.99544 24.2597 7.79543L50.7746 34.3103C53.3197 36.8502 54.8595 39.1152 55.6395 41.4503C56.8796 45.1653 59.8346 48.1203 63.5494 49.3601C65.8844 50.1352 68.1494 51.6802 70.7993 54.33C71.1696 54.7003 71.1696 55.3003 70.7995 55.6702Z" fill="#9097AB"/>
    <path d="M119.739 111.38L111.119 94.1449C109.839 91.59 107.269 90 104.409 90H101.034L71.7645 60.7352C70.7895 59.7602 69.2045 59.7602 68.2295 60.7352C67.2545 61.7102 67.2545 63.2953 68.2295 64.2703L98.2293 94.27C98.6992 94.735 99.3343 95.0001 99.9992 95.0001H104.409C105.364 95.0001 106.219 95.53 106.639 96.385L114.454 112.01L112.004 114.46L96.3794 106.645C95.5293 106.22 94.9994 105.365 94.9994 104.41V100C94.9994 99.3351 94.7343 98.6999 94.2644 98.23L64.2646 68.2302C63.2896 67.2552 61.7045 67.2552 60.7295 68.2302C59.7545 69.2052 59.7545 70.7903 60.7295 71.7653L89.9995 101.035V104.41C89.9995 107.265 91.5895 109.84 94.1444 111.115L111.379 119.735C111.739 119.915 112.119 120 112.499 120C113.144 120 113.784 119.75 114.269 119.265L119.269 114.265C120.034 113.505 120.219 112.34 119.739 111.38Z" fill="#9097AB"/>
    <path d="M119.954 23.4905C119.904 22.6306 119.314 21.8056 118.554 21.3905C117.774 20.9656 116.834 20.9806 116.069 21.4404L102.389 29.6454C98.2344 27.4954 95.5194 23.4605 95.0645 18.8705L110.344 9.70558C111.045 9.28558 111.495 8.55058 111.555 7.73567C111.61 6.92075 111.265 6.13068 110.629 5.61576C106.164 2.01063 100.869 0.0707248 95.3094 0.000647206C89.2045 -0.0492743 82.6795 2.79063 77.9046 7.88051C72.3247 13.8254 69.7447 19.7203 70.0097 25.8905C70.1548 29.2755 68.6398 32.8155 65.8499 35.6055L58.6548 42.8005C57.6798 43.7804 57.6798 45.3605 58.6548 46.3355C60.0247 47.7055 61.7148 48.7505 63.5448 49.3606C65.8799 50.1357 68.1448 51.6807 70.7947 54.3305C71.1097 54.6455 71.1097 55.3554 70.7947 55.6704L66.9798 59.4853C66.0048 60.4603 66.0048 62.0453 66.9798 63.0203L69.4798 65.5204C69.9699 66.0053 70.6097 66.2505 71.2498 66.2505C71.8899 66.2505 72.5297 66.0056 73.0198 65.5155L89.2898 49.2455C91.3147 49.7604 93.1597 50.0004 94.9998 50.0004C101.855 50.0004 108.485 47.1404 113.19 42.1454C117.964 37.0853 120.364 30.4605 119.954 23.4905ZM109.554 38.7203C105.734 42.7703 100.565 45.0003 94.9996 45.0003C93.2296 45.0003 91.4146 44.7003 89.2745 44.0504C88.3846 43.7804 87.4345 44.0253 86.7845 44.6755L76.0446 55.4154C76.1546 53.7403 75.5546 52.0153 74.2247 50.6855C71.1148 47.5756 68.2248 45.6455 65.1247 44.6155C64.8296 44.5206 64.5446 44.4055 64.2648 44.2705L69.3948 39.1356C73.1747 35.3605 75.2248 30.4507 75.0148 25.6706C74.8048 20.8507 76.8849 16.2807 81.5597 11.2958C85.3696 7.23083 90.5296 4.87092 95.2445 5.00077C98.4594 5.04084 101.574 5.87568 104.414 7.43567L91.2144 15.3556C90.4595 15.8105 89.9994 16.6205 89.9994 17.5006C89.9994 24.6206 93.9545 31.0204 100.324 34.2154L101.379 34.7404C102.149 35.1203 103.054 35.0854 103.784 34.6455L114.769 28.0556C114.169 32.0355 112.379 35.7203 109.554 38.7203Z" fill="#9097AB"/>
    <path d="M65.5244 69.4851L63.0244 66.985C62.0494 66.01 60.4643 66.01 59.4893 66.985L55.6744 70.7999C55.2894 71.1749 54.6994 71.1648 54.2344 70.6998C51.6845 68.1498 50.1395 65.8799 49.3644 63.5498C48.7543 61.7149 47.7095 60.0248 46.3394 58.6598C45.3594 57.6848 43.7793 57.6848 42.8043 58.6598L35.2643 66.1999C32.7942 68.6648 29.1244 69.7799 24.9994 69.9997C11.215 70.0002 0 81.215 0 94.9999C0 95.8748 0.109921 96.7148 0.220077 97.5548L0.299998 98.1447C0.409919 98.9798 0.929993 99.7047 1.69007 100.075C2.45506 100.445 3.34005 100.405 4.07013 99.9698L20.105 90.3499C24.26 92.4998 26.975 96.5299 27.4299 101.125L10.705 111.16C9.98501 111.595 9.52493 112.365 9.49493 113.205C9.46001 114.05 9.85493 114.85 10.54 115.34C14.81 118.38 19.79 119.989 24.95 120C24.965 120 24.9751 120 24.9849 120C31.1899 120 37.4648 117.085 42.1999 112.01C47.7048 106.104 50.2548 100.255 49.9897 94.1247C49.8446 90.7348 51.3648 87.1798 54.1646 84.3797L65.5244 73.0199C66.4994 72.0451 66.4994 70.4601 65.5244 69.4851ZM50.6197 80.84C46.8348 84.6249 44.7798 89.5448 44.9847 94.335C45.1898 99.1249 43.1397 103.655 38.5347 108.6C34.7447 112.67 29.8097 115 24.9849 115C24.9748 115 24.965 115 24.9598 115C22.1748 114.995 19.4499 114.4 16.9199 113.265L31.2849 104.645C32.0398 104.19 32.4998 103.38 32.4998 102.5C32.4998 95.3798 28.5399 88.9748 22.175 85.785L21.1201 85.26C20.3551 84.8801 19.4502 84.915 18.7152 85.355L5.0552 93.555C5.79511 83.2001 14.4601 75.0001 25.0601 75.0001C25.0901 75.0001 25.1201 75.0001 25.1501 75.0001C30.555 75.0001 35.4 73.13 38.8 69.7351L44.2701 64.2651C44.4051 64.5452 44.515 64.8302 44.6151 65.125C45.6451 68.2199 47.5801 71.1149 50.78 74.315C52.0449 75.5851 53.7249 76.1799 55.4101 76.0501L50.6197 80.84Z" fill="#9097AB"/>
    </svg>
    </Grid>
    <Grid item>
      <Typography align="center" variant="h4">We're working on it!</Typography>
    </Grid>
    <Grid item style={{padding:"0 50px 0 50px"}}>
      <Typography align="center">Studybuddy is currently only available on screens larger than 960px. Resize your screen or use a desktop or laptop for the meantime.</Typography>
    </Grid>
    </Grid>
  )
}

    }
    function Contents(){
      return(
      <>
      <Topbar />
      <SubjRoute exact path="/le">
        <Longexams/>
      </SubjRoute>
      <SubjRoute exact path="/ma">
        <MyAccount/>
      </SubjRoute>
      <SubjRoute exact path="/question/:id">
        <IndivQnA/>
      </SubjRoute>
      <FooterNew/>
      </>
      )
    }

    function PrivateRoute({ children, ...rest }) {
      const fire = useContext(FirebaseContext);
      const [loadprot, setLoadprot] = useState(true);
      const [proc, setProc] = useState(false);

      const c = localStorage.getItem("localSubjects");

      return (
        <Route
          {...rest}
          render={({ location }) =>
            fire.status || c==0 ? (
              children
            ) : (
              <Redirect
                to={{
                  pathname: "/",
                  state: { from: location }
                }}
              />
            )
          }
        />
      );
    }


    function SubjRoute({ children, ...rest }) {
      const fire = useContext(FirebaseContext);
      const click = useContext(ClickContext);
      const [loadprot, setLoadprot] = useState(false);
      const [proc, setProc] = useState(false);

      const c = localStorage.getItem("localSubjects");

      return (
        <Route
          {...rest}
          render={({ location }) =>
            c>0 || fire.status ? (
              children
            ) : c==0?
            (
              <Redirect
                to={{
                  pathname: "/ss",
                  state: { from: location }
                }}
              />
            ):
            (
              <Redirect
                to={{
                  pathname: "/",
                  state: { from: location }
                }}
              />
            )
          }
        />
      );
    }


export default Main;