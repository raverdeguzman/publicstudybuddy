import React, {useContext,useEffect,useState,useRef} from 'react';
import { Link, useParams, useLocation } from 'react-router-dom';


import './styles/footer.css';
import logo from './img/footer logo.png';

export default function FooterNew(){
    return(
        <div className="footer-wrapper">
            <div>
                <img width="144px" height="138px" src={logo}/>
            </div>
            <div className="footer-content-wrapper">
{/*                 <div className="footer-content-links">
                    <Link to="/#about" style={{textDecoration:"none", color:"inherit"}}>
                    <div>ABOUT</div>
                    </Link>
                    <Link to="/#features" style={{textDecoration:"none", color:"inherit"}}>
                    <div>FEATURES</div>
                    </Link>
                    <Link to="/#questions" style={{textDecoration:"none", color:"inherit"}}>
                    <div>QUESTIONS</div>
                    </Link> 
                    <Link to="/#team" style={{textDecoration:"none", color:"inherit"}}>
                    <div>TEAM</div>
                    </Link>
                </div> */}
                <div className="footer-content-description">
                    <div>Designed and developed under the Academics Committee of Ateneo Gabay</div>
                    <div>Copyright © 2020. All rights reserved.</div>
                </div>

            </div>
            <div className="footer-policies"> 
                <div style={{display:"flex", justifyContent:"flex-start",marginBottom:"16px"}}>
                <a href="https://facebook.com/StudybuddyADMU" style={{textDecoration:"none", color:"inherit"}}>
                <svg role="button" width="94" height="35" viewBox="0 0 94 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M32.0833 0H2.91667C1.30521 0 0 1.30521 0 2.91667V32.0833C0 33.6948 1.30521 35 2.91667 35H18.9583V21.875H14.5833V16.0417H18.9583V12.269C18.9583 7.74812 21.719 5.28646 25.7527 5.28646C27.685 5.28646 29.3446 5.43083 29.8287 5.495V10.22L27.0317 10.2215C24.8383 10.2215 24.414 11.2642 24.414 12.7925V16.0417H30.889L29.4306 21.875H24.414V35H32.0833C33.6948 35 35 33.6948 35 32.0833V2.91667C35 1.30521 33.6948 0 32.0833 0Z" fill="#FAF9F9"/>
                </svg> 
                </a>
                <a href="mailto:gabay.ls@obf.ateneo.edu" style={{textDecoration:"none", color:"inherit"}}>
                 <svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M34.2973 14.3173H17.5937V21.477H27.0611C25.5474 26.2501 21.809 27.8404 17.5171 27.8404C12.9461 27.8461 8.91373 24.8499 7.60008 20.4718C6.28644 16.0936 8.00329 11.3724 11.8224 8.86074C15.6415 6.34909 20.6566 6.64301 24.1561 9.58359L29.358 4.62894C23.5963 -0.678569 15.0093 -1.51047 8.33598 2.59235C1.66265 6.69516 -1.47112 14.7331 0.664138 22.2701C2.7994 29.8072 9.68345 35.007 17.5171 35C27.1661 35 35.892 28.6366 34.2973 14.3173Z" fill="#FAF9F9"/>
                </svg>
                </a>
                </div>
                <a style={{textDecoration:"none", color:"inherit"}} href="https://drive.google.com/file/d/1MSsAwUBpM3PnOcZg01dks86sAQR9HuJg/view">
                <div>CODE OF CONDUCT</div>
                </a>
                <a style={{textDecoration:"none", color:"inherit"}} href="https://drive.google.com/file/d/1MSsAwUBpM3PnOcZg01dks86sAQR9HuJg/view">
                <div>PRIVACY POLICY</div>
                </a>
                {/* <div>PRIVACY POLICY</div> */}
            </div>
        </div>
    )
}