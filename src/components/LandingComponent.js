import React, {useState,useContext,useEffect} from 'react';

import './styles/landing.css';

import tab from './img/Tab Pic.png';
import profile from './img/Profile.png';

import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import{FirebaseContext} from '../contexts/FirebaseContext';
import raver from "./img/raver.jpg"
import kendra from "./img/kendra.jpg"
import rj from "./img/rj.jpg"
import audrey from "./img/audrey.jpg"
import christian from "./img/christian.jpeg"
import claire from "./img/claire.jpg"
import matt from "./img/matt.png"
import kyla from "./img/kyla.png"
import rikki from "./img/rikki.jpg"

import { Link,Redirect } from 'react-router-dom';


import FooterNew from './FooterNewComponent';


import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      marginBottom: 96,
        },
    accordionClosed:{
        backgroundColor: "#EAC646",
    },
    accordionOpen:{
        backgroundColor: "#FFFFFF",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
  }));




export default function Landing (){

    const classes = useStyles();
    const fire = useContext(FirebaseContext);

    /* const [expand,setExpand] = useState(false); */
    const [num, setNum] =useState(0);

    const featureNames =["Anonymous Online Forum", "LE Database", "Tutors Marketplace", "Monetized Answers"];
    const featureDesc=["(1) A large database of sample long exams from classes offered in the Ateneo is available for you to use.",
    "(2) A large database of sample long exams from classes offered in the Ateneo is available for you to use.",
    "(3) A large database of sample long exams from classes offered in the Ateneo is available for you to use.",
    "(4) A large database of sample long exams from classes offered in the Ateneo is available for you to use.",
    ]


    return (     
    <div>   
        <div className="header">
            <div>
            </div>
            <div className="nested">
                <div className="header-element" role="button">
                    <Link to={fire.status?"/selection":"/"} onClick={()=>fire.status?"":alert("You must login first")} style={{textDecoration:"none"}}>FORUM</Link>
                    {/* <a className="no-text-decorations" href="#about">FORUM</a> */}
                </div>
                <div className="header-element" role="button">
                    {/* <a href="#features">EXAMS</a> */}
                    <Link to={fire.status?"/exams":"/"} onClick={()=>fire.status?"":alert("You must login first")} style={{textDecoration:"none"}}>EXAMS</Link>
                </div>
{/*                 <div className="header-element" role="button">
                    <Link to={fire.status?"/tutors":"/"} onClick={()=>fire.status?"":alert("You must login first")} style={{textDecoration:"none"}}>TUTORS</Link>
                </div> */}
                <div className="header-element" role="button">
                    {/* <a href="#team">DASHBOARD</a> */}
                    <Link to={fire.status?"/dashboard":"/"} onClick={()=>fire.status?"":alert("You must login first")} style={{textDecoration:"none"}}>DASHBOARD</Link>
                </div>
                <div className="header-element header-login" onClick={()=>fire.status?fire.signout(fire.auth.currentUser.uid):fire.signin()} role="button">
                {fire.status?"LOGOUT":"LOGIN"}
                </div>
            </div>
        </div>
        <div className="wrapper">
            <div className="nested-wrapper-1">
                <div className="nested-content-1">
                    <div className="studybuddy-name">Studybuddy</div>
                    <div className="studybuddy-subtitle">We study as a community.</div>
                    <div className="studybuddy-button" onClick={()=>fire.status?fire.signout(fire.auth.currentUser.uid):fire.signin()} role="button">{fire.status?"LOGOUT":"LOGIN WITH OBF EMAIL"}</div>
                </div>
                <div className="nested-content-1">
                <svg width="394" height="463" viewBox="0 0 394 463" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M285.215 306.616L231.442 364.599C228.417 366.962 225.97 369.984 224.287 373.435C222.603 376.886 221.728 380.674 221.728 384.513V445.502C221.728 449.213 223.202 452.771 225.826 455.395C228.45 458.019 232.009 459.493 235.719 459.493H305.665" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                            <path d="M221.871 297.414C221.871 299.854 222.841 302.195 224.567 303.921C226.292 305.647 228.633 306.616 231.074 306.616H363.947C371.095 306.615 377.95 303.775 383.005 298.721C388.059 293.667 390.899 286.811 390.9 279.663V174.207C390.9 171.79 390.425 169.398 389.5 167.165C388.575 164.933 387.219 162.905 385.509 161.197C383.8 159.49 381.771 158.135 379.538 157.213C377.305 156.29 374.911 155.816 372.495 155.819H304.271C302.622 155.818 300.99 156.143 299.467 156.773C297.943 157.404 296.559 158.329 295.394 159.494C294.228 160.66 293.303 162.044 292.673 163.567C292.042 165.09 291.718 166.723 291.718 168.372V261.262C291.718 268.409 288.879 275.264 283.825 280.318C278.771 285.372 271.916 288.211 264.768 288.211H231.074C228.633 288.211 226.292 289.181 224.567 290.907C222.841 292.633 221.871 294.973 221.871 297.414Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                            <path d="M222.236 299.984C222.792 301.897 223.955 303.578 225.549 304.774C227.142 305.97 229.081 306.617 231.074 306.616H363.947C371.095 306.615 377.95 303.775 383.005 298.721C388.059 293.666 390.899 286.811 390.9 279.663V174.207C390.899 170.811 389.959 167.481 388.183 164.587C386.407 161.692 383.866 159.345 380.839 157.806C382.74 160.766 383.748 164.212 383.743 167.731V277.315C383.743 283.371 381.337 289.18 377.054 293.462C372.772 297.745 366.963 300.151 360.907 300.151H223.892C223.336 300.147 222.782 300.091 222.236 299.984Z" fill="#F0E1D4" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                            <path d="M291.377 225.253H266.156C254.428 225.257 243.182 229.919 234.891 238.214C226.6 246.508 221.943 257.755 221.943 269.483V300.158" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                            <path d="M343.497 306.616L289.724 364.599C286.699 366.962 284.252 369.984 282.569 373.435C280.886 376.886 280.011 380.674 280.01 384.513V445.502C280.01 449.213 281.484 452.771 284.108 455.395C286.732 458.019 290.291 459.493 294.002 459.493H363.947" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                            <path d="M284.019 85.7403C284.019 85.7403 258.838 81.9911 243.709 115.072C230.457 144.043 220.576 171.061 220.576 171.061C220.576 171.061 200.154 182.482 165.805 194.708V211.102C165.805 211.102 218.194 214.145 235.675 211.102C248.528 208.866 283.995 161.292 286.459 129.022C288.923 96.7525 284.019 85.7403 284.019 85.7403Z" fill="#EAC646"/>
                            <path d="M172.696 210.941C174.636 210.549 173.221 204.52 172.083 200.802C171.452 198.757 169.946 197.707 167.938 196.824C163.944 195.059 157.032 194.029 153.136 194.881C147.417 196.132 143.743 198.678 140.184 202.84C139.162 204.029 134.135 209.752 134.135 211.279L172.696 210.941Z" fill="#FAB6AD"/>
                            <path d="M308.132 72.4308C329.843 74.6666 371.756 101.027 364.82 197.819C360.389 259.714 319.751 290.73 314.84 288.624C309.929 286.517 226.592 245.444 245.682 223.348C263.947 202.216 258.003 186.047 255.13 157.431C252.256 128.815 247.938 97.6728 271.149 86.8957C308.76 69.4417 302.328 71.8309 308.132 72.4308Z" fill="#EAC646"/>
                            <path d="M134.151 459.043H193.518C193.579 458.096 193.596 457.196 193.582 456.316H134.083C134.016 457.225 134.039 458.139 134.151 459.043Z" fill="#383C4D"/>
                            <path d="M187.467 431.34C187.256 438.096 193.513 442.373 193.513 456.33H134.151C134.151 456.33 133.288 444.622 145.16 441.923C157.031 439.224 167.825 435.618 168.905 431.357L187.467 431.34Z" fill="#EAC646"/>
                            <path d="M251.309 219.251C251.309 219.251 160.702 213.341 138.685 240.352C116.667 267.363 159.407 373.651 163.725 432.182C163.725 432.182 185.406 434.432 194.209 432.182C203.013 429.933 210.675 316.514 188.497 289.053C188.497 289.053 269.417 309.282 302.291 283.767C335.164 258.252 264.99 219.728 251.309 219.251Z" fill="#383C4D"/>
                            <path d="M1.90735e-05 365.318L47.0894 403.01C47.771 402.162 48.3572 401.33 48.8992 400.522L1.99388 362.986C1.24301 363.685 0.574097 364.468 1.90735e-05 365.318Z" fill="#41465A"/>
                            <path d="M58.831 376.961C54.7206 382.182 57.1882 389.551 49.0423 400.621L1.94955 362.932C1.94955 362.932 8.08454 353.096 19.0934 358.501C30.1023 363.907 40.7533 367.894 44.1071 365.185L58.831 376.961Z" fill="#EAC646"/>
                            <path d="M317.362 286.48C315.999 294.104 242.301 315.989 160.273 261.964C160.273 261.964 147.546 333.225 64.6524 380.498C64.6524 380.498 46.4009 372.387 41.4384 361.466C41.4384 361.466 99.9149 286.725 120.736 226.156C129.482 200.751 239.482 214.043 255.563 219.251C271.643 224.459 323.415 252.506 317.362 286.48Z" fill="#41465A"/>
                            <path d="M314.448 97.1956C295.467 103.501 268.753 159.667 268.753 159.667C268.753 159.667 238.855 162.533 193.606 181.48C193.606 181.48 192.563 191.327 198.524 199.394C198.524 199.394 248.286 205.843 274.169 203.194C282.921 202.291 316.088 177.482 334.322 143.15C350.627 112.441 337.635 89.4962 314.448 97.1956Z" fill="#EAC646"/>
                            <path d="M193.957 181.793C193.957 181.793 181.346 175.134 175.44 176.466C169.533 177.799 162.417 183.126 161.677 184.605C160.937 186.085 155.757 193.337 155.757 194.963C155.757 196.589 172.498 195.986 174.843 195.986C177.188 195.986 192.679 196.235 195.668 195.614C198.657 194.994 193.957 181.793 193.957 181.793Z" fill="#FFCFC9"/>
                            <path d="M174.857 195.999C172.498 195.999 155.77 196.592 155.77 194.977C155.77 193.361 160.951 186.115 161.691 184.619C162.43 183.123 169.53 177.802 175.453 176.48C175.453 176.48 168.146 186.626 174.871 196.013" fill="#FAB6AD"/>
                            <path d="M289.547 41.7525C289.169 44.6973 295.736 59.5098 307.454 71.0265C317.737 81.1254 273.507 88.9406 272.349 88.685C271.19 88.4294 266.026 72.8772 264.966 69.2337C263.906 65.5903 289.547 41.7525 289.547 41.7525Z" fill="#FFCFC9"/>
                            <path d="M264.969 69.2338C265.426 70.805 267.253 75.9379 269.274 81.289C274.155 78.927 279.871 74.2985 284.342 65.488C289.186 55.9447 288.582 46.1935 289.162 43.6748C289.201 43.2094 289.126 42.7415 288.944 42.3115C285.085 45.9413 264.008 65.9209 264.969 69.2338Z" fill="#FAB6AD"/>
                            <path d="M251.381 26.5241C249.537 33.869 247.914 46.1833 250.423 52.1615C252.931 58.1397 257.359 69.1247 259.094 69.7007C260.828 70.2767 279.912 67.775 284.342 59.4757C288.773 51.1765 288.971 44.2474 289.547 41.7525C290.123 39.2576 279.717 19.0054 266.418 19.3905C253.119 19.7756 251.916 24.3939 251.381 26.5241Z" fill="#FFCFC9"/>
                            <path d="M243.32 22.8806C246.854 24.3292 244.175 28.4294 250.818 29.0429C257.461 29.6564 258.998 21.5139 258.998 21.5139C258.998 21.5139 259.755 24.7688 263.088 25.8799C263.088 25.8799 261.312 22.4307 263.858 20.9106C266.404 19.3905 267.969 21.4253 267.969 21.4253C267.969 21.4253 270.188 20.3142 271.377 22.9658C272.567 25.6175 270.423 27.2058 267.628 27.5091C267.628 27.5091 267.372 31.4491 273.623 32.049C279.874 32.6489 281.162 28.7941 281.162 28.7941C281.162 28.7941 282.185 27.85 282.621 30.5936C283.057 33.3373 280.917 37.7886 285.276 41.3026C287.018 42.7034 288.003 43.259 288.524 43.4192C286.326 45.321 287.389 51.0947 292.372 53.4498C299.138 56.6502 304.1 54.2849 304.717 49.6087C305.273 45.3858 302.58 47.0422 303.269 43.7873C303.269 43.7873 307.676 47.0115 310.529 42.3149C312.86 38.4771 308.783 36.0742 308.783 36.0742C308.783 36.0742 311.687 32.0933 309.274 29.3121C306.548 26.139 302.284 28.1942 302.908 26.2242C303.531 24.2542 305.16 19.6257 302.509 17.0422C299.857 14.4587 295.651 15.4982 295.055 14.1281C294.458 12.7579 295.307 10.7197 298.047 11.9092C297.939 11.0251 297.639 10.1752 297.168 9.41927C296.697 8.66329 296.067 8.01948 295.321 7.53297C292.89 5.89357 289.288 6.2753 286.8 7.00468C286.377 4.77223 285.324 2.04557 282.631 0.77768C278.17 -1.31162 274.987 1.23439 273.245 3.4089C272.988 2.64825 272.517 1.97808 271.888 1.47917C271.259 0.980257 270.499 0.673904 269.7 0.597039C264.39 -0.173241 260.021 1.20713 258.998 5.48798C257.918 9.99718 250.031 -0.39819 246.319 8.4873C245.559 10.3039 247.543 11.7388 245.297 13.201C244.025 14.0292 242.393 11.3264 243.664 10.3789C242.079 11.0349 240.759 12.2007 239.912 13.6918C239.23 14.8915 237.625 20.5527 243.32 22.8806Z" fill="#383C4D"/>
                            <path d="M383.743 225.253H353.068C341.34 225.257 330.095 229.919 321.804 238.214C313.513 246.508 308.855 257.755 308.855 269.483V300.158" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                            <path d="M42.3484 206.224H218.126C219.436 206.224 220.693 206.745 221.62 207.672C222.547 208.599 223.068 209.856 223.068 211.166C223.068 212.477 222.547 213.734 221.62 214.661C220.693 215.588 219.436 216.108 218.126 216.108H42.3484V206.224Z" fill="white"/>
                            <path d="M42.3484 206.224H218.126C219.436 206.224 220.693 206.745 221.62 207.672C222.547 208.599 223.068 209.856 223.068 211.166V211.166C223.068 212.477 222.547 213.734 221.62 214.661C220.693 215.588 219.436 216.108 218.126 216.108H42.3484" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10"/>
                            <path d="M118.453 459.152L90.0751 221.405C89.1753 216.337 81.9633 216.177 80.842 221.2L45.4909 457.82" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                            <path d="M61.1895 197.076H196.404C198.245 197.076 199.738 198.569 199.738 200.41V202.455C199.738 204.296 198.245 205.788 196.404 205.788H61.1895C59.3486 205.788 57.8562 204.296 57.8562 202.455V200.41C57.8562 198.569 59.3486 197.076 61.1895 197.076Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10"/>
                            <path d="M66.6837 196.926H165.791C168.354 196.926 170.075 194.462 169.042 192.267L141.046 132.717C139.458 129.343 135.89 127.165 131.95 127.165H32.8391C30.276 127.165 28.5583 129.629 29.591 131.824L57.5869 191.371C59.1718 194.745 62.7403 196.926 66.6837 196.926Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10"/>
                </svg>
                </div>
                <div id="about"></div>
            </div>
            
            <div className="nested-wrapper-2">
                <div className="about-name">About</div>
                <div className="nested-subwrapper-2">
                    <div className="nested-content-2">
                        <img width="295" height="406" src={tab}/>
                    </div>
                    <div className="nested-content-2">
                        <div className="about-description">
                            Studybuddy is an anonymous online forum where Atenean students can ask and answer academic questions, gain access to a database of hundreds of long exams, and earn money for giving quality answers. In addition, students can connect with tutors to further enhance their discussions.

                            As an initiative of Ateneo Gabay’s Academics Committee, the project aims to 
                            strengthen the communication of the organization’s advocacy for quality education and assert its role in the current realities of the Ateneo and the Philippines.
                        </div>
                    </div>
                </div>
                <div id="features"></div>
            </div>
{/*             <div className="nested-wrapper-3">
                <div className="features-name">
                Features
                </div>
                <div className="nested-wrapper-3sub">
                    <div className="features-left-arrow" role="button" onClick={()=>setNum(y=>y-1)}>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.81066 12L11.2803 19.4697C11.5732 19.7626 11.5732 20.2374 11.2803 20.5303C10.9874 20.8232 10.5126 20.8232 10.2197 20.5303L2.21967 12.5303C1.92678 12.2374 1.92678 11.7626 2.21967 11.4697L10.2197 3.46967C10.5126 3.17678 10.9874 3.17678 11.2803 3.46967C11.5732 3.76256 11.5732 4.23744 11.2803 4.53033L3.81066 12Z" fill="#161716"/>
                        </svg>
                    </div>

                    <div className="nested-subwrapper-3">
                        <div className="nested-content-3">
                            <svg width="473" height="336" viewBox="0 0 473 336" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g filter="url(#filter0_dd)">
                            <rect x="32" y="16" width="408.477" height="272" fill="#C4C4C4"/>
                            </g>
                            <defs>
                            <filter id="filter0_dd" x="0" y="0" width="472.477" height="336" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                            <feOffset dy="8"/>
                            <feGaussianBlur stdDeviation="8"/>
                            <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"/>
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                            <feOffset dy="16"/>
                            <feGaussianBlur stdDeviation="16"/>
                            <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"/>
                            <feBlend mode="normal" in2="effect1_dropShadow" result="effect2_dropShadow"/>
                            <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow" result="shape"/>
                            </filter>
                            </defs>
                            </svg>
                        </div>
                        <div className="nested-content-3">
                            <div className="features-heading">
                                {featureNames[num]}
                            </div>
                            <div className="features-description">
                                {featureDesc[num]}
                            </div>
                        </div>

                    </div>

                    <div className="features-right-arrow" role="button" onClick={()=>setNum(y=>y+1)}>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20.1893 12L12.7197 19.4697C12.4268 19.7626 12.4268 20.2374 12.7197 20.5303C13.0126 20.8232 13.4874 20.8232 13.7803 20.5303L21.7803 12.5303C22.0732 12.2374 22.0732 11.7626 21.7803 11.4697L13.7803 3.46967C13.4874 3.17678 13.0126 3.17678 12.7197 3.46967C12.4268 3.76256 12.4268 4.23744 12.7197 4.53033L20.1893 12Z" fill="#161716"/>
                        </svg>
                    </div>
                </div>
                <div className="features-carousel-dots">
                    <svg width="134" height="14" viewBox="0 0 134 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="7" cy="7" r="7" fill="#161716"/>
                    <circle cx="37" cy="7" r="6.5" stroke="#161716"/>
                    <circle cx="67" cy="7" r="6.5" stroke="#161716"/>
                    <circle cx="97" cy="7" r="6.5" stroke="#161716"/>
                    <circle cx="127" cy="7" r="6.5" stroke="#161716"/>
                    </svg>
                </div>
                <div id="questions"></div>
            </div>
            <div className="nested-wrapper-4">
                <div className="questions-name">Questions?</div>
                    <div className="nested-content-4">
                        <div className="questions-description">
                            Feel free to send us a message through the Studybuddy Facebook page if you don’t find your answers here.
                        </div>
                        <div className={classes.root}>
                            <Accordion TransitionProps={{ unmountOnExit: true }} style={{padding: 20}} elevation={9}>                                
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon fontSize="large"/>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                                >
                                <div className="accordion-heading">Question 1</div>
                                </AccordionSummary>
                                <AccordionDetails>
                                <div className="accordion-body">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                                    sit amet blandit leo lobortis eget.
                                </div>
                                </AccordionDetails>
                            </Accordion>                     
                            <Accordion TransitionProps={{ unmountOnExit: true }} style={{padding: 20}} elevation={9}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon fontSize="large"/>}
                                aria-controls="panel2a-content"
                                id="panel2a-header"
                                >
                                <div className="accordion-heading">Question 2</div>
                                </AccordionSummary>
                                <AccordionDetails>
                                <div className="accordion-body">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                                    sit amet blandit leo lobortis eget.
                                </div>
                                </AccordionDetails>
                            </Accordion>
                        </div>
                        <div className="questions-button" role="button">
                            GO TO STUDYBUDDY FB
                        </div>
                    </div>
                    <div id="team"></div>
            </div>
       */}      
{/*             <div className="nested-wrapper-5">
                <div className="team-name">Meet the Team</div>
                <div className="team-description">The Atenean students behind Studybuddy.</div>
                <div className="nested-content-5">
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={raver}/>
                        <div className="team-member-name">Raver De Guzman</div>
                        <div className="team-member-description">PROJECT HEAD, DEV</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={kendra}/>
                        <div className="team-member-name">Kendra Go</div>
                        <div className="team-member-description">PROJECT HEAD, DESIGN</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={rj}/>
                        <div className="team-member-name">RJ Valiente</div>
                        <div className="team-member-description">RESEARCH</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={kyla}/>
                        <div className="team-member-name">Kyla Villegas</div>
                        <div className="team-member-description">DESIGN</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={matt}/>
                        <div className="team-member-name">Matt Alejo</div>
                        <div className="team-member-description">DESIGN</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={rikki}/>
                        <div className="team-member-name">Rikki Chu</div>
                        <div className="team-member-description">RESEARCH</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={christian}/>
                        <div className="team-member-name">Christian Dasalla</div>
                        <div className="team-member-description">MARKETING</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={claire}/>
                        <div className="team-member-name">Claire Valleza</div>
                        <div className="team-member-description">MARKETING</div>
                    </div>
                    <div className="team-member">
                        <img className="team-member-pic" width="240" height="240" src={audrey}/>
                        <div className="team-member-name">Audrey Benamer</div>
                        <div className="team-member-description">RESEARCH</div>
                    </div>
                </div>
            </div> */}
        </div>
        <FooterNew/>
    </div>
    )

    /* 
                            <div className="nested-content-3">
                            <div className="features-heading">
                                Tutors Marketplace
                            </div>
                            <div className="features-description">
                                A large database of sample long exams from classes offered in the Ateneo is available for you to use.
                            </div>
                        </div>
                        <div className="nested-content-3">
                            <div className="features-heading">
                                LE Database
                            </div>
                            <div className="features-description">
                                A large database of sample long exams from classes offered in the Ateneo is available for you to use.
                            </div>
                        </div>
                        <div className="nested-content-3">
                            <div className="features-heading">
                                Monetized Answers
                            </div>
                            <div className="features-description">
                                A large database of sample long exams from classes offered in the Ateneo is available for you to use.
                            </div>
                        </div> */


} 