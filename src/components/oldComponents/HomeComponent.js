import React, {useState,useContext,useEffect} from 'react';
import {Logtest} from './LoginComponent';
import Menu from './MenuComponent';
import Forum from './ForumComponent';
import{FirebaseContext} from '../contexts/FirebaseContext';
import { Redirect } from 'react-router';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import logo from './img/logo.png';
import triangles from './img/triangles.png';

const useStyles = makeStyles({
    buttonStyle:{
        color: "blue",
        //border:"none"
    },
    container:{
        backgroundColor:"#7AB9EA",
        paddingLeft:96
    },
    subtitleStyle:{
        color: "white",
        fontFamily: "Roboto,sans-serif",
        fontWeight: 700,
        fontSize: 56,
        lineHeight:1.2
    },
    buttonColor:{
        backgroundColor:"#6de5ff",
        "&:hover":{
            backgroundColor:"#ff9840",
            //color: "#FFFFFF"
        },
        textTransform: "uppercase"
    },  
    welcome:{
        fontFamily: "Comfortaa,sans-serif",
        color: "white",
        fontSize: 36
    }
})

        
/* onClick={()=>{
    const sayHello = fire.functions.httpsCallable('sayHello');
        sayHello({name:"Shaun"}).then(result =>{
            console.log(result.data);
        });
}} */
function Home () {
    const fire = useContext(FirebaseContext);
    const [emailOne,setEmailOne] = useState("");
    const [emailTwo,setEmailTwo] = useState("");
    const [emailThree,setEmailThree] = useState("");
    const [org,setOrg] = useState("");

    const classes = useStyles();

    const capitalize = (str, lower = true) =>
    (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());
    ;


    return( 
                <Grid container 
                direction="column"
                justify="center"
                alignItems="flex-start"
                style={{ minHeight: '100vh', width: '100%',position:"relative",overflow:"hidden",}}
                className={classes.container}
                    >
                    <Grid container item justify="flex-end" style={{position:"absolute"}}>
                    <img style={{zIndex:5,pointerEvents:"none"}} src={triangles}/>
                    </Grid>
                    {fire.admin ? 
                        <Grid item>
                            <input value={emailOne} placeholder="Make Admin" onChange={e => setEmailOne(e.target.value)} type="text"/>
                            <button onClick={()=>{
                                fire.functions.httpsCallable('addModeratorRole')({email:emailOne})
                                .then(result=>console.log(result));
                                }}>Submit</button>
                        </Grid> :
                        <div></div>}
                        {fire.admin ?  <Grid item>
                            <label>Add a moderator:</label>
                            <input value={emailTwo} placeholder="Add Moderator's email address" onChange={e => setEmailTwo(e.target.value)} type="text"/>
                            <button onClick={()=>{
                                fire.functions.httpsCallable('addModeratorRole')({email:emailTwo})
                                .then(result=>console.log(result));
                                }}>Submit</button>
                        </Grid> :
                        <div></div> }
                        {fire.admin  ? <Grid item>
                        <label>Add a tutor:</label>
                            <input value={emailThree} placeholder="Add Tutor's email address" onChange={e => setEmailThree(e.target.value)} type="text"/>
                            <input value={org} placeholder="Org" onChange={e => setOrg(e.target.value)} type="text"/>
                            <button onClick={()=>{
                                fire.functions.httpsCallable('addTutorRole')({email:emailThree, org:org})
                                .then(result=>console.log(result));
                                }}>Submit</button>
                        </Grid> :
                        <div></div>}
                        <Grid container item style={{marginBottom:90}}>
                            <Grid item>
                            <img width="50px" height="50px" src={logo}/>
                            </Grid>
                            <Grid item>
                            <Typography classes={{root:classes.welcome}}>studybuddy</Typography>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography classes={{root: classes.subtitleStyle}}>We study as a<br/>community</Typography>
                            {/* <Typography classes={{root: classes.subtitleStyle}}>community.</Typography> */}
                        </Grid>
                        <Grid item style={{ marginTop: '30px', marginBottom: '30px'}}>
                            <Logtest/>
                        </Grid>
                        <Grid item>
                            {fire.org? `Welcome ${capitalize(fire.name)} from the ${fire.org}!` : fire.moderator || fire.admin? `Welcome Moderator ${capitalize(fire.name)}!`:"" }
                        </Grid>
                </Grid>
    )
};

export default Home;