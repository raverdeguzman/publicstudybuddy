import React, {useContext,useEffect,useState,useRef, setState} from 'react';
import{FirebaseContext} from '../contexts/FirebaseContext';
import {ClickContext} from '../contexts/ClickContext';
import {FilterContext} from '../contexts/FilterContext';
import AutoLinkText from 'react-autolink-text2';
import {Logtest} from './LoginComponent';
import FilterContextProvider from '../contexts/FilterContext';

import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Typography, Button, Tab,Tabs, CssBaseline, TextField } from '@material-ui/core';

import {makeStyles} from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Tooltip from '@material-ui/core/Tooltip';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CloseIcon from '@material-ui/icons/Close';
import TurnedInNotIcon from '@material-ui/icons/TurnedInNot';

import Backdrop from '@material-ui/core/Backdrop';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';


import Paper from '@material-ui/core/Paper';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

//import FlagRoundedIcon from '@material-ui/icons/FlagRounded';
import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
import TurnedInRoundedIcon from '@material-ui/icons/TurnedInRounded';
//import TurnedInNotRoundedIcon from '@material-ui/icons/TurnedInNotRounded';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import Skeleton from '@material-ui/lab/Skeleton';

import Container from '@material-ui/core/Container';
import ButtonBase from '@material-ui/core/ButtonBase';
import InputBase from '@material-ui/core/InputBase';

import CircularProgress from '@material-ui/core/CircularProgress';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import Chip from '@material-ui/core/Chip';
import DoneIcon from '@material-ui/icons/Done';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import FlagOutlinedIcon from '@material-ui/icons/FlagOutlined';
import TurnedInOutlinedIcon from '@material-ui/icons/TurnedInOutlined';
import RemoveIcon from '@material-ui/icons/Remove';
import TurnedInIcon from '@material-ui/icons/TurnedIn';
import CancelIcon from '@material-ui/icons/Cancel';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import FlagIcon from '@material-ui/icons/Flag';
//Other components
import VerticalLinearStepper from './StepperComponent';

import Searchbar from './SearchbarComponent';
import { Link } from 'react-router-dom';

import Navbar from './NavbarComponent';

import Subject from './SubjectComponent';

import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component'


import {IndivQnA} from './MyAccountComponent';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

/**
Assignment:

Add a watchlist. *check*
Apply Image Hosting API. *check*

Try now (Tuesday):
Create an autocomplete (use to differentiate subjects). *check*
The value in autocomplete will then be used in the useData query (e.g. Retrieve only docs with 'Math' in code). *check*

Wednesday
Polish DB (Use pagination) *check*
Just use get for Questions, but locally, modify array to allow changes from user input. *check*
Fix filter button (e.g. filter Math 10 from entire collection and not just the 5 results) *check*
-where number > 0 *check*
-then onclick, replace > with == and 0 with query. *check* 

Thursday
Add a search function (Search question/description). *check* 
Add a special tutor account and change corresponding UI. *check* 

Saturday
Put post in actual component *check* 
Put watchlist in actual component *check* (Fix watchlist delete) 

Add a flag system *check*

Polish UI (Fix overflow, reply divs, etc).
* Add an appbar *check*
* Fix layout *check*
* Card Questions *check*
    *fix x icon in image attachment
* Card Answers, Card Replies

Add OP, Friend 1, Friend 2, Tutor 1 system
Add an upvote function
Add form validation
Apply session (subjname)/local storage (forms, subjnum)
Fix Protected Routes.
 * */

/**
TO DO LIST:
- Add Functionality for Chips
- Hide Qform
- Add functionality to Question Cards
- Create new Questions and Answers Block
- Update styles according to Figma design

DONE:
- Forum (closed) Base UI
- Forum (ask a 1) Base UI

CHANGES TO FILE:
(08/25/2020)
- Changed style for Navbar
- Changed measurement of Grid items (Navbar)
- Added My Account and Long Exams to UI
- Imported Add, Done and Search Icons 
- Added Grid Container for Chips
- Added Chips (w/out functionality)
- Added default handleDelete and handleClick function (no use yet)

(08/26/2020)
- Changed color of Chip
- Fixed spacing for askSearch
- Changed Qform
- Fixed Questions 'Carousel'

(08/27/20)
- Fixed style askSearch
- Fixed spacing of Questions
- Created toggle for Qform
- Fixed style Cursors
**/
var Latex = require('react-latex');


const useStyles = makeStyles({
    /**paper:{
        //boxShadow: "shadow",
        backgroundColor:"#FFFFFF",
        color: "#3485C3",
        //border:"none"
    },
    container:{
        backgroundColor:"#1bb3f4"
        //backgroundColor: "#0084c1"
    },
    buttonStyle:{
        color:"#3485C3",
        textTransform: "none",
		marginRight: "10px",
        "&:hover":{
            backgroundColor:"#0000003b"
        }
    },
    navTab:{
        textTransform: "capitalize",
        //marginRight: "10px"
    },
    signoutMargin:{
        marginRight:"3px"
    },**/
    paperMargin:{
        margin: "50px",
        padding: "45px 30px",
        transition:"width 2s",
    },
    buttonColor:{
        backgroundColor:"#6de5ff",
        "&:hover":{
            backgroundColor:"#ff9840",
            //color: "#FFFFFF"
        },
        height: "50px",
        width: "100%"
    }, 
    replyButtonStyle:{
        backgroundColor:"#6de5ff",
        "&:hover":{
            backgroundColor:"#ff9840",
            //color: "#FFFFFF"
        },
    }, 
    answersContainer:{
        backgroundColor:"#f8f9fa"
    },
    objectFitContain:{
        objectFit:"contain",
    },
	subjGreeting: {
		fontFamily: 'Roboto Slab',
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '48px',
		lineHeight: '63px',
		color: '#3485C3',
	},
	askSearch: {
		margin: "10px 30px",
		background:"#3485C3", 
		boxShadow:"0px 4px 5px rgba(0, 0, 0, 0.14)", 
		borderRadius:"205px", 
		color: "#F5F7FA" , 
		height:"67px",
		padding: "25px",
		
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '24px',
		lineHeight: '28px',
	},
	clickedClassTag: {
		marginRight: "10px",
		backgroundColor: "#FF9F24", 
		color: "#F5F7FA", 
		border: "1px solid rgba(0, 0, 0, 0.12)", 
        borderRadius: "16px",
        "&hover":{
            backgroundColor: "#F5F7FA", 
        }
	},
	classTags: {
		marginRight: "10px",
		backgroundColor: "#E7EEF7", 
		color: "#FF9F24", 
		border: "1px solid #FF9F24", 
		borderRadius: "16px",
	},
	header: {
		fontFamily: 'Roboto Slab',
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '36px',
		lineHeight: '47px',
		color: '#3485C3',
	},
	questionHeader:{
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '24px',
		lineHeight: '28px',
		
		width: "80%",
		color: "#3485C3",
	},
	qButton: {
		padding: "0",
		float: "right",
	},
	qTimestamp: {
		fontFamily: "Roboto",
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: '18px',
		lineHeight: '30px',

		color: '#9097AB',
	},
	rHeader: {
		fontFamily: "Roboto",
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '18px',
		lineHeight: '30px',

		color: '#3485C3',
	},
	points: {
		fontFamily: "Roboto",
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: '14px',
		lineHeight: '30px',

		color: '#9097AB',
	},
	votes: {
		fontFamily: "Roboto",
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '18px',
		lineHeight: '30px',

		color: '#3485C3',
	},
	reply: {
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: '16px',
		lineHeight: '30px',

		color: '#293150',
	},
	commentHeader: {
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '16px',
		lineHeight: '30px',

		color: '#293150',
	},
	submitButton: {
		width:'20%',
		borderRadius: '0 4px 4px 0',
		border: '2px solid #CCD1D9',
		background: '#FF9F24',
		height: '42px',
		
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: '14px',
		lineHeight: '24px',
		color: '#FFFFFF',
	},
	seeMore: {
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: '16px',
		lineHeight: '30px',

		color: '#9097AB',
		float: 'right',
	},
	qFormHeader:{
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'bold',
		fontSize: '24px',
		lineHeight: '28px',
		color: "#3485C3",
	},
	qFormButtons: {
		borderRadius: 4,
		border: '2px solid #FF9F24',
		background: '#FF9F24',
		fontFamily: 'Roboto',
		fontStyle: 'normal',
		fontWeight: 'normal',
		fontSize: '16px',
		lineHeight: '24px',
		color: '#F5F7FA',
    },
    cardStyle:{
        transition: "all 0.3s ease",
        height: "320px", 
        position: "relative", 
        "&:hover":{
            background:"rgba(255, 255, 255, 0.5)"
        }
    }
})

//Alert component for snackbar
function Alert(props) {
    return <MuiAlert elevation={1} variant="filled" {...props} />;
  }

//forum body
function Forum (){
    const [showqform, setShowqform] = useState(false);
    const [showtest, setShowtest] = useState(false);
    //const [showqformButton, setShowqformButton] = useState(false);
    const [showSearchbar, setShowSearchbar] = useState(false);
    const [subjects, setSubjects] = useState([]);
    //const [selectedChip,setSelectedChip] = useState(0);


    const fire = useContext(FirebaseContext);
    const click = useContext(ClickContext);


    const showtestRef = useRef();
    showtestRef.current =showtest;

    //console.log("watchlist",click.watchlist)

    const classes = useStyles();
    let chipTheme = classes.classTags;
    //console.log("Classes:",classes)

    useEffect(()=>{
        sessionStorage.setItem("mounted",0);
    },[])

    const [selectedTab, setSelectedTab] = useState(0);
    const [selectedChip, setSelectedChip] = useState(sessionStorage.getItem("chipSelect")?sessionStorage.getItem("chipSelect"):0);
    



    useEffect(()=>{
        if (fire.status==true){
            let z = [];
            const unsubscribe= fire.store.collection("users").doc(fire.auth.currentUser.uid).onSnapshot(
            documentSnapshot=>{
                //setSubArray(documentSnapshot.data().uSubjects);
                setSubjects(documentSnapshot.data().uSubjects);
                click.setSubjectCode(documentSnapshot.data().uSubjects[selectedChip]);
            });   

        return unsubscribe;
        }

        else{
        console.log("don cheadle");
        }
    },[fire.status,selectedChip]
    )

      const [open, setOpen] = React.useState(false);
  const [scroll, setScroll] = React.useState('paper');


	const handleChange = (event, newValue) => {
		setSelectedTab(newValue);
	};

	const handleDelete = (element,i) => {
        removeSub(element);
        setSelectedChip(i-1);
        sessionStorage.setItem("chipSelect",parseInt(i-1));
	};

	const handleClick = (element,i) => {
        //console.info('You clicked the Chip.',i);
        setSelectedChip(i);
        sessionStorage.setItem("chipSelect",parseInt(i));
        click.click("");
        click.setSubjectCode(element);
        click.setTextSearch([""]);
    };

    const handleClickOpen = (scrollType) => () => {
        setOpen(true);
        setScroll(scrollType);
      };
    
      const handleClose = () => {
        setOpen(false);
      };


      const descriptionElementRef = React.useRef(null);
      React.useEffect(() => {
        if (open) {
          const { current: descriptionElement } = descriptionElementRef;
          if (descriptionElement !== null) {
            descriptionElement.focus();
          }
        }
      }, [open]);

    //console.log("haha",sessionStorage.getItem("chipSelect"),selectedChip);
    

    async function removeSub (element){
        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            /* answers: [...c.content.answers,answer, imageRef.current?`<img src=${imageRef.current}/>`:], */
            uSubjects: fire.fieldValue.arrayRemove(element),
        });
    }
    
    //console.log("subjectcode",click.subjectCode);
    


        //console.log("subjectcode",subjects);
        
/*         if(click.subjectCode==""){
            click.setSubjectCode(subjects[0]);
        } */

    const b= subjects.map((element,i)=>
        <Chip classes={{root:`${selectedChip==i ? classes.clickedClassTag : chipTheme}`}} key={i} color="primary" variant="outlined" size="medium" icon={selectedChip==i ? <DoneIcon/> : {}} deleteIcon={subjects.length-1?<CancelIcon style={{color: selectedChip==i? "#E7EEF7" : "#FF9F24", width:15, height:15,marginRight: 10}} mini="true"/>:<></>} label={element} onClick={()=>handleClick(element,i)} onDelete={()=>handleDelete(element,i)}/>
    )

    return(
        <Grid container direction="column" style={{ background: "#E7EEF7" }}>
			{/**<Subject/>*/}
            {/* Navbar */}
{/*             <Grid item>
				<Navbar />
            </Grid>    */}             
			
			<Grid container item>
				<Grid item style={{padding: "50px 50px 25px"}}>
                    {b}
                    <Link to="/ss" style={{textDecoration:"none"}}>
                    <Chip clickable classes={{root:classes.classTags}} color="primary" variant="outlined" size="medium" icon={<AddIcon style={{marginLeft: 10, width:15, height:15}}/>} label="ADD" />
                    </Link>
{/* 					<Chip classes={{root:classes.clickedClassTag}} color="primary" variant="outlined" size="medium" icon={<DoneIcon/>} deleteIcon={<HighlightOffIcon style={{color: "#F5F7FA", width:15, height:15,marginRight: 10}} mini="true"/>} label="MATH21" onClick={handleClick} onDelete={handleDelete}/>
					<Chip classes={{root:classes.classTags}} color="primary" variant="outlined" size="medium" deleteIcon={<HighlightOffIcon style={{color: "#656D78", width:15, height:15,marginRight: 10}}/>} label="THEO11" onClick={handleClick} onDelete={handleDelete}/>
					<Chip classes={{root:classes.classTags}} color="primary" variant="outlined" size="medium" deleteIcon={<HighlightOffIcon style={{color: "#656D78", width:15, height:15,marginRight: 10}}/>} label="SocSc11" onClick={handleClick} onDelete={handleDelete}/>
					<Chip classes={{root:classes.classTags}} color="primary" variant="outlined" size="medium" deleteIcon={<HighlightOffIcon style={{color: "#656D78", width:15, height:15,marginRight: 10}}/>} label="CSCI22" onClick={handleClick} onDelete={handleDelete}/>
					<Chip classes={{root:classes.classTags}} color="primary" variant="outlined" size="medium" icon={<AddIcon style={{marginLeft: 10, width:15, height:15}}/>} label="ADD" onClick={handleClick}/>
 */}				
                </Grid>
			</Grid>

            {/**Jumbotron Welcome*/}
            <Grid container item spacing={0} style={{padding:"10px 50px"}} alignItems="center">    
                <Grid item container direction="column">
                    <Grid item>
                        <Typography style={{whiteSpace:"pre-line"}} classes={{root: classes.subjGreeting}} variant="h3">
                            {click.textSearch[0]==""?`Welcome to the ${click.subjectCode} Forum!`:`Search Results for: "${click.textSearch.join(" ")}"`}
                        </Typography>
                    </Grid>
					<Grid item style={{ margin: '65px auto', objectFit: 'contain'}}>
						<Chip classes={{root: classes.askSearch}} variant="outlined" size="medium" icon={<AddIcon style={{color: "#F5F7FA"}}/>} label={`Ask a ${click.subjectCode} question`} onClick={()=>click.setShowqformButton(true)}/>
						<Chip classes={{root: classes.askSearch}} variant="outlined" size="medium" icon={<SearchIcon style={{color: "#F5F7FA"}}/>} label={`Search ${click.subjectCode} questions`} onClick={()=>click.setShowSearchbar(true)}/>
					</Grid>
					{/** Qform **/}

                <Backdrop open={click.showqformButton} style={{zIndex:1101}}>
                    <Qform/>
                </Backdrop>
                <Backdrop open={click.showSearchbar} style={{zIndex:1101}}>
                    <Search/>
                </Backdrop>
{/*                 <Backdrop open={click.showqformButton} style={{zIndex:1101}}>
                    <Searchbar/>
                </Backdrop> */}
					{/**
                    <Grid item>
                        <Searchbar />
                    </Grid>
                    <Grid item>
                        <Button onClick={()=>setShowqformButton(!showqformButton)} classes={{root: classes.buttonColor}} variant="contained">{`Ask your ${click.search} ${click.numSubject?click.numSubject:""} question`}</Button>
                    </Grid>**/}
                </Grid>
                <FilterContextProvider>
				<Grid container item style={{margin: 'auto', paddingBottom:65}} justify="center" alignItems="center">
					<CursorL/>
					{/**Question Cards**/}
					<Questions />
					<CursorR/>
                
				</Grid>
                </FilterContextProvider>
            </Grid>
			
{/*  			<Grid id="question"  container item xs style={{padding: "65px 50px", background:"#F5F7FA", marginTop:"65px"}} direction="column">
				<Typography classes={{root:classes.header}}>Question</Typography>
				<Grid container item justify="center" style={{margin: "25px auto 15px", width:"740px", borderRadius: "8px",}} >
					<MainQuestion />
				</Grid>
			</Grid>  */}

{/*             <Grid item container direction="column" style={{padding:"0 50px 55px", background: "#F5F7FA"}} classes={{root:classes.answersContainer}}>
                <Grid item style={{marginBottom:"30px"}}>
                    <Typography classes={{root:classes.header}}>Answers</Typography>
                </Grid>
				<Masonry/>
            </Grid> */}
        
        </Grid>
    
    )
}

//the submission form
function Qform(){
    const fire = useContext(FirebaseContext);
    const [question, setQuestion] = useState("");
    const [description, setDescription] = useState("");
    const [code, setCode] = useState("");
    const [loading,setLoading] = useState(false);

    const [image,setImage] = useState(null);
    const [imageReset,setImageReset] = useState("");

    const [open, setOpen] = React.useState(false);
    const [errorAlert, setErrorAlert] = React.useState(false);

    const click = useContext(ClickContext);

    //console.log("Img",image);

    const classes=useStyles();

    const imageRef = useRef();
    imageRef.current = image;

    const inputEl = useRef();
    function handleClick () {
        // `current` points to the mounted text input element
        inputEl.current.click();
    };


    const imagePreview = useRef({src:""});
    function previewFile(file) {
        const preview = imagePreview.current;
        const reader = new FileReader();
      
        reader.addEventListener("load", function () {
          // convert image file to base64 string
          preview.src = reader.result;
        }, false);
      
        if (file) {
          reader.readAsDataURL(file);
          
        }
      }

      const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
        setErrorAlert(false);
      };

    async function saveIt(){
        setLoading(true);

        if (image) {
        const files = image;
        const data = new FormData();

        data.append('file',files[0]);
        data.append('upload_preset','imgupload');
        setLoading(true);

        const res = await fetch(
            'https://api.cloudinary.com/v1_1/scholarbuddy/image/upload',
            {
                method: 'POST',
                body: data
            }
        )
        
        const file = await res.json();
        setImage(file.secure_url);
        }

        //console.log("Saving");
        const addPost = await fire.store.collection("forum").add({
            question: question,
            description: description,
            subjectCode: `${click.subjectCode}`,
            timestamp: fire.fieldValue.serverTimestamp(),
            img: imageRef.current,
            keywords: question[question.length-1]=="?" || question[question.length-1]=="!"||question[question.length-1]=="." ? ["",...question.substring(0,question.length-1).toLowerCase().split(" ")] : ["",...question.toLowerCase().split(" ")],
            savedBy: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
            flaggedBy: [],
            modans: {
                text:"No leaks. Maintain respect.",
                timestamp: fire.fieldValue.serverTimestamp()
            },
            answerers:[],
            tutorans: [],
            answersLength:0,
            show: true,
            user: fire.auth.currentUser.uid
        });

        await fire.store.collection("forum").doc(addPost.id).collection("answers").add({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
            user: "System Moderator",
            answer: 
            `Welcome to Studybuddy, an anonymous online forum where you can discuss any subject in the Ateneo! To maintain a conducive space for constructive discussions, here are a few ground rules:
            
            1. Please respect one another. Let us help one another towards the path of learning. 
            2. Any form of academic dishonesty is not allowed. Users who leak exams/assignment answers will be banned permanently.
            3. Askers are expected to show their preliminary work.
            4. Memes and other inappropriate content are not allowed and will be removed.

            Here in Studybuddy, we study as a community. Happy learning!
            `,
            img: null,
            upvoteLength:0,
            upvotes: [],
            downvotes:[],
            timestamp: fire.fieldValue.serverTimestamp(),
            systemMod: true
        });

        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uQuestions: fire.fieldValue.arrayUnion(addPost.id)
        });

        await fire.store.collection("stats").doc("forum collection size").update({
            size: fire.fieldValue.increment(1)
        })

        setImage(null);
        setImageReset("");
        imagePreview.current.src = "";
        setQuestion("");
        setDescription("");
        setCode("");
        setLoading(false);
        setOpen(true);
    }

    return(
        <Grid container item style={{borderRadius:10,margin: "65px 65px", position:"absolute",overflow:"scroll",background: "White", padding: "20px"}} md={5} spacing={2} direction="column">
			<Grid item>
                <CancelIcon onClick={()=>(click.setShowqformButton(false))} role="button"/>
            </Grid>
            <QuestionAnswerIcon style={{ margin: "auto", width: "120px", height: "120px", color: "#3485C3"}}/>
			<Typography classes={{root:classes.qFormHeader}} style={{ margin: "24px auto 0 auto"}}>Can't find what you're looking for?</Typography>
			<Typography classes={{root:classes.qFormHeader}} style={{ margin: "0 auto 48px auto"}}>Start a new discussion!</Typography>
            <Grid item xs={12}>
                <TextField style={{border: '2px #CCD1D9',borderRadius:10}} value={question} onChange={e=>setQuestion(e.target.value)} variant="outlined" fullWidth label="Type in your question here"/>
            </Grid>
            <Grid item xs={12}>
                <TextField style={{border: '2px #CCD1D9',borderRadius:10}} variant="outlined" value={description} onChange={e=>setDescription(e.target.value)} fullWidth inputProps={{rows:"6"}} rowsMax={6} multiline label="Add a description for your question here"/>
            </Grid>
            <Grid xs={12} item>
				{loading?
                <CircularProgress style={{ float: "right"}}/>:
                <>
                <Button style={{ float: "right", marginLeft: "10px"}} classes={{root:classes.qFormButtons}} variant="contained" onClick={()=>{if (question) {saveIt();setLoading(true);} else return setErrorAlert(true)}}>Submit Question</Button>
                <input value={imageReset} onChange={e=>{previewFile(e.target.files[0]);setImage(e.target.files); setImageReset(e.target.value); }} ref={inputEl} id="upload-pic" accept="image/*" type="file"/>     
				<Button style={{ float: "right"}} classes={{startIcon: classes.signoutMargin , root: classes.qFormButtons}} startIcon={<AttachFileIcon/>} onClick={handleClick}>
                    Attach an Image
                </Button>
				
				<div style={{clear: "both"}}></div> 
				<div></div>
                </>
                }
            </Grid>
            <Grid item>
            {image  ?
                    <IconButton onClick={()=>{
                        imagePreview.current.src = "";
                        setImage(null);
                        setImageReset("");}}>
                            <CancelRoundedIcon/>
                    </IconButton>:
                    ""
                    }
            <img  ref={imagePreview} src="" style={{maxHeight:"100px", maxWidth: "100%"}} />
            </Grid>
            
                    <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                Question successfully posted! You may view it in your saved posts in My Account.
                </Alert>
            </Snackbar>

                <Snackbar open={errorAlert} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="warning">
            You can't leave the question field empty.
            </Alert>
            </Snackbar> 

            {/* <Typography>{`Subject: ${click.search}`}</Typography> 
            <Grid item xs={6} style={{textAlign:"right"}}>
                {loading?
                <CircularProgress/>:
                <Button classes={{root:classes.replyButtonStyle}} variant="contained" onClick={()=>{if (question&&description) {saveIt();setLoading(true);} else return setErrorAlert(true)}}>Post</Button>
                }
            </Grid>
            <Grid item style={{}}>
                    <img  ref={imagePreview} src="" style={{height: "150px",maxHeight:"100%", maxWidth: "100%"}} />
                    {image?
                    <IconButton onClick={()=>{
                        imagePreview.current.src = "";
                        setImage(null);
                        setImageReset("");}}>
                            
                            <CancelRoundedIcon/>
                    </IconButton>:
                    ""
                    }
                </Grid>   

        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Question successfully posted!
        </Alert>
      </Snackbar>

      <Snackbar open={errorAlert} autoHideDuration={6000} onClose={handleClose}>
<Alert onClose={handleClose} severity="warning">
All fields must be filled up!
</Alert>
</Snackbar>  */}


        </Grid>


        )
}

function Search(){

    const [prelim, setPrelim] =useState("");
    const [open, setOpen] =useState(false);
    const [errorAlert, setErrorAlert] =useState(false);

    const click = useContext(ClickContext);

    const classes=useStyles();


    const prelimRef = useRef();
    prelimRef.current = prelim;

    function searchIt (){
        //console.log("From searchIt:",...prelimRef.current.split(" "));

        if (prelimRef.current.split(" ").length<=10){

        click.setTextSearch(
            prelimRef.current[prelimRef.current.length-1]=="?" ||
            prelimRef.current[prelimRef.current.length-1]=="!"||
            prelimRef.current[prelimRef.current.length-1]=="." ? 
            [...prelimRef.current.substring(0,prelimRef.current.length-1).toLowerCase().split(" ")] :
            [...prelimRef.current.toLowerCase().split(" ")]
            );

            click.setShowSearchbar(false);
            setPrelim("");
        }

        else{
            setErrorAlert(true);
        }
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
        setErrorAlert(false);
      };


    return(
        <Grid container item style={{borderRadius:10, margin: "65px 65px", background: "White", padding: "30px 30px"}} md={5} spacing={2} direction="column">
                <Grid>
            <CancelIcon onClick={()=>(click.setShowSearchbar(false))} role="button"/>
        </Grid>
        <SearchIcon style={{ margin: "auto", width: "120px", height: "120px", color: "#3485C3"}}/>
        <Typography classes={{root:classes.qFormHeader}} style={{ margin: "24px auto 0 auto"}}>{`Have a specific ${click.subjectCode} question in mind?`}</Typography>
        <Typography classes={{root:classes.qFormHeader}} style={{ margin: "0 auto 48px auto"}}>Try searching!</Typography>
        <Grid item xs={12}>
            <TextField onKeyPress={e=>{if (e.key==="Enter") {searchIt();}}} style={{border: '2px #CCD1D9',}} value={prelim} onChange={e=>setPrelim(e.target.value)} variant="outlined" fullWidth label="Search here"/>
        </Grid>
        
                    <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity="success">
                        Question successfully posted!
                        </Alert>
                    </Snackbar>

                    <Snackbar open={errorAlert} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="warning">
                    Your query must not exceed ten words.
                    </Alert>
                    </Snackbar> 
    </Grid>

    )
}

//data-getter of questions
function useData (){
    const [loading, setLoading] = useState(true);
    const fire = useContext(FirebaseContext);
    //const [name, setName] = useState([{id:"", content:{question:"",description:"", code:"",number:"",modans:[""], answers:[""],author:"",show:true,userflag:[]}}]);
    const [name, setName]= useState([]);
    const filter = useContext(FilterContext);
    const click = useContext(ClickContext);


    const [answer,setAnswer]=useState([]);

    const [querySnapshotSize, setQuerySnapshotSize]= useState(0);

    const querySnapshotSizeRef = useRef();
    querySnapshotSizeRef.current = querySnapshotSize;

    const [nextData,setNextData] = useState(0);
    const [prevData,setPrevData] = useState(0);

    const nextDataRef = useRef();
    nextDataRef.current = nextData;

    const prevDataRef = useRef();
    prevDataRef.current = prevData;

    const pageRef = useRef();
    pageRef.current = filter.page;

    const watchlistRef = useRef();
    watchlistRef.current = click.watchlist;

    const nameRef = useRef();
    nameRef.current = name;


//realtime
useEffect(()=>{
    setLoading(true);

    if( pageRef.current == "next"){
    const afterQuery = fire.store.collection("forum").where("show","==",true).where("subjectCode","==",`${click.subjectCode?click.subjectCode:""}`).where("keywords","array-contains-any",click.textSearch).orderBy(click.orderBy,"desc").startAt(filter.cursor);
    //console.log("haha2",click.subjectCode);

    const unsubscribe = afterQuery.limit(4).onSnapshot(querySnapshot => {
        //console.log("From Snapshot, search value",click.search) .orderBy("timestamp","desc") ${click.subjectCode} .orderBy("number") .where("keywords","array-contains-any",click.textSearch)
        //console.log("From Snapshot, z value",querySnapshot)
        let z= [];
        querySnapshot.forEach(queryDocumentSnapshot => {
            z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
        });
        //if (z.length>0) {setName(z);} 
        setName(z);
        //console.log("From Snapshot, z value",z)
        setQuerySnapshotSize(querySnapshot.size);
        setNextData(querySnapshot.docs[querySnapshot.size-1]);
        //console.log("From UseData, nextData:",nextDataRef.current)
        setPrevData(querySnapshot.docs[0]);
        //console.log("From UseData, prevData:",prevDataRef.current)
        //console.log("From UseData, name:",name)
        setLoading(false);
        //click.click(z[0].id);
        //return unsubscribe;
        //console.log("Normal IF is firing");

    }
    )

    return unsubscribe



    //return unsubscribe;

}

    else if (pageRef.current == "prev"){
        const beforeQuery = fire.store.collection("forum").where("show","==",true).where("subjectCode","==",click.subjectCode).where("keywords","array-contains-any",click.textSearch).orderBy(click.orderBy,"desc").endAt(filter.cursor);

        const unsubscribe = beforeQuery.limitToLast(4).onSnapshot(querySnapshot => {
            //console.log("From Snapshot, search value",click.search)
            let z= [];
            querySnapshot.forEach(queryDocumentSnapshot => {
                z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
            });
            //if (z.length>0) {setName(z);} 
            setName(z);
            //console.log("From Snapshot, z value",z)
            setQuerySnapshotSize(querySnapshot.size);
            setNextData(querySnapshot.docs[querySnapshot.size-1]);
            //console.log("From UseData, nextData:",nextDataRef.current)
            setPrevData(querySnapshot.docs[0]);
            //console.log("From UseData, prevData:",prevDataRef.current)
            //console.log("From UseData, name:",name)
            setLoading(false);
            //click.click(z[0].id);
        }
        )
        return unsubscribe
        }
        //console.log("Normal IF is firing",click.subjectCode);

},[fire.status,fire.store,click.subjectCode,click.search,filter.cursor,click.numCompare,click.numSubject,click.orderBy,click.textSearch,click.watchlist]);

    return {name,loading, nextDataRef,prevDataRef,querySnapshotSizeRef};
}

//question cards
function Questions (){
    const name = useData().name;
    //console.log("@passed to questions",name)
    const loading = useData().loading;
    const click = useContext(ClickContext);
    const filter = useContext(FilterContext);


    const classes= useStyles();
    
    if (loading==false && name.length){
        //console.log("qc",Object.keys(name[0].content.answers))
        const b =name.map ((element,i)=>
            <Grid item md={3} key={i} /* onClick={()=> click.click(`${element.id}`)} */>
               {/* <Link to={`question/${element.id}`}> */}
               <Link to={`/question/${element.id}`} style={{color: '#3485C3',textDecoration:"none"}} /* onClick={(event) => {event.preventDefault(); window.open(window.location.origin+`/question/${element.id}`);}} */>

                <Card role="button" classes={{root:classes.cardStyle}}
					//style if not clicked >> style={{background:'rgba(255, 255, 255, 0.8)',}}
				>
                    <CardContent style={{fontFamily: "Roboto", fontStyle: 'normal',fontWeight: 'normal',color: '#3485C3', padding:28}}
						//style if not clicked >> style={{color:'rgba(52, 133, 195, 0.7)',}}
					>
                            <Typography variant="h6" style={{fontSize: '24px', lineHeight: '28px'}}>
                            <Latex>{element.content.question}</Latex>
                            </Typography>
                        
						<CardContent style={{position: "absolute", bottom: "0", width: "77.1%", padding: "0 0 28px 0", fontSize: '18px', lineHeight: '21px',}}>
							<Typography style={{ float: "left",}} variant="caption">
							{`${element.content.subjectCode}`}
							</Typography>
							<Typography variant="caption" style={{ float: "right",}}>
							{`
							Replies: ${element.content.answersLength}`}
							</Typography>
							<div style={{clear: "both"}}></div> 
							<div></div>
						</CardContent>
                        <Typography variant="caption">
                        {element.content.tutorans.length ? "A tutor has replied!" : ""}
                        </Typography>
                    </CardContent>
                </Card>
                </Link>

                {/* </Link> */}
            </Grid>
        );

        return(

            <Grid item xs={10} style={{width: "100%", margin:"0 auto"}} container justify="center" spacing={2}>
                <Grid container item xs={12} justify="flex-start">
                <FormControl style={{width:150}}>
                <InputLabel id="demo-simple-select-helper-label">Sort by</InputLabel>
                    <Select value={click.orderBy} onChange={(e)=>{click.setOrderBy(e.target.value);click.setPage("next");click.resetCount();e.target.value=="timestamp"?click.setCursor(new Date()):click.setCursor(1000000)}}>
                        <MenuItem value={"timestamp"}>Latest</MenuItem>
                        <MenuItem value={"answersLength"}>Most answered</MenuItem>
                    </Select>
                </FormControl>
                </Grid>
            {b}
            </Grid>
        )
        }

	else if (loading ==false){
		return(
			<Grid item md={6} style={{marginLeft:"40px"}}container justify="center" spacing={2}>
				<Grid item md={6}>
					<Card>
						<CardContent>
						<Typography variant="h6">There are no questions here as of the moment. Ask the first one!</Typography>
						</CardContent>
					</Card>
				</Grid>
			</Grid>
		)
	}

	else {
		return(
			<Grid item md={6} container justify="flex-end" spacing={2}>
				<Grid item md={6}>
					<Skeleton variant="rect"/>
				</Grid>
				<Grid item md={6}>
					<Skeleton variant="rect"/>
				</Grid>
				<Grid item md={6}>
					<Skeleton variant="rect"/>
				</Grid>
				<Grid item md={6}>
					<Skeleton variant="rect"/>
				</Grid>
			</Grid>
		)
	}

}

//pagination
function CursorL(){

    const click = useContext(ClickContext);
    const filter = useContext(FilterContext);
    const nextDataRef = useData().nextDataRef;
    const prevDataRef = useData().prevDataRef;
    const querySnapshotSizeRef = useData().querySnapshotSizeRef;
    
    //const [count, setCount] = useState(0);

    const loading = useData().loading;

    {/**const next = () =>{
        //console.log("azizi");
        click.setPage("next")
        click.setCursor(nextDataRef.current)
        click.setCount(1)
        //console.log("azizi",nextDataRef.current.data());
	}**/}

    const prev = () =>{
        //console.log("berkeley");
        filter.setPage("prev")
        if (!loading) {filter.setCount(-1)}
        filter.setCursor(prevDataRef.current)
        }

	return(
		<Grid item container justify="center" xs={1}>
			<Grid item>
				{filter.count && !loading ?
				<IconButton onClick={prev}>
					<ChevronLeftIcon style={{color:"#3485C3", fontSize:"70px", }}/>
				</IconButton>:
				<IconButton disabled>
					<ChevronLeftIcon style={{color:"rgba(0, 0, 0, 0.26)", fontSize:"70px"}}/>
				</IconButton>}
			</Grid>
		</Grid>
	)
}

function CursorR(){

    const click = useContext(ClickContext);
    const nextDataRef = useData().nextDataRef;
    const prevDataRef = useData().prevDataRef;
    const querySnapshotSizeRef = useData().querySnapshotSizeRef;
    const filter = useContext(FilterContext);
    //const [count, setCount] = useState(0);

    const loading = useData().loading;

    const next = () =>{
        //console.log("azizi");
        filter.setPage("next")
        filter.setCursor(nextDataRef.current)
        filter.setCount(1)
        //console.log("azizi",nextDataRef.current.data());
	}
	
	return (
		<Grid item container justify="center" xs={1}>
			<Grid item>
			{querySnapshotSizeRef.current ==4 && !loading?
				<IconButton onClick={next}>
					<ChevronRightIcon style={{color:"#3485C3", fontSize:"70px", }}/>
				</IconButton>:
				<IconButton disabled>
					<ChevronRightIcon style={{color:"rgba(0, 0, 0, 0.26)", fontSize:"70px"}}/>
				</IconButton>}
			</Grid>
		</Grid>
	)
}

//QuestionBlock
function MainQuestion (){
    const loading = useData().loading;
    const name = useData().name;

    const click = useContext(ClickContext);
    const fire = useContext(FirebaseContext);

    const d= name.find(name => name.id==click.forClickId);
	
	const classes=useStyles();

    let c = [];
    //console.log("My C:", name.length);

    d? c=d : c=name[0]

/*     useEffect(()=>{
        if (click.forClickId=="" && name.length){
            click.click(name[0].id);
        }
    },[]) */


    async function pickIt(uid){
        //console.log("Add to Pick");
        await fire.store.collection("forum").doc(click.forClickId).update({
            savedBy: fire.fieldValue.arrayUnion(uid),
        });

        await fire.store.collection("users").doc(uid).update({
            uQuestions: fire.fieldValue.arrayUnion(click.forClickId),
        });
    }
    async function removePick(uid){
        //console.log("Remove Pick");
        await fire.store.collection("forum").doc(click.forClickId).update({
            savedBy: fire.fieldValue.arrayRemove(uid),
        });

        await fire.store.collection("users").doc(uid).update({
            uQuestions: fire.fieldValue.arrayRemove(click.forClickId),
        });
    }
    

    async function userFlagIt(uid){
        await fire.store.collection("forum").doc(click.forClickId).update({ 
            flaggedBy: fire.fieldValue.arrayUnion(uid),
        });
    }

    async function removeUserFlagIt(uid){
        await fire.store.collection("forum").doc(click.forClickId).update({ 
            flaggedBy: fire.fieldValue.arrayRemove(uid),
        });
    }

    async function modFlagIt(){
        await fire.store.collection("forum").doc(click.forClickId).update({ 
            show: false,
        });
    }

    if (loading===false && name.length){
        return(
				<Card style={{padding:"35px 40px", background: "#FFFFFF", borderRadius: "8px"}}>
					{/**Question style={{marginTop: "30px"}}*/}
					<Grid item>
						{c.content.img? 
							<Grid item container justify="center">
								<img style={{maxWidth: "100%", maxHeight:350}} src={c.content.img}/> 
							</Grid>:
						""}
					</Grid>
					<Grid container item style={{margin:"40px 0 10px"}} direction="row">
						<Typography classes={{root: classes.questionHeader}}>
							{c.content.question}
						</Typography>						
					</Grid>
                    <Typography classes={{root: classes.qTimestamp}}>{
                        (new Date(c.content.timestamp.seconds*1000)).toLocaleString()
                        }</Typography>
                    
                    <Grid container item spacing={2}>
                        {/**Save Button */}
                        <Grid item>
                        {c.content.savedBy.includes(`${fire.auth.currentUser.uid}`) ? 
							<Button classes={{root: classes.qButton}} style={{color: "#3485C3"}}onClick={(e)=>{e.preventDefault();removePick(fire.auth.currentUser.uid);}}
                            startIcon={<TurnedInIcon fontSize="large" style={{fontSize:"25px",color: "#3485C3"}}/>}>Unsave Post
                            </Button>
							: <Button classes={{root: classes.qButton}} style={{color: "#3485C3"}}onClick={(e)=>{e.preventDefault();pickIt(fire.auth.currentUser.uid);}}
								startIcon={<TurnedInNotIcon fontSize="large" style={{fontSize:"25px",color: "#3485C3"}}/>}>Save Post
							</Button>}
						</Grid>

                        <Grid item>
							{/**Flag Button */}
							{
							//mod flag {`Flagged by ${c.content.userflag.length} user/s! Remove?`}  c.content.userflag.includes(fire.auth.currentUser.uid)
							fire.admin || fire.moderator? 
								<Button classes={{root: classes.qButton}} onClick={(e)=>{e.preventDefault(); modFlagIt();}}
                                    startIcon={<DeleteForeverRoundedIcon fontSize="large" style={{fontSize:"25px",color: "#3485C3"}}/>}>Flagged by {c.content.flaggedBy.length} users
								</Button>
							
							//else, user flag
							:fire.status?
                                    c.content.flaggedBy.includes(`${fire.auth.currentUser.uid}`) ? 
                                    <Button classes={{root: classes.qButton}} style={{color: "#3485C3"}} onClick={(e)=>{e.preventDefault(); removeUserFlagIt(fire.auth.currentUser.uid);}}
                                    startIcon={<FlagIcon fontSize="large" style={{fontSize:"25px",color: "#3485C3"}}/>}>Unreport Post
                                    </Button>
                                    : <Button classes={{root: classes.qButton}} style={{color: "#3485C3"}} onClick={(e)=>{e.preventDefault(); userFlagIt(fire.auth.currentUser.uid);}}
                                    startIcon={<FlagOutlinedIcon fontSize="large" style={{fontSize:"25px",color: "#3485C3"}}/>}>Report Post
                                    </Button>
								:<div></div>}
                        </Grid>
					</Grid>
					<Grid item style={{ margin: "16px 0 8px" }}>
						<Typography variant="h6" style={{whiteSpace:"pre-line"}}>
							<AutoLinkText text={c.content.description}/>
        {/*                             {c.content.description.length > 450?
						//onClick={()=>{handleExpandClick(i); click.click(c.id)}}  <Button classes={{root: classes.seeMore}} //onClick={()=>{handleExpandClick(i); click.click(c.id)}} startIcon={<KeyboardArrowDownIcon/>}>See More</Button>
                         <a href="facebook.com">See More</a>:<div></div>
                        } */}
						</Typography>
					</Grid>
	            </Card>
        )
    }

    else {
        return(
            ""
        )
    }
}

//Masonry
function Masonry(){
	const MasonryLayout = props => {
	  const columnWrapper = {};
	  const result = [];

	  // create columns
	  for (let i = 0; i < props.columns; i++) {
		columnWrapper[`column${i}`] = [];
	  }
	  
	  // divide children into columns
	  for (let i = 0; i < props.children.length; i++) {
		const columnIndex = i % props.columns;
		columnWrapper[`column${columnIndex}`].push(
		  <div style={{ marginBottom: `${props.gap}px`}}>
			{props.children[i]}
		  </div>
		);
	  }
	  
	  // wrap children in each column with a div
	  for (let i = 0; i < props.columns; i++) {
		result.push(
		  <div
			style={{
			  marginLeft: `${i > 0 ? props.gap : 0}px`,
			  flex: 1,
			}}>
			{columnWrapper[`column${i}`]}
		  </div>
		);
	  }
	  return (
		<div style={{ display: 'flex' }}>
		  {result}
		  
		</div>
	  );
	}

	MasonryLayout.propTypes = {
	  columns: PropTypes.number.isRequired,
	  gap: PropTypes.number.isRequired,
	  children: PropTypes.arrayOf(PropTypes.element),
	};

	MasonryLayout.defaultProps = {
	  columns: 2,
	  gap: 20,
	};
	
	
	{/**From Answers()**/}
	const name = useData().name;
    //const name = nameOrig.slice();
    const loading = useData().loading;
    const click = useContext(ClickContext);
    const fire = useContext(FirebaseContext);
    const [index, setIndex] = useState(0);

    const [subload, setSubload] =useState(false);
    const [answer, setAnswer] = useState("");
    const [comment, setComment] = useState("");
    const [image,setImage] = useState(null);
    const [imageReset,setImageReset] = useState("");
    const [imgSrc,setImgSrc] = useState("");

    const [forumAnswer, setForumAnswer] =useState("");

    const imageRef = useRef();
    imageRef.current = image;
   
    const [addToWatchlistLoad, setAddToWatchlistLoad] = useState(false);

    const classes = useStyles();
    const [expanded, setExpanded] = useState(false);

    const [display,setDisplay] = useState(false);
    const [errorAlert, setErrorAlert] = React.useState(false);
    const [errorTwoAns, setErrorTwoAns] = React.useState(false);
    const [errorUpvote, setErrorUpvote] = React.useState(false);
    const [errorDownvote, setErrorDownvote] = React.useState(false);

    const [commentIcon,setCommentIcon] = useState(false);

    const [answersLimit,setAnswersLimit] = useState(5);

    const answersLimitRef = useRef();
    answersLimitRef.current=answersLimit;

    const [upvote,setUpvote] = useState(false);
    const [downvote,setDownvote] = useState(false);

    const inputComment = useRef();

    const [seeMoreLoad,setSeeMoreLoad]=useState(false);

    const [question, setQuestion]=useState();

    //console.log("molla",name)
    useEffect(()=>{
        if (click.forClickId != ""){
        const unsubscribe = fire.store.collection("forum").doc(click.forClickId).collection("answers").orderBy("upvoteLength","desc").limit(answersLimitRef.current).onSnapshot(querySnapshot => {
            //console.log("From Snapshot, search value",click.search) id:queryDocumentSnapshot.id, content:
            let z= [];
            querySnapshot.forEach(queryDocumentSnapshot => {
                z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
            });
            setAnswer(z);
            //console.log("murky",answer)
            setTimeout(() => {
                setSeeMoreLoad(false);
              }, 500);
        }
        )

        return unsubscribe;
        }

        else if (name[0]) {
            const unsubscribe = fire.store.collection("forum").doc(name[0].id).collection("answers").orderBy("upvoteLength","desc").limit(answersLimitRef.current).onSnapshot(querySnapshot => {
                //console.log("From Snapshot, search value",click.search) id:queryDocumentSnapshot.id, content:
                let z= [];
                querySnapshot.forEach(queryDocumentSnapshot => {
                    z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
                });
                setAnswer(z);
                //console.log("murky",answer)
                setTimeout(() => {
                    setSeeMoreLoad(false);
                  }, 500);
            }
            )

            return unsubscribe;
        }

    },[click.forClickId, name,answersLimitRef.current])



    function handleExpandClick(i) {
      commentIcon?setExpanded(false):setExpanded(i);
      setCommentIcon(!commentIcon);
    };

    const d= name.find(name => name.id==click.forClickId);
    let c = [];
    d? c=d : c=name[0] 

    const nameRef = useRef();
    nameRef.current = name;
    const inputEl = useRef(null);


    useEffect(() => {
        if (inputEl.current) {
          inputEl.current.focus();
          inputEl.current.selectionStart = forumAnswer.length;
          inputEl.current.selectionEnd = forumAnswer.length;
        }

        if (inputComment.current){
            inputComment.current.focus();
            inputComment.current.selectionStart = comment.length;
            inputComment.current.selectionEnd = comment.length;
        }
      }, [forumAnswer,comment]);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setErrorAlert(false);
        setErrorTwoAns(false);
        setErrorUpvote(false);
        setErrorDownvote(false);
      };

    async function replyIt(text,cid){
        //console.log("index",i);
        setSubload(true);

/*         if (fire.tutor){
            c.content.modans[i].replies = [...c.content.modans[i].replies,answer]
            await fire.store.collection("questions").doc(click.forClickId).update({
                modans: c.content.modans,
                timestamp:fire.fieldValue.serverTimestamp(),
            });
        }  */

        if (image){
            const files = image;
            const data = new FormData();
    
            data.append('file',files[0]);
            data.append('upload_preset','imgupload');
    
            const res = await fetch(
                'https://api.cloudinary.com/v1_1/scholarbuddy/image/upload',
                {
                    method: 'POST',
                    body: data
                }
            )
            
            const file = await res.json();
            setImage(file.secure_url);
            }
        

        const answerId = await fire.store.collection("forum").doc(click.forClickId===""?c.id:click.forClickId).collection("answers").add({ 
            subjectCode: c.content.subjectCode,
            questionId: c.id,
            question: c.content.question,
            user: fire.auth.currentUser.uid,
            answer: text,
            img: imageRef.current,
            upvoteLength:0,
            upvotes: [],
            downvotes:[],
            timestamp: fire.fieldValue.serverTimestamp()
        });

        setSubload(false);

        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uAnswers: fire.fieldValue.arrayUnion(answerId.id)
        });


        if (fire.tutor){
            await fire.store.collection("forum").doc(click.forClickId===""?c.id:click.forClickId).update({ 
                tutorans: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
                answersLength: fire.fieldValue.increment(1)
            });
        }

        await fire.store.collection("forum").doc(click.forClickId===""?c.id:click.forClickId).update({ 
            answerers: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
            answersLength: fire.fieldValue.increment(1)
        });

        
        setForumAnswer("");
        setImage(null);
        setImageReset("");
        setImgSrc(null);

    }

    async function commentIt(ansid){
        await fire.store.collection("forum").doc(click.forClickId===""?c.id:click.forClickId).collection("answers").doc(ansid).update({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
        commentsMap:fire.fieldValue.arrayUnion({comment:comment,timestamp:new Date()})
        })

        setComment("");
    }

    async function downvoteIt(author,uid,ansid,inc){
        await fire.store.collection("forum").doc(click.forClickId).collection("answers").doc(ansid).update({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
            upvotes: fire.fieldValue.arrayRemove(uid),
            downvotes: inc?fire.fieldValue.arrayUnion(""):fire.fieldValue.arrayUnion(uid),
            upvoteLength: fire.fieldValue.increment(-1)
        });

        await fire.store.collection("users").doc(author).update({
            upvotes: fire.fieldValue.increment(-1)
        })
        //setDownvote(true);
    }

    async function upvoteIt(author,uid,ansid,inc){

        await fire.store.collection("forum").doc(click.forClickId).collection("answers").doc(ansid).update({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
            upvotes: inc?fire.fieldValue.arrayUnion(""):fire.fieldValue.arrayUnion(uid),
            downvotes: fire.fieldValue.arrayRemove(uid),
            upvoteLength: fire.fieldValue.increment(1)
        });

        await fire.store.collection("users").doc(author).update({
            upvotes: fire.fieldValue.increment(1)
        })
        //setUpvote(true);
    }


    //console.log("expanded",expanded)
	
	function addForm(array, newItem) {
			return [newItem, ...array.slice(0)];
		}
		
	function checkForm(vote) {
		if (vote === '-0') {
			return true;
		}
    }


    const imgClick = useRef();
    function handleClick () {
        // `current` points to the mounted text input element
        imgClick.current.click();
    };

    
    const imagePreview = useRef({src:""});
    function previewFile(file) {
        const preview = imagePreview.current;
        const reader = new FileReader();
      
        reader.addEventListener("load", function () {
          // convert image file to base64 string
          preview.src = reader.result;
          setImgSrc(preview.src);
        }, false);
      
        if (file) {
          reader.readAsDataURL(file);

          
        }
      }
	
    if (loading==false && name.length){
        
        //replies list
        //const arr = c.content.answers.sort(function(a,b) {return (a.votes < b.votes) ? 1 : ((b.votes < a.votes) ? -1 : 0);} )
		//const arr = answer.sort(function(a,b) {return (a.votes < b.votes) ? 1 : ((b.votes < a.votes) ? -1 : 0);} ) value={description} onChange={e=>setDescription(e.target.value)}
        //answers
        //console.log("hiho",inputEl.current?inputEl.current.val():"")
        if (answer&&c.content.answerers){
            const sortedAnswers = addForm(answer, {answer: "", votes: '-0', replies: []})
            return (
                <>
                <MasonryLayout columns={2} gap={25}>
						{sortedAnswers.map((el,i) =>
							<Grid item xs={12} key={i}>
								{checkForm(el.votes) ?
										<Card>
											<CardContent>
												<Grid container>
													<Typography style={{padding: "16px 16px 0"}} classes={{root: classes.rHeader}}>Contribute to this discussion</Typography>
													<TextField value={forumAnswer}  onChange={e=>setForumAnswer(e.target.value)} style={{margin: "16px" , border: "2px solid #CCD1D9",borderRadius: "4px"}} variant="outlined" fullWidth  inputProps={{ref:inputEl,rows:"6"}} rowsMax={6} multiline placeholder="Type your answer here"/>
													<Grid xs={12} item>
														{subload?
                                                            <CircularProgress style={{float:"right"}}/>:
                                                            <>
                                                            <Button onClick={()=>c.content.answerers.includes(fire.auth.currentUser.uid)?setErrorTwoAns(true):replyIt(inputEl.current.value)} style={{ float: "right", margin: "0 16px 0 10px", background: "#FF9F24" , color: "#FFFFFF"}} classes={{root:classes.replyButtonStyle}} variant="contained" >Submit Answer</Button>
                                                            <input onChange={e=>{previewFile(e.target.files[0]);setImage(e.target.files); setImageReset(e.target.value); }} ref={imgClick} id="upload-pic" accept="image/*" type="file"/>
                                                            <Button onClick={handleClick} style={{ float: "right", background: "#FF9F24", color: "#FFFFFF"}} classes={{startIcon: classes.signoutMargin , root: classes.replyButtonStyle}} startIcon={<AttachFileIcon/>}>
                                                                Attach an Image
                                                            </Button>
                                                            <div style={{clear: "both"}}></div> 
                                                            <div></div>
                                                            </>
                                                            }
													</Grid>
                                                    <Grid item>
                                                    {image  ?
                                                    <IconButton onClick={()=>{
                                                        imagePreview.current.src = "";
                                                        setImage(null);
                                                        setImageReset("");
                                                        setImgSrc(null);}}>
                                                            
                                                            <CancelRoundedIcon/>
                                                    </IconButton>:
                                                    ""
                                                    } 
                                                     <img ref={imagePreview} src={imgSrc} style={{maxHeight:"100%", maxWidth: "100%"}} />

                                                    </Grid>
												</Grid>												
											</CardContent>
										</Card>
								:
									<Card style={{padding:16}}>
										<CardHeader 
										avatar={<Avatar>S</Avatar>}  
										title={
											<Typography style={{display: "flex",flexDirection: "row"}} alignItems="center">
												<Typography classes={{root: classes.rHeader}} style={el.content.systemMod?{color:"#FF9F24"}:{}}>
                                                    {
                                                        el.content.systemMod?
                                                        `System Moderator`:
                                                            c.content.tutorans.includes(el.content.user)?
                                                            `Tutor ${c.content.tutorans.indexOf(el.content.user)+1}${el.content.user==fire.auth.currentUser.uid?" (me)":""}`:
                                                            `Studybuddy ${c.content.answerers.indexOf(el.content.user)+1}${el.content.user==fire.auth.currentUser.uid?" (me)":""}`
                                                    }
                                                </Typography>
												<Typography classes={{root: classes.points}}>&nbsp;&nbsp;&nbsp;&nbsp; ● &nbsp;&nbsp;&nbsp; #{i} Answer</Typography>
											</Typography>
										} 
										subheader={el.content.timestamp?(new Date(el.content.timestamp.seconds*1000)).toLocaleString():"Timestamp Unavailable"}  
										action={
												<Grid container item direction="row" style={{padding:"8px 8px 8px 40px"}} alignItems="center">
													<IconButton disabled={el.content.downvotes.includes(fire.auth.currentUser.uid) || el.content.user == fire.auth.currentUser.uid?true:false} onClick={()=>downvote?setErrorDownvote(true):downvoteIt(el.content.user,fire.auth.currentUser.uid,el.id,el.content.upvotes.includes(fire.auth.currentUser.uid))} style={{zIndex:3}} style={{padding:2, marginRight:8}} >
														<RemoveCircleOutlineIcon style={{fontSize:16, color:el.content.downvotes.includes(fire.auth.currentUser.uid)||el.content.user == fire.auth.currentUser.uid?"rgba(0, 0, 0, 0.26)":"#3485C3"}}/>
													</IconButton>
													<Typography variant="caption" classes={{root: classes.votes}}>
														{el.content.upvoteLength}
													</Typography>
													<IconButton disabled={el.content.upvotes.includes(fire.auth.currentUser.uid)||el.content.user == fire.auth.currentUser.uid?true:false} onClick={()=>upvote?setErrorUpvote(true):upvoteIt(el.content.user,fire.auth.currentUser.uid,el.id,el.content.downvotes.includes(fire.auth.currentUser.uid))} style={{zIndex:3}} style={{padding:2, marginLeft:8}}> {/* style={{padding:2, borderRadius:"50%", border: "1px solid #3485C3", marginLeft:8}} */}
														<AddCircleOutlineIcon style={{fontSize:16, color:el.content.upvotes.includes(fire.auth.currentUser.uid)||el.content.user == fire.auth.currentUser.uid?"rgba(0, 0, 0, 0.26)":"#3485C3"}}/>
													</IconButton>
												</Grid>
										}/>
										{el.content.img? 
											<CardMedia classes={{img:classes.objectFitContain}} src={el.content.img} component="img" height="200" style={{maxWidth:400, margin:"auto"}}/>
											  :
											""}
										<CardContent style={{paddingRight:50}}>
											<Typography variant="body1" style={{whiteSpace:"pre-line"}} classes={{root: classes.reply}}>
												{el.content.answer}
											</Typography>
										</CardContent>
										<CardActions disableSpacing>
											<Button classes={{root: classes.commentHeader}} onClick={()=>{handleExpandClick(i); click.click(c.id)}} endIcon={commentIcon &&expanded===i?<KeyboardArrowUpIcon/>:<KeyboardArrowDownIcon/>}>{`${el.content.commentsMap?el.content.commentsMap.length:"0"} ${el.content.commentsMap?el.content.commentsMap.length==1?"comment":"comments":"comments"}`}</Button>
										</CardActions>
										<Collapse in={expanded===i} timeout="auto" unmountOnExit>
                                            {
                                            el.content.commentsMap?
                                            el.content.commentsMap.map((reply,i) => 
												<div className="replyStyle" style={{padding:"0 32px 0 50px"}} key={i}>
													<Typography className="replyStyle" style={{display: "flex",flexDirection: "row"}} alignItems="center" >
														{reply.comment} <Typography style={{color:"#9097AB"}}>&nbsp;— {reply.timestamp?(new Date(reply.timestamp.seconds*1000)).toLocaleString():"Timestamp Unavailable"} </Typography>
													</Typography>
													<hr/>  
												</div>
												):<div></div>}
											
											<CardContent style={{background: "#E6E9ED",padding:"16px 32px", margin: "0 -16px -16px"}} alignitems="center">
												<TextField className="textfield" value={comment} onChange={e=>setComment(e.target.value)} size="small" style={{width:"80%"}} inputProps={{ref:inputComment,borderRadius:0}} variant="outlined" placeholder="Write your comment"/>
												<Button classes={{root: classes.submitButton}} onClick={()=>{if (comment) return commentIt(el.id); else return setErrorAlert(true);}} variant="contained">Submit</Button>
											</CardContent>  
										</Collapse>
								</Card>}
									
                                    <Snackbar open={errorTwoAns} autoHideDuration={6000} onClose={handleClose}>
										<Alert onClose={handleClose} severity="warning">
											You can only answer once! Add it as a comment instead.
										</Alert>
									</Snackbar> 

                                    <Snackbar open={errorAlert} autoHideDuration={6000} onClose={handleClose}>
										<Alert onClose={handleClose} severity="warning">
											The comment must not be empty!
										</Alert>
									</Snackbar>

                                    <Snackbar open={errorUpvote} autoHideDuration={6000} onClose={handleClose}>
										<Alert onClose={handleClose} severity="warning">
											You already upvoted this answer.
										</Alert>
									</Snackbar>

                                    
                                    <Snackbar open={errorDownvote} autoHideDuration={6000} onClose={handleClose}>
										<Alert onClose={handleClose} severity="warning">
											You already downvoted this answer.
										</Alert>
									</Snackbar>    
								
							</Grid>)					
						}
				</MasonryLayout>
                <Grid container justify="center">
                {seeMoreLoad?<CircularProgress style={{root:"rgba(0, 0, 0, 0.87)"}}/>:<Button onClick={()=>{setAnswersLimit(answersLimitRef.current+5); setSeeMoreLoad(true);}}>Show More</Button>}
                </Grid>
                </>
            )
        }
		else{
            return(
				<Grid container item justify="space-between">
                    <Grid item md={6}>
                    <Card>
                        <CardContent>
                            <Grid container>
                                <Typography style={{padding: "16px 16px 0"}} classes={{root: classes.rHeader}}>Contribute to this discussion</Typography>
                                <TextField value={forumAnswer}  onChange={e=>setForumAnswer(e.target.value)} style={{margin: "16px" , border: "2px solid #CCD1D9",borderRadius: "4px"}} variant="outlined" fullWidth  inputProps={{ref:inputEl,rows:"6"}} rowsMax={6} multiline placeholder="Type your answer here"/>
                                <Grid xs={12} item>
                                <Button onClick={()=>replyIt(inputEl.current.value)} style={{ float: "right", margin: "0 16px 0 10px", background: "#FF9F24" , color: "#FFFFFF"}} classes={{root:classes.replyButtonStyle}} variant="contained" >Submit Answer</Button>
                                <input onChange={e=>{previewFile(e.target.files[0]);setImage(e.target.files); setImageReset(e.target.value); }} ref={imgClick} id="upload-pic" accept="image/*" type="file"/>
                                <Button onClick={handleClick} style={{ float: "right", background: "#FF9F24", color: "#FFFFFF"}} classes={{startIcon: classes.signoutMargin , root: classes.replyButtonStyle}} startIcon={<AttachFileIcon/>}>
                                    Attach an Image
                                </Button>
                                <div style={{clear: "both"}}></div> 
                                <div></div>
                            </Grid>
                            <Grid item>
                            {image  ?
                            <IconButton onClick={()=>{
                                imagePreview.current.src = "";
                                setImage(null);
                                setImageReset("");
                                setImgSrc(null);}}>
                                    
                                    <CancelRoundedIcon/>
                            </IconButton>:
                            ""
                            } 
                                <img ref={imagePreview} src={imgSrc} style={{maxHeight:"100%", maxWidth: "100%"}} />

                            </Grid>
                            </Grid>												
                        </CardContent>
                    </Card>
                    </Grid>
					<Grid item md={5}>
						<Card>
							<CardContent>
								<Typography variant="h6">There are no answers here as of the moment. Make the first one!</Typography>
							</CardContent>
						</Card>
					</Grid>
				</Grid>
			)
        }
    }

    else {
        return(
            ""
        )
    }
	// component usage

	{/**return(
	  <MasonryLayout columns={3} gap={25}>
		  {
		  [...Array(12).keys()].map(key => {
			const height = 200 + Math.ceil(Math.random() * 300);

			return (
			  <div style={{height: `${height}px`}} />
			)
		  })
		}
	  </MasonryLayout>, 
	  document.body
	);**/}
	
}



export default Forum;