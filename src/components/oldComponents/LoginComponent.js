import React, {Component,useState, useEffect, useContext} from 'react';
import {Loading} from './LoadingComponent';
import{FirebaseContext} from '../contexts/FirebaseContext';
import {ClickContext} from '../contexts/ClickContext';


import Forum from './ForumComponent';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import CircularProgress from '@material-ui/core/CircularProgress';
import { Link,Redirect } from 'react-router-dom';


const useStyles = makeStyles({
    buttonColor:{
       // backgroundColor:"#6de5ff",
        border: "2px solid #F5F7FA",
        borderRadius: 8,
        color: "#F5F7FA",
        "&:hover":{
            backgroundColor:"#ff9840",
            border: "2px solid #F5F7FA",
        },
        textTransform: "capitalize"
    },
    progress:{
        color:"white"
    }
})

function Logtest(){
    const fire = useContext(FirebaseContext);
    const click = useContext(ClickContext);


/*     useEffect(()=>{
        if (fire.status==true){
        console.log("Auth inspect", fire.auth.currentUser.displayname);
        }
    }, [fire.auth.currentUser]); 
    sessionStorage.setItem('loading', true);
    */

    const [loading, setLoading] = useState(false);
    const classes = useStyles();
    const [sub, setSub] = useState([]);
    const [subArray, setSubArray] = useState([]);
    //const [login, setLogin] = useState(false);


/*     async function getUSub(){
        const getSubjects = await fire.store.collection("users").doc(fire.auth.currentUser.uid).get().then(documentSnapshot=>documentSnapshot.data().uSubjects.length);
        const subjects = await fire.store.collection("users").doc(fire.auth.currentUser.uid).get().then(documentSnapshot=>documentSnapshot.data().uSubjects)
        console.log("murky",subjects)
        setSub(getSubjects);
        click.updateUSubjects(getSubjects);
    } */
    
    const login = sessionStorage.getItem("loggedIn");

    useEffect(()=>{
        if (fire.status==true){
        const unsubscribe=fire.store.collection("users").doc(fire.auth.currentUser.uid).onSnapshot(
            documentSnapshot=>{
                setSub(documentSnapshot.data().uSubjects.length);
                localStorage.setItem("localSubjects",documentSnapshot.data().uSubjects.length);
            });
        sessionStorage.setItem("loggedIn",true);
        setLoading(false);
    }

        else{
        console.log("don cheadle");
        setLoading(false);
        }
    },[fire.status,fire.store]
    )

    //console.log("murky",sub);
    //console.log("murky",subArray);


    //let getLoading = sessionStorage.getItem('loading');


/*     if (login){
        //getUSub();        
        if (sub===0){
            return(
                <Link to="/ss" style={{textDecoration:"none"}}>
                    <Button	variant="contained" size="large" classes={{root: classes.buttonColor}} color="secondary">Go to Forum</Button>
                </Link>
            );
        }

        else if(sub>0) {
            return (

                <Link to="/forum" style={{textDecoration:"none"}}>
                    <Button	variant="contained" size="large" classes={{root: classes.buttonColor}} color="secondary">Go to Forum</Button>
                </Link>

            );
        }

        else{
            return(
                <CircularProgress color="white"/>
            )
        }
    } */

    if (login){   
        if (sub===0){
            return(
                <Redirect to="/ss"/>

            );
        }

        else if(sub>0) {
            return (

                <Redirect to="/forum"/>

            );
        }

        else{
            return(
                <>
                <CircularProgress classes={{root:classes.progress}}/>
                <Typography style={{color:"white"}}>Taking too long? Please contact Gabby the Scholar on Facebook.</Typography>
                </>
            )
        }
    }

    else{
        return(
            <div>
                    {loading?
                    <CircularProgress classes={{root:classes.progress}}/>
                   :
                    <Button	variant="outlined" onClick={()=>{setLoading(true);fire.signin();}} size="large" classes={{root: classes.buttonColor}} color="secondary">
                    Login with your OBF Account
                    </Button>}
            </div>
        )
        }

}

/* function useSubjects(){
    useEffect(()=>{
    const getSubjects = fire.store.collection("users").doc(fire.auth.currentUser.uid).get().then(documentSnapshot=>documentSnapshot.data().uSubjects.length);
    console.log("murky",getSubjects);
    setSub(getSubjects);

    const subjects = fire.store.collection("users").doc(fire.auth.currentUser.uid).get().then(documentSnapshot=>documentSnapshot.data().uSubjects)
    click.updateUSubjects(subjects);
},[]
)

} */


export {Logtest};