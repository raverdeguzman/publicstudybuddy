
import React, {useState,useContext,useEffect,useRef} from 'react';
import Topbar from './TopbarComponent.js';
import FooterNew from './FooterNewComponent';
import {Sidebar, NotableQuestions} from './ForumNewComponent';

import{FirebaseContext} from '../contexts/FirebaseContext';
import { Link, useParams } from 'react-router-dom';

import CircularProgress from '@material-ui/core/CircularProgress';

import ErrorIcon from '@material-ui/icons/Error';
import './styles/forum.css';
import './styles/mydashboard.css';
import './styles/questions.css';

import Latex from 'react-latex';

import logo from './img/studybuddy logo.gif';


import bag from './img/bag.png';
import book from './img/book.png';
import headphones from './img/headphones.png';
import pc from './img/pc.png';





export default function Questions (){
    const fire = useContext(FirebaseContext);
    let {id,qid} = useParams();

    const[doc,setDoc]=useState("");
    const [select,setSelect]=useState("Most Likes");
    const [showSelect,setShowSelect]=useState(false);
    const [orderBy, setOrderBy]=useState("timestamp");
    const [desc, setDesc]=useState("desc");
    const [menu, setMenu] = useState(false);

    const [forumAnswer, setForumAnswer] =useState("");
    const [image,setImage] = useState(null);
    const [imageReset,setImageReset] = useState("");
    const [imgSrc,setImgSrc] = useState("");
    const [name,setName]=useState("");
    const [user,setUser]=useState("");
    const [edit, setEdit]=useState(false);

    const [question,setQuestion] = useState("");
    const [description,setDescription] = useState("");
    const inputEl = useRef();
    const [loading, setLoading]=useState(false);

    const imageRef = useRef();
    imageRef.current = image;

    const imagePreview = useRef({src:""});
    const wrapperRef = useRef(null);
    const wrapperRefTwo = useRef(null);


    useEffect(() => {
        window.addEventListener("click", handleClickOutside);
        return () => {
          window.removeEventListener("click", handleClickOutside);
        };
      });

      useEffect(() => {
        window.addEventListener("click", handleClickOutsideTwo);
        return () => {
          window.removeEventListener("click", handleClickOutsideTwo);
        };
      });

    
      const handleClickOutside = event => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
                setMenu(false);
                
          //console.log(event.target);
        }
      };     

      const handleClickOutsideTwo = event => {
        const { current: wrap } = wrapperRefTwo;
        if (wrap && !wrap.contains(event.target)) {
            setShowSelect(false);
                
          //console.log(event.target);
        }
      };     


    useEffect(()=>{
        if (fire.status){
        const unsubscribe = fire.store.collection("forum").doc(qid).onSnapshot(documentSnapshot => {
            console.log("hello",documentSnapshot.id)
            setDoc({id:documentSnapshot.id,content:documentSnapshot.data()});
        }
        )
        return unsubscribe
        }
            
    },[fire.status,qid,fire.store])

    

    useEffect(()=>{
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        const unsubscribe=docQuery.onSnapshot(querySnapshot => { 
            setUser(querySnapshot.data());
        }
        )
        return unsubscribe
        }
    
    },[fire.status,fire.store]);

    useEffect(()=>{
        sessionStorage.setItem("subjectcode",id);
     },[])

    function previewFile(file) {
        const preview = imagePreview.current;
        const reader = new FileReader();
      
        reader.addEventListener("load", function () {
          // convert image file to base64 string
          preview.src = reader.result;
        }, false);
      
        if (file) {
          reader.readAsDataURL(file);
          setName(file.name);
        }
      }

    async function replyIt(){
        setLoading(true);
        if (image){
            const files = image;
            const data = new FormData();
    
            data.append('file',files[0]);
            data.append('upload_preset','imgupload');
    
            const res = await fetch(
                'https://api.cloudinary.com/v1_1/scholarbuddy/image/upload',
                {
                    method: 'POST',
                    body: data
                }
            )
            
            const file = await res.json();
            setImage(file.secure_url);
            }
        
        //console.log("answerId0",doc,description);

        const answerId = await fire.store.collection("forum").doc(qid).collection("answers").add({ 
            subjectCode: id,
            questionId: qid,
            question: doc.content.question,
            user: fire.auth.currentUser.uid,
            answer: description,
            img: imageRef.current,
            upvoteLength:0,
            upvotes: [],
            downvotes:[],
            timestamp: fire.fieldValue.serverTimestamp()
        });

        //console.log("answerId1",answerId);

        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uAnswers: fire.fieldValue.arrayUnion(answerId.id)
        });

        //console.log("answerId2",answerId.id,qid);

        if (fire.tutor){
            await fire.store.collection("forum").doc(qid).update({ 
                tutorans: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
                answersLength: fire.fieldValue.increment(1)
            });
        }

        await fire.store.collection("forum").doc(qid).update({ 
            answerers: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
            answersLength: fire.fieldValue.increment(1)
        });

        //console.log("answerId3",qid,fire.auth.currentUser.uid);
        
        setDescription("");
        setImage(null);
        setImageReset("");
        setImgSrc(null);
        setLoading(false);
    }

    async function saveIt(img){
        setLoading(true);

        if (image && (img?!img.includes("res.cloudinary"):true)) {
        const files = image;
        const data = new FormData();

        data.append('file',files[0]);
        data.append('upload_preset','imgupload');
        //setLoading(true);

        const res = await fetch(
            'https://api.cloudinary.com/v1_1/scholarbuddy/image/upload',
            {
                method: 'POST',
                body: data
            }
        )
        
        const file = await res.json();
        setImage(file.secure_url);
        }

        
        await fire.store.collection("forum").doc(qid).update({
            question: question?question:doc.content.question,
            description: description,
            timestamp: fire.fieldValue.serverTimestamp(),
            img: imageRef.current,
            keywords: question[question.length-1]=="?" || question[question.length-1]=="!"||question[question.length-1]=="." ? ["",...question.substring(0,question.length-1).toLowerCase().split(" ")] : ["",...question.toLowerCase().split(" ")],
			edited: true,
		});
		
		if (fire.moderator){
			await fire.store.collection("forum").doc(qid).update({
			modedit:true
			})
		}

        setImage(null);
        setImageReset("");
        imagePreview.current.src = ""; 
        setQuestion("");
        setDescription("");
        /* setCode(""); */
        setName(null);
        setLoading(false);
        setEdit(false);
        setMenu(false);
        /* setOpen(true); */
    } 

    async function likeIt(author,uid,ansid,inc){
        await fire.store.collection("forum").doc(qid).update({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
            likers: fire.fieldValue.arrayUnion(uid),
            dislikers: fire.fieldValue.arrayRemove(uid),
            likes: inc?fire.fieldValue.increment(2):fire.fieldValue.increment(1)
        });

        await fire.store.collection("users").doc(author).update({
            upvotes: fire.fieldValue.increment(1)
        })
    }

    
    async function dislikeIt(author,uid,ansid,inc){
        await fire.store.collection("forum").doc(qid).update({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
            likers: fire.fieldValue.arrayRemove(uid),
            dislikers: fire.fieldValue.arrayUnion(uid),
            likes: inc?fire.fieldValue.increment(-2):fire.fieldValue.increment(-1)
        });

        await fire.store.collection("users").doc(author).update({
            upvotes: fire.fieldValue.increment(-1)
        })
    }

    function notifyMe(e){
        fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uQuestions:fire.fieldValue.arrayUnion(e)
        })
    }

    function unNotifyMe(e){
        fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uQuestions:fire.fieldValue.arrayRemove(e)
        })
    }

    async function userFlagIt(uid,docid){
        await fire.store.collection("forum").doc(docid).update({ 
            flaggedBy: fire.fieldValue.arrayUnion(uid),
        });
    }

    async function removeUserFlagIt(uid,docid){
        await fire.store.collection("forum").doc(docid).update({ 
            flaggedBy: fire.fieldValue.arrayRemove(uid),
        });
    }

//console.log("j",doc)

if (doc&&user){
    return(
        <>
            <Topbar/>
            <div className="forum-wrapper">
                <Sidebar subject={doc.content?doc.content.subjectCode:"MATH10"} select={0}/>
                <div className="forum-content-wrapper">
                    <div className="forum-content-subject">
                        <Link to={`/forum/${doc.content?doc.content.subjectCode:"MATH10"}`} style={{textDecoration:"none",color:"#000000"}}>
                            {doc.content?doc.content.subjectCode:"Loading"}
                        </Link>
                    </div>
                    <div className="forum-content-department-wrapper">
                        <div className="forum-content-department-name"><Link to="/selection" style={{textDecoration:"none",color:"#FAF9F9"}}>BROWSE OTHER SUBJECTS</Link></div>
                        <div></div>
                    </div>
                    {!edit?
                    <>
                    <div className="question-content-wrapper">
                    <div className="question-heading-wrapper">
                        <div className="question-heading">
                            <Latex>{doc.content.question}</Latex>
                            {user.uQuestions.includes(doc.id)?<span style={{marginLeft:"8px"}}><svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M16.0001 29.3333C17.4667 29.3333 18.6667 28.1333 18.6667 26.6666H13.3334C13.3334 28.1333 14.5201 29.3333 16.0001 29.3333ZM24.0001 21.3333V14.6666C24.0001 10.5733 21.8134 7.14665 18.0001 6.23998V5.33331C18.0001 4.22665 17.1067 3.33331 16.0001 3.33331C14.8934 3.33331 14.0001 4.22665 14.0001 5.33331V6.23998C10.1734 7.14665 8.00007 10.56 8.00007 14.6666V21.3333L6.28007 23.0533C5.44007 23.8933 6.02674 25.3333 7.2134 25.3333H24.7734C25.9601 25.3333 26.5601 23.8933 25.7201 23.0533L24.0001 21.3333Z" fill="#EBC74E"/>
                    </svg></span>
                    :""}
                            <div className="question-menu" ref={wrapperRef} role="button" onClick={()=>{setMenu(!menu);}}>   
                                <svg className="svg-hover" width="32" height="8" viewBox="0 0 32 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="4" cy="4" r="4" fill="#828282"/>
                                <circle cx="16" cy="4" r="4" fill="#828282"/>
                                <circle cx="28" cy="4" r="4" fill="#828282"/>
                                </svg>
                            </div>
                            <div role="button" style={{display:`${menu?"flex":"none"}`}}  className="savedquestions-subject-menu-wrapper question-adjust-menu">
{                            <div className="savedquestions-subject-menu-item"  onClick={()=>user.uQuestions.includes(doc.id)?unNotifyMe(doc.id):notifyMe(doc.id)}>
                                <svg width="20" height="20" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.0001 29.3333C17.4667 29.3333 18.6667 28.1333 18.6667 26.6666H13.3334C13.3334 28.1333 14.5201 29.3333 16.0001 29.3333ZM24.0001 21.3333V14.6666C24.0001 10.5733 21.8134 7.14665 18.0001 6.23998V5.33331C18.0001 4.22665 17.1067 3.33331 16.0001 3.33331C14.8934 3.33331 14.0001 4.22665 14.0001 5.33331V6.23998C10.1734 7.14665 8.00007 10.56 8.00007 14.6666V21.3333L6.28007 23.0533C5.44007 23.8933 6.02674 25.3333 7.2134 25.3333H24.7734C25.9601 25.3333 26.5601 23.8933 25.7201 23.0533L24.0001 21.3333Z" fill="#0A452D"/>
                                </svg>
                                    <div style={{marginLeft:"8px"}}>{user.uQuestions.includes(doc.id)?"Unnotify":"Notify"}</div>
                                </div>}
                                <div className="savedquestions-subject-menu-item" style={{display:doc.content.user===fire.auth.currentUser.uid?"flex":"none"}} onClick={()=>setEdit(true)}>
                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3 17.4601V20.5001C3 20.7801 3.22 21.0001 3.5 21.0001H6.54C6.67 21.0001 6.8 20.9501 6.89 20.8501L17.81 9.94006L14.06 6.19006L3.15 17.1001C3.05 17.2001 3 17.3201 3 17.4601ZM20.71 7.04006C21.1 6.65006 21.1 6.02006 20.71 5.63006L18.37 3.29006C17.98 2.90006 17.35 2.90006 16.96 3.29006L15.13 5.12006L18.88 8.87006L20.71 7.04006Z" fill="#0A452D"/>
                                </svg>
                                    <div style={{marginLeft:"8px"}}>Edit</div>
                                </div>
                                <div className="savedquestions-subject-menu-item" onClick={()=>doc.content.flaggedBy.includes(fire.auth.currentUser.uid)?removeUserFlagIt(fire.auth.currentUser.uid,doc.id):userFlagIt(fire.auth.currentUser.uid,doc.id)}>
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.7667 2.5H7.23333C7.01667 2.5 6.8 2.59167 6.65 2.74167L2.74167 6.65C2.59167 6.8 2.5 7.01667 2.5 7.23333V12.7583C2.5 12.9833 2.59167 13.1917 2.74167 13.35L6.64167 17.25C6.8 17.4083 7.01667 17.5 7.23333 17.5H12.7583C12.9833 17.5 13.1917 17.4083 13.35 17.2583L17.25 13.3583C17.4083 13.2 17.4917 12.9917 17.4917 12.7667V7.23333C17.4917 7.00833 17.4 6.8 17.25 6.64167L13.35 2.74167C13.2 2.59167 12.9833 2.5 12.7667 2.5ZM10 14.4167C9.4 14.4167 8.91667 13.9333 8.91667 13.3333C8.91667 12.7333 9.4 12.25 10 12.25C10.6 12.25 11.0833 12.7333 11.0833 13.3333C11.0833 13.9333 10.6 14.4167 10 14.4167ZM10 10.8333C9.54167 10.8333 9.16667 10.4583 9.16667 10V6.66667C9.16667 6.20833 9.54167 5.83333 10 5.83333C10.4583 5.83333 10.8333 6.20833 10.8333 6.66667V10C10.8333 10.4583 10.4583 10.8333 10 10.8333Z" fill="#0A452D"/>
                                    </svg>
                                    <div style={{marginLeft:"8px"}}>{doc.content.flaggedBy.includes(fire.auth.currentUser.uid)?"Unreport":"Report"}</div>
                            </div>
                                    {/*                         <div className="savedquestions-subject-menu-item">
                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M18 16.08C17.24 16.08 16.56 16.38 16.04 16.85L8.91 12.7C8.96 12.47 9 12.24 9 12C9 11.76 8.96 11.53 8.91 11.3L15.96 7.19C16.5 7.69 17.21 8 18 8C19.66 8 21 6.66 21 5C21 3.34 19.66 2 18 2C16.34 2 15 3.34 15 5C15 5.24 15.04 5.47 15.09 5.7L8.04 9.81C7.5 9.31 6.79 9 6 9C4.34 9 3 10.34 3 12C3 13.66 4.34 15 6 15C6.79 15 7.5 14.69 8.04 14.19L15.16 18.35C15.11 18.56 15.08 18.78 15.08 19C15.08 20.61 16.39 21.92 18 21.92C19.61 21.92 20.92 20.61 20.92 19C20.92 17.39 19.61 16.08 18 16.08Z" fill="#0A452D"/>
                                </svg>
                                    <div style={{marginLeft:"8px"}}>Share</div>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        <div className="question-buttons-wrapper">
                        <div className="savedquestions-menu-wrapper">
                            <div className="savedquestions-menu-like">
                            <svg role="button" onClick={()=>(doc.content.likers?doc.content.likers.includes(fire.auth.currentUser.uid):false)||doc.content.user == fire.auth.currentUser.uid?{}:likeIt(doc.content.user,fire.auth.currentUser.uid,doc.id,(doc.content.dislikers?doc.content.dislikers.includes(fire.auth.currentUser.uid):false))} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.74561 21H5.74561V9H1.74561V21ZM23.7456 10C23.7456 8.9 22.8456 8 21.7456 8H15.4356L16.3856 3.43L16.4156 3.11C16.4156 2.7 16.2456 2.32 15.9756 2.05L14.9156 1L8.33561 7.59C7.96561 7.95 7.74561 8.45 7.74561 9V19C7.74561 20.1 8.64561 21 9.74561 21H18.7456C19.5756 21 20.2856 20.5 20.5856 19.78L23.6056 12.73C23.6956 12.5 23.7456 12.26 23.7456 12V10Z" fill={(doc.content.likers?doc.content.likers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                            </svg>
                            <div className="savedquestions-menu-like-number">{doc.content.likes>0?doc.content.likes:0}</div>
                            <svg role="button" onClick={()=>(doc.content.dislikers?doc.content.dislikers.includes(fire.auth.currentUser.uid):false)||doc.content.user == fire.auth.currentUser.uid?{}:dislikeIt(doc.content.user,fire.auth.currentUser.uid,doc.id,(doc.content.likers?doc.content.likers.includes(fire.auth.currentUser.uid):false))} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.7456 3H6.74561C5.91561 3 5.20561 3.5 4.90561 4.22L1.88561 11.27C1.79561 11.5 1.74561 11.74 1.74561 12V14C1.74561 15.1 2.64561 16 3.74561 16H10.0556L9.10561 20.57L9.07561 20.89C9.07561 21.3 9.24561 21.68 9.51561 21.95L10.5756 23L17.1656 16.41C17.5256 16.05 17.7456 15.55 17.7456 15V5C17.7456 3.9 16.8456 3 15.7456 3ZM19.7456 3V15H23.7456V3H19.7456Z" fill={(doc.content.dislikers?doc.content.dislikers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                            </svg>
                            </div>
                            <div className="savedquestions-menu-timestamp">{doc.content.timestamp?(new Date(doc.content.timestamp.seconds*1000)).toLocaleString():"Loading Timestamp"}</div>
                            <div className="savedquestions-menu-comments">
                                <svg role="button" width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M20.1685 2H4.16846C3.06846 2 2.16846 2.9 2.16846 4V16C2.16846 17.1 3.06846 18 4.16846 18H18.1685L22.1685 22V4C22.1685 2.9 21.2685 2 20.1685 2ZM18.1685 14H6.16846V12H18.1685V14ZM18.1685 11H6.16846V9H18.1685V11ZM18.1685 8H6.16846V6H18.1685V8Z" fill="black"/>
                                </svg>
                                <div className="savedquestions-menu-comments-number">{doc.content.answersLength}</div>
                            </div>
                        </div>
                        </div>
                        {/**Place of Edit */}

                        <div className="question-description">
                            <Latex>{doc.content.description}</Latex>
                            {doc.content.img?<img className="savedquestions-image-wrapper" src={doc.content.img}/>:""}
                        </div>
                    </div>

                    <div /* style={{display:`${showDisc?"inherit":"none"}`}} */ style={{marginTop:"8px"}} className="forum-question-input-wrapper">
{/*                         <div id="questions-triangle-up"></div> */}
                        <div className="forum-question-input">
                        <textarea value={description} onChange={(e)=>setDescription(e.target.value)} className="forum-question-input-description" placeholder="Type your answer here...">
                        </textarea>
                        <div className="forum-question-input-buttons">
                            <div className="forum-question-input-image">
                                <input type="file" accept="image/*" onChange={e=>{previewFile(e.target.files[0]);setImage(e.target.files); setImageReset(e.target.value); }} ref={inputEl} className="forum-question-hide-input-file"/>
                                <svg role="button" onClick={()=>inputEl.current.click()} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21.6602 19V5C21.6602 3.9 20.7602 3 19.6602 3H5.66016C4.56016 3 3.66016 3.9 3.66016 5V19C3.66016 20.1 4.56016 21 5.66016 21H19.6602C20.7602 21 21.6602 20.1 21.6602 19ZM9.16016 13.5L11.6602 16.51L15.1602 12L19.6602 18H5.66016L9.16016 13.5Z" fill="black"/>
                                </svg>
                                <div className="wrap-pic-name">{name}</div>
                                {image  ?
                                <svg role="button" onClick={()=>{
                                    imagePreview.current.src = "";
                                    setImage(null);
                                    setImageReset("");
                                    setName(null);}} width="15" height="15" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M5 0C2.235 0 0 2.235 0 5C0 7.765 2.235 10 5 10C7.765 10 10 7.765 10 5C10 2.235 7.765 0 5 0ZM5 9C2.795 9 1 7.205 1 5C1 2.795 2.795 1 5 1C7.205 1 9 2.795 9 5C9 7.205 7.205 9 5 9ZM5 4.295L6.795 2.5L7.5 3.205L5.705 5L7.5 6.795L6.795 7.5L5 5.705L3.205 7.5L2.5 6.795L4.295 5L2.5 3.205L3.205 2.5L5 4.295Z" fill="#656D78"/>
                                </svg>:
                                ""
                                }
                            </div>
                            <div role={description?"button":""} onClick={()=>description?replyIt():""}  className={description?"forum-question-input-post":"forum-question-input-post-disabled"}>
                                {loading?<CircularProgress style={{alignSelf:"center", marginRight:"8px",color:"#FFFFFF"}} size={20}/>:""}
                                <div>ANSWER</div>
                            </div>
                        </div>   
                        </div>
                    </div></>:
                        <div className="forum-question-input" style={{marginBottom:"24px"}}>
                        <input placeholder={doc.content.question} value={question} onChange={(e)=>setQuestion(e.target.value)} className="forum-question-input-question"/>
                        <textarea value={description} onChange={(e)=>setDescription(e.target.value)} className="forum-question-input-description" placeholder={doc.content.description?"":"Edit the description of your question here..."}>
                        </textarea>
                        <div className="forum-question-input-buttons">
                            <div className="forum-question-input-image">
                                <input type="file" accept="image/*" onChange={e=>{previewFile(e.target.files[0]);setImage(e.target.files); setImageReset(e.target.value); }} ref={inputEl} className="forum-question-hide-input-file"/>
                                <svg role="button" onClick={()=>inputEl.current.click()} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21.6602 19V5C21.6602 3.9 20.7602 3 19.6602 3H5.66016C4.56016 3 3.66016 3.9 3.66016 5V19C3.66016 20.1 4.56016 21 5.66016 21H19.6602C20.7602 21 21.6602 20.1 21.6602 19ZM9.16016 13.5L11.6602 16.51L15.1602 12L19.6602 18H5.66016L9.16016 13.5Z" fill="black"/>
                                </svg>
                                <div className="wrap-pic-name">{doc.content.img?doc.content.img:name}</div>
                                {image ||doc.content.img ?
                                <svg role="button" onClick={()=>{
                                    imagePreview.current.src = "";
                                    setImage(null);
                                    setImageReset("");
                                    setName(null);
                                    doc.content.img="";}} width="15" height="15" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M5 0C2.235 0 0 2.235 0 5C0 7.765 2.235 10 5 10C7.765 10 10 7.765 10 5C10 2.235 7.765 0 5 0ZM5 9C2.795 9 1 7.205 1 5C1 2.795 2.795 1 5 1C7.205 1 9 2.795 9 5C9 7.205 7.205 9 5 9ZM5 4.295L6.795 2.5L7.5 3.205L5.705 5L7.5 6.795L6.795 7.5L5 5.705L3.205 7.5L2.5 6.795L4.295 5L2.5 3.205L3.205 2.5L5 4.295Z" fill="#656D78"/>
                                </svg>:
                                ""
                                }
                            </div>
                            <div role={question?"button":"button"} onClick={()=>question?saveIt(doc.content.img):saveIt(doc.content.img)} className={question?"forum-question-input-post":"forum-question-input-post"}>
                                {loading?<CircularProgress style={{alignSelf:"center", marginRight:"8px",color:"#FFFFFF"}} size={20}/>:""}
                                <div>EDIT</div>
                            </div>
                        </div>   
                    </div>}

                    <div ref={wrapperRefTwo} className="sort-by-wrapper">
                        <div className="sort-by-label">Sort by</div>
                        <div role="button" onClick={()=>setShowSelect(!showSelect)} className="sort-by-display">
                            {select}
                            <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.4958 0.355336L6.7821 5.06909L2.06834 0.355336C1.84136 0.127846 1.53321 0 1.21185 0C0.890489 0 0.582333 0.127846 0.355354 0.355336C-0.118451 0.829141 -0.118451 1.59452 0.355354 2.06832L5.93168 7.64465C6.40548 8.11845 7.17086 8.11845 7.64466 7.64465L13.221 2.06832C13.6948 1.59452 13.6948 0.829141 13.221 0.355336C12.7472 -0.10632 11.9697 -0.118469 11.4958 0.355336Z" fill="#EAC646"/>
                            </svg>
                        </div>
                        <div className="sort-by-choices-wrapper" style={{display:`${showSelect?"flex":"none"}`}}>
                            <div className={`sort-by-element ${select==="Most Likes"?"sort-by-element-selected":""}`} role="button" onClick={()=>{setOrderBy("upvoteLength"); setDesc("desc");setShowSelect(false);setSelect("Most Likes");}}>Most Likes</div>
                            <div className={`sort-by-element ${select==="Newest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc("desc");setShowSelect(false);setSelect("Newest");}}>Newest</div>
                            <div className={`sort-by-element ${select==="Oldest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc("asc");setShowSelect(false);setSelect("Oldest");}}>Oldest</div>
                        </div>
                    </div>
                    <Answers orderby={orderBy} desc={desc} id={id} qid={qid}/>
                </div>
                <NotableQuestions/>
            </div>
            <FooterNew/>
        </>
    )
    }

    else{
        return(
            <>
            <Topbar/>
            <div className="forum-wrapper">
                <Sidebar/>
                <div className="forum-content-wrapper">
                <CircularProgress style={{color:"#EAC646", alignSelf:"center",justifySelf:"center"}}/>
                </div>
                <NotableQuestions/>
            </div>
            <FooterNew/>
        </>
        )
    }
}

function Answers ({orderby, desc}){
    const array = Array.from(Array(5).keys())
    const fire = useContext(FirebaseContext);
    let {id,qid} = useParams();

    const[doc,setDoc]=useState([]);
    const [question,setQuestion] = useState("");
    const [loading, setLoading]=useState(false);
    const [pict,setPict] = useState([bag,book,headphones,pc]);
    const [name, setName]=useState("");
    const [picName,setPicName]=useState("");
    const [counter, setCounter]=useState(5);
    const [edit,setEdit]=useState(false);
    const [description,setDescription] = useState("");
    const [image,setImage] = useState(null);
    const [imageReset,setImageReset] = useState("");
    const [imgSrc,setImgSrc] = useState("");
    const imageRef = useRef();
    imageRef.current = image;
    const inputEl = useRef();

    const [menu, setMenu] = useState(-1);

    const imagePreview = useRef({src:""});
    const wrapperRefThree = useRef(null);

    useEffect(() => {
        window.addEventListener("click", handleClickOutsideThree);
        return () => {
          window.removeEventListener("click", handleClickOutsideThree);
        };
      });



    const handleClickOutsideThree = event => {
        const { current: wrap } = wrapperRefThree;
        if (wrap && !wrap.contains(event.target)) {
            setMenu(false);
          //console.log(event.target);
        }
      };   



    function previewFile(file) {
        const preview = imagePreview.current;
        const reader = new FileReader();
      
        reader.addEventListener("load", function () {
          // convert image file to base64 string
          preview.src = reader.result;
        }, false);
      
        if (file) {
          reader.readAsDataURL(file);
          setPicName(file.name);
        }
      }


    useEffect(()=>{
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        docQuery.onSnapshot(querySnapshot => { 
            setName(querySnapshot.data());
        }
        )
        }
    
    },[fire.status]);

     useEffect(()=>{
        const unsubscribe = fire.store.collection("forum").doc(qid).collection("answers").orderBy(orderby,desc).limit(counter).onSnapshot(querySnapshot => {
            let z= [];
            querySnapshot.forEach(queryDocumentSnapshot => {
                z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
            });
            setDoc(z);
        }
        )

        return unsubscribe;
        },[fire.store,orderby,desc,qid,counter])

        async function replyIt(){
            setLoading(true);
            if (image){
                const files = image;
                const data = new FormData();
        
                data.append('file',files[0]);
                data.append('upload_preset','imgupload');
        
                const res = await fetch(
                    'https://api.cloudinary.com/v1_1/scholarbuddy/image/upload',
                    {
                        method: 'POST',
                        body: data
                    }
                )
                
                const file = await res.json();
                setImage(file.secure_url);
                }
            
    
            const answerId = await fire.store.collection("forum").doc(qid).collection("answers").add({ 
                subjectCode: id,
                questionId: qid,
                question: doc.content.question,
                user: fire.auth.currentUser.uid,
                answer: description,
                img: imageRef.current,
                upvoteLength:0,
                upvotes: [],
                downvotes:[],
                timestamp: fire.fieldValue.serverTimestamp()
            });
    
    
            await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
                uAnswers: fire.fieldValue.arrayUnion(answerId.id)
            });
    
    
            if (fire.tutor){
                await fire.store.collection("forum").doc(qid).update({ 
                    tutorans: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
                    answersLength: fire.fieldValue.increment(1)
                });
            }
    
            await fire.store.collection("forum").doc(qid).update({ 
                answerers: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
                answersLength: fire.fieldValue.increment(1)
            });
    
            
            setDescription("");
            setImage(null);
            setImageReset("");
            setImgSrc(null);
            setLoading(false);
        }

        async function editReplyIt(img,answer,desc){
            //console.log("index",i);
            //setSubload(true);

            setLoading(true);
    
            let imageLink = 0;
            if (image && (img?!img.includes("res.cloudinary"):true)){
                const files = image;
                const data = new FormData();
        
                data.append('file',files[0]);
                data.append('upload_preset','imgupload');
        
                const res = await fetch(
                    'https://api.cloudinary.com/v1_1/scholarbuddy/image/upload',
                    {
                        method: 'POST',
                        body: data
                    }
                )
                
                const file = await res.json();
                //setImage(file.secure_url);
                imageLink = file.secure_url
                }
            
            //console.log("rep",answer.id)
            await fire.store.collection("forum").doc(qid).collection("answers").doc(answer.id).update({ 
                answer: description?description:desc,
                img: imageLink,
                timestamp: fire.fieldValue.serverTimestamp(),
                edited: true
            });
            
            if (fire.moderator){
                await fire.store.collection("forum").doc(qid).collection("answers").doc(answer.id).update({ 
                    modedit:true
                });
            }

            setDescription("");
            setImage(null);
            setImageReset("");
            setImgSrc(null);
            setLoading(false);
            setEdit(false);
            //setEdited(true);
        }


        async function upvoteIt(author,uid,ansid,inc){

            await fire.store.collection("forum").doc(qid).collection("answers").doc(ansid).update({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
                upvotes: fire.fieldValue.arrayUnion(uid),
                downvotes: fire.fieldValue.arrayRemove(uid),
                upvoteLength: inc?fire.fieldValue.increment(2):fire.fieldValue.increment(1)
            });
    
            await fire.store.collection("users").doc(author).update({
                upvotes: fire.fieldValue.increment(1)
            })
        }
    
        
    async function downvoteIt(author,uid,ansid,inc){
        await fire.store.collection("forum").doc(qid).collection("answers").doc(ansid).update({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
            upvotes: fire.fieldValue.arrayRemove(uid),
            downvotes: fire.fieldValue.arrayUnion(uid),
            upvoteLength: inc?fire.fieldValue.increment(-2):fire.fieldValue.increment(-1)
        });

        await fire.store.collection("users").doc(author).update({
            upvotes: fire.fieldValue.increment(-1)
        })
    }

    async function userFlagIt(uid,docid){
        await fire.store.collection("forum").doc(qid).collection("answers").doc(docid).update({ 
            flaggedBy: fire.fieldValue.arrayUnion(uid),
        });
    }

    async function removeUserFlagIt(uid,docid){
        await fire.store.collection("forum").doc(qid).collection("answers").doc(docid).update({ 
            flaggedBy: fire.fieldValue.arrayRemove(uid),
        });
    }


        //console.log("Hello",doc)

    const questions = doc?doc.sort((a)=>a.content.systemMod?-1:1).map((element,i)=>
        <>
        {edit!==i?
        <div className="forum-answers-wrapper">
            <div className="savedquestions-question" style={{color:`${element.content.systemMod?"#EBC74E":"inherit"}`}}>
            {!element.content.systemMod?<span style={{marginRight:"16px"}}><img className="my-answers-prof-pic" src={pict[element.content.user===fire.auth.currentUser.uid?name.pic:Math.floor(i%4)]} width="32" height="32"/></span>:""}
            {element.content.systemMod?
            <span style={{display:"flex",gridGap:"8px",alignItems:"center"}}><ErrorIcon/>System Moderator</span>:
            element.content.tutorans? 
                element.content.tutorans.includes(element.content.user)?
                `Tutor ${element.content.user==fire.auth.currentUser.uid?" (me)":""}`:
                `Studybuddy ${element.content.user==fire.auth.currentUser.uid?" (me)":""}`:
            `Studybuddy ${element.content.user==fire.auth.currentUser.uid?" (me)":""}`
            }
            <div style={{display:`${element.content.systemMod?"none":"inline-flex"}`}}  className="question-menu question-adjust-menu-answers" role="button" onClick={()=>setMenu(i===0&& menu!==0?0:(menu-i===0)?false:i)}>   
                <svg className="svg-hover" width="32" height="8" viewBox="0 0 32 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="4" cy="4" r="4" fill="#828282"/>
                <circle cx="16" cy="4" r="4" fill="#828282"/>
                <circle cx="28" cy="4" r="4" fill="#828282"/>
                </svg>
                <div role="button" style={{display:`${menu===i?"flex":"none"}`}}  className="savedquestions-subject-menu-wrapper">
{/*                         <div className="savedquestions-subject-menu-item"  onClick={()=>user.uQuestions.includes(element.id)?unNotifyMe(element.id):notifyMe(element.id)}>
                        <svg width="20" height="20" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M16.0001 29.3333C17.4667 29.3333 18.6667 28.1333 18.6667 26.6666H13.3334C13.3334 28.1333 14.5201 29.3333 16.0001 29.3333ZM24.0001 21.3333V14.6666C24.0001 10.5733 21.8134 7.14665 18.0001 6.23998V5.33331C18.0001 4.22665 17.1067 3.33331 16.0001 3.33331C14.8934 3.33331 14.0001 4.22665 14.0001 5.33331V6.23998C10.1734 7.14665 8.00007 10.56 8.00007 14.6666V21.3333L6.28007 23.0533C5.44007 23.8933 6.02674 25.3333 7.2134 25.3333H24.7734C25.9601 25.3333 26.5601 23.8933 25.7201 23.0533L24.0001 21.3333Z" fill="#0A452D"/>
                        </svg>
                            <div style={{marginLeft:"8px"}}>{user.uQuestions.includes(element.id)?"Unnotify":"Notify"}</div>
                        </div> */}
                        <div className="savedquestions-subject-menu-item" onClick={()=>setEdit(i)}>
                        <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3 17.4601V20.5001C3 20.7801 3.22 21.0001 3.5 21.0001H6.54C6.67 21.0001 6.8 20.9501 6.89 20.8501L17.81 9.94006L14.06 6.19006L3.15 17.1001C3.05 17.2001 3 17.3201 3 17.4601ZM20.71 7.04006C21.1 6.65006 21.1 6.02006 20.71 5.63006L18.37 3.29006C17.98 2.90006 17.35 2.90006 16.96 3.29006L15.13 5.12006L18.88 8.87006L20.71 7.04006Z" fill="#0A452D"/>
                        </svg>
                            <div style={{marginLeft:"8px"}}>Edit</div>
                        </div>
                        <div className="savedquestions-subject-menu-item" onClick={()=>element.content.flaggedBy?element.content.flaggedBy.includes(fire.auth.currentUser.uid)?removeUserFlagIt(fire.auth.currentUser.uid,element.id):userFlagIt(fire.auth.currentUser.uid,element.id):userFlagIt(fire.auth.currentUser.uid,element.id)}>
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.7667 2.5H7.23333C7.01667 2.5 6.8 2.59167 6.65 2.74167L2.74167 6.65C2.59167 6.8 2.5 7.01667 2.5 7.23333V12.7583C2.5 12.9833 2.59167 13.1917 2.74167 13.35L6.64167 17.25C6.8 17.4083 7.01667 17.5 7.23333 17.5H12.7583C12.9833 17.5 13.1917 17.4083 13.35 17.2583L17.25 13.3583C17.4083 13.2 17.4917 12.9917 17.4917 12.7667V7.23333C17.4917 7.00833 17.4 6.8 17.25 6.64167L13.35 2.74167C13.2 2.59167 12.9833 2.5 12.7667 2.5ZM10 14.4167C9.4 14.4167 8.91667 13.9333 8.91667 13.3333C8.91667 12.7333 9.4 12.25 10 12.25C10.6 12.25 11.0833 12.7333 11.0833 13.3333C11.0833 13.9333 10.6 14.4167 10 14.4167ZM10 10.8333C9.54167 10.8333 9.16667 10.4583 9.16667 10V6.66667C9.16667 6.20833 9.54167 5.83333 10 5.83333C10.4583 5.83333 10.8333 6.20833 10.8333 6.66667V10C10.8333 10.4583 10.4583 10.8333 10 10.8333Z" fill="#0A452D"/>
                            </svg>
                            <div style={{marginLeft:"8px"}}>{element.content.flaggedBy?element.content.flaggedBy.includes(fire.auth.currentUser.uid)?"Unreport":"Report":"Report"}</div>
                        </div>
{/*                         <div className="savedquestions-subject-menu-item">
                        <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18 16.08C17.24 16.08 16.56 16.38 16.04 16.85L8.91 12.7C8.96 12.47 9 12.24 9 12C9 11.76 8.96 11.53 8.91 11.3L15.96 7.19C16.5 7.69 17.21 8 18 8C19.66 8 21 6.66 21 5C21 3.34 19.66 2 18 2C16.34 2 15 3.34 15 5C15 5.24 15.04 5.47 15.09 5.7L8.04 9.81C7.5 9.31 6.79 9 6 9C4.34 9 3 10.34 3 12C3 13.66 4.34 15 6 15C6.79 15 7.5 14.69 8.04 14.19L15.16 18.35C15.11 18.56 15.08 18.78 15.08 19C15.08 20.61 16.39 21.92 18 21.92C19.61 21.92 20.92 20.61 20.92 19C20.92 17.39 19.61 16.08 18 16.08Z" fill="#0A452D"/>
                        </svg>
                            <div style={{marginLeft:"8px"}}>Share</div>
                        </div> */}
                    </div>
            </div>
            </div>
            <div className="savedquestions-description">
                <Latex>{element.content.answer}</Latex>
                {element.content.img?<img className="savedquestions-image-wrapper" src={element.content.img}/>:""}
            </div>
            
            <div className="savedquestions-menu-wrapper" style={{display:`${element.content.systemMod?"none":"grid"}`}}>
                <div className="savedquestions-menu-like">
                    <svg role="button" onClick={()=>element.content.upvotes.includes(fire.auth.currentUser.uid)||element.content.user == fire.auth.currentUser.uid?{}:upvoteIt(element.content.user,fire.auth.currentUser.uid,element.id,element.content.downvotes.includes(fire.auth.currentUser.uid))} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.74561 21H5.74561V9H1.74561V21ZM23.7456 10C23.7456 8.9 22.8456 8 21.7456 8H15.4356L16.3856 3.43L16.4156 3.11C16.4156 2.7 16.2456 2.32 15.9756 2.05L14.9156 1L8.33561 7.59C7.96561 7.95 7.74561 8.45 7.74561 9V19C7.74561 20.1 8.64561 21 9.74561 21H18.7456C19.5756 21 20.2856 20.5 20.5856 19.78L23.6056 12.73C23.6956 12.5 23.7456 12.26 23.7456 12V10Z" fill={element.content.upvotes.includes(fire.auth.currentUser.uid)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                    <div className="savedquestions-menu-like-number">{element.content.upvoteLength>0?element.content.upvoteLength:0}</div>
                    <svg role="button" onClick={()=>element.content.downvotes.includes(fire.auth.currentUser.uid)||element.content.user == fire.auth.currentUser.uid?{}:downvoteIt(element.content.user,fire.auth.currentUser.uid,element.id,element.content.upvotes.includes(fire.auth.currentUser.uid))} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.7456 3H6.74561C5.91561 3 5.20561 3.5 4.90561 4.22L1.88561 11.27C1.79561 11.5 1.74561 11.74 1.74561 12V14C1.74561 15.1 2.64561 16 3.74561 16H10.0556L9.10561 20.57L9.07561 20.89C9.07561 21.3 9.24561 21.68 9.51561 21.95L10.5756 23L17.1656 16.41C17.5256 16.05 17.7456 15.55 17.7456 15V5C17.7456 3.9 16.8456 3 15.7456 3ZM19.7456 3V15H23.7456V3H19.7456Z" fill={element.content.downvotes.includes(fire.auth.currentUser.uid)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                </div>
                <div className="savedquestions-menu-timestamp">{element.content.timestamp?(new Date(element.content.timestamp.seconds*1000)).toLocaleString():"Loading Timestamp"}</div>
{/*                 <div className="savedquestions-menu-comments">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.99973 8.99999V7.40999C9.99973 6.51999 8.91973 6.06999 8.28973 6.69999L3.69973 11.29C3.30973 11.68 3.30973 12.31 3.69973 12.7L8.28973 17.29C8.91973 17.92 9.99973 17.48 9.99973 16.59V14.9C14.9997 14.9 18.4997 16.5 20.9997 20C19.9997 15 16.9997 9.99999 9.99973 8.99999Z" fill="black"/>
                    </svg>
                    <div className="savedquestions-menu-comments-number">{element.content.commentsMap?element.content.commentsMap.length:"0"}</div>
                    <div className="answer-comment-wrapper">
                        <div id="questions-triangle-up"></div>
                            {element.content.commentsMap?element.content.commentsMap.map((comment,i)=>
                            <div className="answer-comments">
                                <div>{comment.comment}</div>
                                <div>{comment.timestamp?(new Date(comment.timestamp.seconds*1000)).toLocaleString():"Loading Timestamp"}</div>
                            </div>):""}
                        <div className="answer-input">
                            <input placeholder="Type your comment here..." value={question} onChange={(e)=>setQuestion(e.target.value)} className="answer-question-input-description"/>
                            <div role="button" onClick={()=>{}} className="answer-question-input-post">
                                {loading?<CircularProgress style={{alignSelf:"center", marginRight:"8px",color:"#FFFFFF"}} size={20}/>:""}
                                <div>COMMENT</div>
                            </div>
                        </div>
                    </div>
                </div> */}
            </div>
        </div>:
                    <div /* style={{display:`${showDisc?"inherit":"none"}`}} */ style={{marginTop:"8px"}} className="forum-question-input-wrapper answer-edit">
                    {/*                         <div id="questions-triangle-up"></div> */}
                                            <div className="forum-question-input">
                                            <textarea value={description} onChange={(e)=>setDescription(e.target.value)} className="forum-question-input-description" placeholder={element.content.answer}>
                                            </textarea>
                                            <div className="forum-question-input-buttons">
                                                <div className="forum-question-input-image">
                                                    <input type="file" accept="image/*" onChange={e=>{previewFile(e.target.files[0]);setImage(e.target.files); setImageReset(e.target.value); }} ref={inputEl} className="forum-question-hide-input-file"/>
                                                    <svg role="button" onClick={()=>inputEl.current.click()} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M21.6602 19V5C21.6602 3.9 20.7602 3 19.6602 3H5.66016C4.56016 3 3.66016 3.9 3.66016 5V19C3.66016 20.1 4.56016 21 5.66016 21H19.6602C20.7602 21 21.6602 20.1 21.6602 19ZM9.16016 13.5L11.6602 16.51L15.1602 12L19.6602 18H5.66016L9.16016 13.5Z" fill="black"/>
                                                    </svg>
                                                    <div className="wrap-pic-name">{element.content.img?element.content.img:picName}</div>
                                                    {element.content.img  ?
                                                    <svg role="button" onClick={()=>{
                                                        imagePreview.current.src = "";
                                                        setImage(null);
                                                        setImageReset("");
                                                        setPicName(null);
                                                        element.content.img="";}} width="15" height="15" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M5 0C2.235 0 0 2.235 0 5C0 7.765 2.235 10 5 10C7.765 10 10 7.765 10 5C10 2.235 7.765 0 5 0ZM5 9C2.795 9 1 7.205 1 5C1 2.795 2.795 1 5 1C7.205 1 9 2.795 9 5C9 7.205 7.205 9 5 9ZM5 4.295L6.795 2.5L7.5 3.205L5.705 5L7.5 6.795L6.795 7.5L5 5.705L3.205 7.5L2.5 6.795L4.295 5L2.5 3.205L3.205 2.5L5 4.295Z" fill="#656D78"/>
                                                    </svg>:
                                                    ""
                                                    }
                                                </div>
                                                <div role="button" onClick={()=>editReplyIt(element.content.img,element,element.content.answer)} className="forum-question-input-post">
                                                    {loading?<CircularProgress style={{alignSelf:"center", marginRight:"8px",color:"#FFFFFF"}} size={20}/>:""}
                                                    <div>EDIT</div>
                                                </div>
                                            </div>   
                                            </div>
                                        </div> }
        </>
    ):<div></div>

    return(
            <>
            <div ref={wrapperRefThree} className="dashboard-content-savedquestions">
                {questions}
                <div role="button" className="dashboard-content-savedquestions-viewall-wrapper">
                    <div onClick={()=>setCounter(y=>y+5)} style={{display:`${doc.length<5||(doc.length>5&&!doc.length%5)?"none":"flex"}`}} className="dashboard-content-savedquestions-viewall">See More</div>
                </div>
            </div>
            </>
        )
    }
