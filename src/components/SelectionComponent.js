import React, {useState,useContext,useEffect,useRef} from 'react';
import Topbar from './TopbarComponent.js';
import FooterNew from './FooterNewComponent';
import './styles/selection.css';
import {Link} from 'react-router-dom';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import{FirebaseContext} from '../contexts/FirebaseContext';
import CircularProgress from '@material-ui/core/CircularProgress';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
/* import ClickAwayListener from 'react-click-away-listener';
 */

/*  import OutsideAlerter from './Clickaway';
 */


export function Selection (){

    const [show, setShow] = useState(true);
    const [inpSub, setInpSub] = useState("");

    const fire = useContext(FirebaseContext);

    async function updateSubjectServer(input){
        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uSubjects: fire.fieldValue.arrayUnion(`${input.toUpperCase().substring(0,input.length-1)}${input.substring(input.length-1).toLowerCase()}`),
        });
        setInpSub("");
    }

    return(
        <div>
            <Topbar/>
            <div className="selection-wrapper">
                <div className="selection-intro">
                    <div onClick={()=>setShow(!show)} role="button" className="selection-heading">
                        Let's get you started!
                    </div>
                    <div className="selection-description">
                        Keep track of specific forums by saving them. You can add a class by typing it in the search bar.{/* <br/>and clicking the subject from the list.  */}
                    </div>
                </div>
                <div className="selection-input-wrapper">
                    <input value={inpSub} 
                    onKeyPress={e=>{if (e.key==="Enter") return inpSub.includes(" ")||(Number.isNaN(parseInt(inpSub.substring(inpSub.length - 1)))&&inpSub.substring(inpSub.length - 1)!="i")?"":updateSubjectServer(inpSub)}}
                    onChange={e=>setInpSub(e.target.value)} className="selection-input" placeholder="Type with the format SUBJ00 (e.g. MATH10)"/>
                    <div className={inpSub.includes(" ")||(Number.isNaN(parseInt(inpSub.substring(inpSub.length - 1)))&&inpSub.substring(inpSub.length - 1)!="i")?"selection-button-disabled":"selection-button"} 
                    role={inpSub.includes(" ")||(Number.isNaN(parseInt(inpSub.substring(inpSub.length - 1)))&&inpSub.substring(inpSub.length - 1)!="i")?"":"button"} 
                    onClick={()=>
                        inpSub.includes(" ")||(Number.isNaN(parseInt(inpSub.substring(inpSub.length - 1)))&&inpSub.substring(inpSub.length - 1)!="i")?"":updateSubjectServer(inpSub)}>Add Class</div>
                </div>
{/*                 <div className="selection-request"> inpSub.includes(" ")||(Number.isNaN(parseInt(inpSub.substring(inpSub.length - 1)))&&inpSub.substring(inpSub.length - 1)!="i")
                    Can’t find a class? <a href="/landing">Request here</a>
                </div> */}
                {/**Cards or Illustration */}
                <Subjects show={show}/>
            </div>
            <FooterNew/>
        </div>
    )
}



export function Subjects ({show}){
//if (show)

    const fire = useContext(FirebaseContext);
    const [loading,setLoading] = useState(true);
    const [subjects, setSubjects] = useState([0]);
    const [menu,setMenu]=useState(1000);
    const wrapperRef = useRef(null);


    useEffect(() => {
        window.addEventListener("mousedown", handleClickOutside);
        return () => {
          window.removeEventListener("mousedown", handleClickOutside);
        };
      });
    
      const handleClickOutside = event => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
          setMenu(false);
        }
      };


    useEffect(()=>{
        if (fire.status==true){
        const unsubscribe = fire.store.collection("users").doc(fire.auth.currentUser.uid).onSnapshot(
            documentSnapshot=>{
                //setSubArray(documentSnapshot.data().uSubjects);
                setSubjects(documentSnapshot.data().uSubjects);
            });
        }
        else{
        console.log("don cheadle");
        }
    },[fire.status]
    )

    useEffect(()=>{
        setTimeout(() => {
            setLoading(false);
          }, 1000);
    },[subjects])


    async function removeSub (element){
        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uSubjects: fire.fieldValue.arrayRemove(element),
        });
        setMenu(false);
    }

    const handleClickAway = () => {
        setMenu(false);
      };
    


    const subjectCards = subjects.map((element,i)=>
            <div className={`selection-subject-card ${i<4?`color${i+1}`:`color${((i+1)%4)?((i+1)%4):4}`}`}>
                <div className="selection-subject-card-top-wrapper">
                    <div className="selection-subject-card-top">{element}</div>
                    <div className="selection-subject-card-top-menu">
                            <svg ref={wrapperRef} onClick={()=>setMenu(i===0&& menu!==0?0:menu-i?i:false)} role="button" id={`top-menu-${i+1<3?i:(i+1)%2}`} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" 
                                d="M6.5 1.5C6.5 2.32843 7.17157 3 8 3C8.82843 3 9.5 2.32843 9.5 1.5C9.5 0.671573 8.82843 -2.93554e-08 8 -6.55671e-08C7.17157 -1.01779e-07 6.5 0.671573 6.5 1.5ZM6.5 8C6.5 8.82843 7.17157 9.5 8 9.5C8.82843 9.5 9.5 8.82843 9.5 8C9.5 7.17157 8.82843 6.5 8 6.5C7.17157 6.5 6.5 7.17157 6.5 8ZM8 16C7.17157 16 6.5 15.3284 6.5 14.5C6.5 13.6716 7.17157 13 8 13C8.82843 13 9.5 13.6716 9.5 14.5C9.5 15.3284 8.82843 16 8 16Z" 
                                fill={`${i>0?((i+1)%2)?`#1A051D`:`#FAF9F9`:`#1A051D`}`}/>
                            </svg>
                        <div role="button" style={{display:`${menu===i?"flex":"none"}`}} onClick={()=>removeSub(element)} className="selection-remove">
                            <span id="overlay">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4.99984 15.8333C4.99984 16.75 5.74984 17.5 6.6665 17.5H13.3332C14.2498 17.5 14.9998 16.75 14.9998 15.8333V5.83333H4.99984V15.8333ZM15.8332 3.33333H12.9165L12.0832 2.5H7.9165L7.08317 3.33333H4.1665V5H15.8332V3.33333Z" fill="black"/>
                            </svg>
                            <div>Remove</div>
                            </span>
                        </div>
                    </div>  
                </div>
                <div className="selection-subject-card-bottom-wrapper">
                    {/* <div className="selection-subject-card-bottom-subject">{`${element} Forum`}</div> <MoreVertIcon style={{color:`${i>0?((i+1)%2)?`#1A051D`:`#FAF9F9`:`#1A051D`}`}}/>*/}
                    <div className="selection-subject-card-bottom-department">
                        <Link to={`/forum/${element}`} style={{textDecoration:"none"}}>
                            <div role="button" className={`selection-subject-card-bottom-department-name ${i<4?`color${i+1}`:`color${((i+1)%4)?((i+1)%4):4}`}`}>Browse {element} Forum</div>
                        </Link>
                        <div></div>
                    </div>
                </div>
            </div>

    )

    if (loading){
        return(
            <CircularProgress style={{color:"#EAC646", alignSelf:"center",justifySelf:"center"}}/>
        )
    }

    else if (subjects.length){
        return(
            
            <div className="selection-subject-wrapper">
                {subjectCards}
            </div>
        )
    }

    else if (subjects.length==0){
    return(
        <>
        <div>
            <svg width="328" height="386" viewBox="0 0 328 386" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M65.555 265.025H22.8301C21.0527 265.024 19.3286 264.418 17.9412 263.307C16.5538 262.196 15.5857 260.646 15.1961 258.912L5.88555 217.414C5.62921 216.27 5.63332 215.083 5.89755 213.941C6.16179 212.799 6.6794 211.731 7.41209 210.816C8.14479 209.901 9.07382 209.162 10.1305 208.655C11.1871 208.147 12.3443 207.884 13.5165 207.884" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M144.36 265.025H101.635C99.8574 265.024 98.1333 264.418 96.7459 263.307C95.3584 262.196 94.3904 260.646 94.0008 258.912L84.6902 217.414C84.4339 216.27 84.438 215.083 84.7022 213.941C84.9665 212.799 85.4841 211.731 86.2168 210.816C86.9495 209.901 87.8785 209.162 88.9351 208.655C89.9918 208.147 91.149 207.884 92.3212 207.884" stroke="#A8832D" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M97.5971 222.525H25.0907C22.4518 222.525 19.8906 221.632 17.8239 219.991C15.7571 218.351 14.3065 216.059 13.7081 213.489L3.25967 168.615C2.9192 167.153 2.91347 165.632 3.24291 164.167C3.57234 162.702 4.22844 161.331 5.16222 160.155C6.096 158.979 7.28334 158.029 8.63562 157.376C9.9879 156.723 11.4702 156.384 12.9718 156.384H85.4813C88.1198 156.384 90.6806 157.277 92.7468 158.918C94.813 160.559 96.263 162.851 96.8608 165.421L107.309 210.303C107.648 211.765 107.652 213.284 107.321 214.748C106.991 216.211 106.334 217.582 105.401 218.756C104.467 219.931 103.281 220.88 101.93 221.533C100.579 222.185 99.0976 222.524 97.5971 222.525Z" fill="white" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M146.55 254.674H30.2632C27.4048 254.674 25.0876 256.991 25.0876 259.849C25.0876 262.708 27.4048 265.025 30.2632 265.025H146.55C149.409 265.025 151.726 262.708 151.726 259.849C151.726 256.991 149.409 254.674 146.55 254.674Z" fill="#E1F5EE" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M16.4404 382.69L30.8778 275.096C31.1547 269.452 35.6031 265.025 40.9977 265.025H65.9474C71.3421 265.025 75.7904 269.452 76.0704 275.096L90.5077 382.69" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M108.134 265.025H132.359C137.596 265.025 141.916 269.452 142.187 275.096L156.472 382.69" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M222.452 158.407C224.026 159.445 227.674 167.173 227.741 168.104C227.808 169.035 223.618 175.954 223.618 175.954C223.618 175.954 221.953 175.991 221.637 172.184C221.251 167.678 219.273 165.971 216.407 165.329C215.208 165.062 213.005 165.974 212.85 167.739C212.695 169.504 217.253 170.675 218.168 171.746C219.559 173.365 218.777 175.647 218.777 175.702C218.777 175.702 211.742 173.572 210.391 173.176C209.174 172.814 203.74 167.873 200.351 162.974C199.089 163.898 197.768 164.737 196.395 165.485C187.876 170.067 158.666 180.725 143.535 178.309C128.34 175.875 140.477 127.497 140.477 127.497C162.482 128.513 171.695 125.711 173.374 125.118L169.958 147.481C179.204 147.207 201.957 150.977 201.969 150.98C208.362 151.82 220.192 156.919 222.452 158.407Z" fill="#FFCFC9"/>
            <path d="M140.371 127.476H140.502C162.506 128.492 171.719 125.69 173.399 125.096L172.592 130.573C172.592 130.573 156.269 138.259 138.113 137.571L140.371 127.476Z" fill="#FAB6AD"/>
            <path d="M142.577 188.627C147.804 188.244 157.279 127.415 157.279 127.415C168.947 127.585 183.056 122.142 183.056 122.142C183.056 122.142 173.408 90.5227 170.082 84.0023C166.757 77.4819 142.884 66.2089 140.066 65.3934L93.532 54.7441C93.532 54.7441 53.8102 64.6115 41.2836 74.7374C28.757 84.8634 18.9688 127.576 18.9688 127.576C41.9743 136.853 65.8256 135.487 65.8256 135.487L57.7169 184.361C55.0911 196.228 46.7025 233.902 61.4929 252.194C61.4929 252.194 73.3927 193.708 142.577 188.627Z" fill="#E6F7FF"/>
            <path d="M111.995 70.3925L98.0778 78.8845L80.4304 58.3649L93.6781 50.4296C94.0641 50.1997 94.4919 50.0487 94.9368 49.9855C95.3816 49.9222 95.8345 49.948 96.2693 50.0612C96.7041 50.1744 97.1121 50.3729 97.4695 50.6451C97.827 50.9172 98.1268 51.2577 98.3516 51.6467L111.995 70.3925Z" fill="#144A64"/>
            <path d="M133.16 72.3398L142.954 78.1208L142.482 66.3701L140.066 65.3934C140.066 65.3934 135.764 68.2778 133.16 72.3398Z" fill="#144A64"/>
            <path d="M65.8257 135.481C67.7121 129.301 69.5986 114.374 69.5986 114.374" stroke="#144A64" stroke-width="5" stroke-linejoin="round"/>
            <path d="M98.0776 78.8845L111.733 70.9097C111.733 70.9097 124.835 86.4973 125.2 89.2235C125.565 91.9497 126.253 119.75 126.253 119.75H133.159" stroke="#144A64" stroke-width="5" stroke-linejoin="round"/>
            <path d="M142.695 78.1087L133.418 72.857C133.418 72.857 125.005 86.1656 125.203 89.2235" stroke="#144A64" stroke-width="5" stroke-linejoin="round"/>
            <path d="M67.3073 129.341C67.3073 129.341 37.9031 130.214 20.6208 121.007" stroke="#144A64" stroke-width="5" stroke-linejoin="round"/>
            <path d="M155.28 177.494C155.28 178.89 140.937 178.382 138.916 178.382C136.896 178.382 124.242 178.863 121.269 178.382C105.109 179.712 33.032 184.951 24.6495 173.301C14.9465 159.81 23.615 129.292 23.615 129.292C45.3852 136.853 65.8258 135.478 65.8258 135.478L64.0397 147.067C81.319 149.197 112.5 160.151 123.101 163.988C125.374 163.26 134.027 160.641 138.408 161.639C143.477 162.78 149.566 167.344 150.199 168.612C150.831 169.881 155.28 176.1 155.28 177.494Z" fill="#FFCFC9"/>
            <path d="M138.907 178.382C140.928 178.382 155.271 178.89 155.271 177.497C155.271 176.103 150.831 169.89 150.198 168.622C149.566 167.353 143.48 162.789 138.408 161.648C138.408 161.648 144.667 170.341 138.907 178.382Z" fill="#FAB6AD"/>
            <path d="M23.6029 129.301C23.6029 129.301 45.8842 137.139 65.8258 135.481L64.0398 147.067C64.0398 147.067 37.4287 142.808 22.6597 133.169L23.6029 129.301Z" fill="#FAB6AD"/>
            <path d="M216.373 383.786H167.104C167.055 383.034 167.037 382.316 167.052 381.626H216.437C216.491 382.346 216.47 383.07 216.373 383.786Z" fill="#144A64"/>
            <path d="M172.124 361.727C172.3 367.1 167.107 370.505 167.107 381.61H216.373C216.373 381.61 217.088 372.294 207.245 370.146C197.402 367.997 188.433 365.128 187.538 361.727H172.124Z" fill="#EAC646"/>
            <path d="M187.876 371.764L192.373 365.107" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M191.384 372.574L195.613 366.458" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M195.972 372.844L199.392 367.267" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M119.139 192.932C119.139 192.932 194.335 188.228 212.609 209.728C230.883 231.227 195.412 307.902 191.828 354.482C191.828 354.482 173.834 356.274 166.528 354.482C159.223 352.69 152.864 273.055 171.272 251.196C171.272 251.196 101.291 267.295 74.0012 246.991C46.7117 226.688 107.787 193.316 119.139 192.932Z" fill="#383C4D"/>
            <path d="M187.532 361.727C182.536 363.415 177.123 363.415 172.127 361.727L170.669 355.081C170.669 355.081 180.242 355.565 188.986 354.728L187.532 361.727Z" fill="#FFCFC9"/>
            <path d="M327.71 309.18L288.627 339.177C288.073 338.502 287.573 337.842 287.127 337.197L326.055 307.318C326.676 307.873 327.231 308.498 327.71 309.18Z" fill="#144A64"/>
            <path d="M278.881 318.457C282.295 322.613 280.244 328.476 287.008 337.288L326.091 307.29C326.091 307.29 320.986 299.465 311.861 303.758C302.736 308.051 293.885 311.234 291.101 309.076L278.881 318.457Z" fill="#EAC646"/>
            <path d="M297.591 316.355L297.107 308.599" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M300.56 314.465L300.077 307.519" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M303.53 312.576L303.046 306.439" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M61.4929 249.151C62.6187 255.218 126.606 269.921 194.694 226.94C194.694 226.94 199.17 278.787 267.967 316.394C267.967 316.394 282.937 310.99 289.665 299.115C289.665 299.115 244.785 246.632 227.506 198.436C220.252 178.2 128.958 188.794 115.61 192.932C102.261 197.07 56.4756 222.111 61.4929 249.151Z" fill="#41465A"/>
            <path d="M291.101 309.076C291.101 309.076 288.152 318.156 278.881 318.457L272.839 314.173C278.303 311.38 283.162 307.537 287.139 302.863L291.101 309.076Z" fill="#FFCFC9"/>
            <path d="M120.289 38.0187C120.289 38.0187 117.444 45.4975 98.9907 52.6234C98.9907 52.6234 111.977 70.3986 125.185 87.0997C125.185 87.0997 130.479 71.7556 140.048 65.3995L120.289 38.0187Z" fill="#FFCFC9"/>
            <path d="M140.066 65.3934C137.246 67.3194 134.87 69.9209 132.904 72.6958C126.819 68.1318 119.976 60.1205 117.949 54.309C116.948 51.4337 119.599 43.6597 121.135 39.1688L140.066 65.3934Z" fill="#FAB6AD"/>
            <path d="M119.918 39.4791C119.918 41.9133 117.803 49.4073 120.362 52.4135C125.042 57.9115 138.448 66.7139 145.041 66.3458C149.739 66.0932 156.393 45.7135 157.385 42.6678C158.377 39.6221 162.661 27.4089 159.619 24.1442C156.576 20.8794 131.142 13.7352 126.089 24.2567C120.487 35.9192 119.918 39.4791 119.918 39.4791Z" fill="#FFCFC9"/>
            <path d="M170.082 17.0609C167.584 33.403 156.941 35.5389 135.974 23.2648C126.369 17.6451 133.16 27.7102 127.759 36.4487L125.994 37.1059C126.052 36.2265 126.177 32.5936 124.369 32.5845C121.707 32.5845 120.889 36.613 120.633 39.0988L119.611 39.4791C119.611 39.4791 119.501 21.2628 122.084 15.4726C128.474 1.13866 146.943 8.88222 151.473 11.3194C165.768 19.0021 170.082 17.0609 170.082 17.0609Z" fill="#383C4D"/>
            <path d="M135.502 22.0143C150.411 33.7833 162.126 36.5734 167.943 29.6544" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
            <path d="M123.067 27.3675L123.038 27.3592C119.469 26.344 115.753 28.414 114.738 31.9827L113.91 34.8917C112.895 38.4605 114.965 42.1765 118.534 43.1918L118.563 43.2001C122.132 44.2153 125.848 42.1453 126.863 38.5765L127.691 35.6676C128.706 32.0988 126.636 28.3828 123.067 27.3675Z" fill="white" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M127.938 34.7935L126.606 39.4791C126.184 40.9576 125.193 42.2082 123.85 42.9562C122.507 43.7042 120.922 43.8883 119.443 43.4681L117.642 42.9539C117.563 42.9326 117.487 42.9082 117.408 42.8808C118.862 43.233 120.396 43.0079 121.687 42.2526C122.978 41.4974 123.927 40.271 124.333 38.8311L125.665 34.1454C126.078 32.7079 125.917 31.1667 125.217 29.8453C124.517 28.5239 123.332 27.5253 121.911 27.059L122.148 27.1199L123.94 27.625C124.675 27.8305 125.362 28.1794 125.961 28.6516C126.561 29.1239 127.061 29.7101 127.432 30.3765C127.804 31.0429 128.04 31.7763 128.127 32.5344C128.214 33.2925 128.15 34.0603 127.938 34.7935Z" fill="white" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M122.148 27.1107L127.625 7.88118C127.866 7.03245 128.272 6.23956 128.82 5.5478C129.368 4.85603 130.046 4.27893 130.817 3.84947C131.588 3.42 132.436 3.14657 133.312 3.04481C134.189 2.94304 135.077 3.01492 135.925 3.25634L155.398 8.79397C157.808 9.47857 158.903 11.3802 158.216 13.7931" stroke="#144A64" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
            <path d="M213.127 179.149H127.753C123.855 179.149 121.385 175.251 123.265 172.069L152.17 123.183C154.684 118.923 159.473 116.288 164.694 116.288H250.071C253.968 116.288 256.439 120.186 254.559 123.368L225.653 172.254C223.131 176.508 218.336 179.149 213.127 179.149Z" fill="#E1F5EE"/>
            <path d="M212.29 180.719H130.421C126.603 180.719 124.184 176.901 126.028 173.782L154.346 125.887C156.81 121.719 161.511 119.133 166.617 119.133H248.485C252.304 119.133 254.723 122.951 252.879 126.07L224.561 173.961C222.097 178.13 217.396 180.719 212.29 180.719Z" fill="#EAC646"/>
            <path d="M213.127 179.149H127.753C123.855 179.149 121.385 175.251 123.265 172.069L152.17 123.183C154.684 118.923 159.473 116.288 164.694 116.288H250.071C253.968 116.288 256.439 120.186 254.559 123.368L225.653 172.254C223.131 176.508 218.336 179.149 213.127 179.149Z" stroke="#A8832D" stroke-width="5" stroke-miterlimit="10"/>
            <path d="M214.277 179.213H58.3805C56.7371 179.213 55.4048 180.545 55.4048 182.189V182.846C55.4048 184.489 56.7371 185.822 58.3805 185.822H214.277C215.92 185.822 217.253 184.489 217.253 182.846V182.189C217.253 180.545 215.92 179.213 214.277 179.213Z" fill="#EAC646" stroke="#A8832D" stroke-width="6" stroke-miterlimit="10"/>
        </svg>
        </div>
        <div className="selection-warning">
            You're currently not subscribed<br/>to any of the classes.
        </div>
        </>
    )
    }

    else {
        return(
            <CircularProgress style={{color:"#EAC646",alignSelf:"center"}}/>
        )
    }
}