import React from 'react';

export const Loading = () =>{
    return(
        <span className="spinner-border spinner-border-sm white-it"></span>  
    );
}