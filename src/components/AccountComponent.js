import React, {useState,useContext,useEffect} from 'react';
import Topbar from './TopbarComponent.js';
import FooterNew from './FooterNewComponent';
import {Sidebar, NotableQuestions} from './ForumNewComponent';
import{FirebaseContext} from '../contexts/FirebaseContext';

import { Link, useParams } from 'react-router-dom';

import './styles/account.css';

import pic from './img/profilepic.png';
import bag from './img/bag.png';
import book from './img/book.png';
import headphones from './img/headphones.png';
import pc from './img/pc.png';

export default function Account(){

    const fire = useContext(FirebaseContext);
    const [name, setName]=useState("");
    const [loading,setLoading]=useState(false);

    const[display,setDisplay]=useState(false);

    useEffect(()=>{
        setLoading(true);
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        const unsubscribe=docQuery.onSnapshot(querySnapshot => { 
            setName(querySnapshot.data());
            setLoading(false);
        }
        )

        return unsubscribe
        }
    
    },[fire.status]);
/* 
    useEffect(()=>{
        if (fire.status){
            fire.store.collection("users").get().then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {
                    doc.ref.update({
                        pic: 1
                    });
                });
            });
        }
    },[fire.status]) */

    const [pict,setPict] = useState([bag,book,headphones,pc]);

    async function changePic(i){
        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            pic: i
        })
    }

    return(
        <>
            <Topbar/>
                <div className="account-wrapper">
                    <div className="account-intro">
                        <div className="account-heading">My Account</div>
                        <div className="account-description">You can track the progress you’ve made on Studybuddy through this page.</div>
                    </div>
                    <div className="account-details-wrapper">
                        <img onClick={()=>setDisplay(true)}  role="button" className="account-pic" src={pict[name.pic]}/>
                        <div className="choices-wrapper" style={{display:display?"grid":"none"}}>
                            <img role="button" onClick={()=>{setDisplay(false);changePic(0);}} className="account-pic-choices" src={bag}/>
                            <img role="button"onClick={()=>{setDisplay(false);changePic(1);}} className="account-pic-choices" src={book}/>
                            <img role="button" onClick={()=>{setDisplay(false);changePic(2);}} className="account-pic-choices" src={headphones}/>
                            <img role="button" onClick={()=>{setDisplay(false);changePic(3);}} className="account-pic-choices" src={pc}/>
                        </div>
                        <div className="account-name-wrapper">
                            <div className="account-name">{fire.status?fire.auth.currentUser.displayName:"Loading stats ..."}</div>
                            <div className="account-email">{fire.status?fire.auth.currentUser.email:""}</div>
                            <Link to="/" style={{textDecoration:"none",color:"inherit"}}>
                                <div className="account-logout-button" onClick={()=>fire.status?fire.signout(fire.auth.currentUser.uid):fire.signin()} role="button">{fire.status?"LOGOUT":""}</div>
                            </Link>
                        </div>
                    </div>
                    <div className="account-stats-wrapper">
                        <div className="account-stats-points">
                            <div>Total Studybuddy Points</div>
                            <div>{name.upvotes}</div>
                        </div>
                        <div className="account-stats-earnings">
                            <div>Total Studybuddy Earnings</div>
                            <div>Php {name.upvotes}.00</div>
                        </div>
                        <div className="account-stats-metrics">
                            <div className="account-stats-metric">
                                <div className="account-metric-heading">Subjects Added</div>
                                <div className="account-metric-number">{name.uSubjects?name.uSubjects.length<10?`0${name.uSubjects.length}`:name.uSubjects.length:"00"}</div>
                                <div className="account-metric-view">
                                    <Link to="/dashboard" style={{textDecoration:"none",color:"inherit"}}>
                                        View All →
                                    </Link>
                                </div>
                            </div>
                            <div className="account-stats-metric">
                                <div className="account-metric-heading">Questions Asked</div>
                                <div className="account-metric-number">{name.uQuestions?name.uQuestions.length<10?`0${name.uQuestions.length}`:name.uQuestions.length:"00"}</div>
                                <div className="account-metric-view">
                                    <Link to="/dashboard" style={{textDecoration:"none",color:"inherit"}}>
                                        View All →
                                    </Link>
                                </div>
                            </div>
                            <div className="account-stats-metric">
                                <div className="account-metric-heading">Answers Made</div>
                                <div className="account-metric-number">{name.uAnswers?name.uAnswers.length<10?`0${name.uAnswers.length}`:name.uAnswers.length:"00"}</div>
                                <div className="account-metric-view">
                                    <Link to="/dashboard" style={{textDecoration:"none",color:"inherit"}}>
                                        View All →
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <FooterNew/>
        </>
    )
}