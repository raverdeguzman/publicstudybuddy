
import React, {useState,useContext,useEffect,useRef} from 'react';
import{FirebaseContext} from '../contexts/FirebaseContext';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Typography, Tab,Tabs, CssBaseline, TextField } from '@material-ui/core';
import Switch from '@material-ui/core/Switch';


import {Link} from 'react-router-dom';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


export default function Mod () {
    const fire = useContext(FirebaseContext);
    const [emailOne,setEmailOne] = useState("");
    const [emailTwo,setEmailTwo] = useState("");
    const [emailThree,setEmailThree] = useState("");
    const [org,setOrg] = useState("");

    const [dataLimit,setDataLimit]=useState(5);
    const [orderBy,setOrderBy]=useState("timestamp");

    const [questions,setQuestions]=useState([]);
    const [edit,setEdit]=useState(false);
    const [subjectCode,setSubjectCode]=useState(null);
    const [seq,setSeq]=useState("desc");
    const [seqTwo,setSeqTwo]=useState("desc");
    const [sort,setSort]=useState(true);
    const [sortTwo,setSortTwo]=useState(false);

    const [ans,setAns]=useState([""]);
    const [num,setNum]=useState(0);

    const [user, setUser]=useState([]);
    const [uid, setUid]=useState([""]);
    const [dataLimitTwo,setDataLimitTwo]=useState(5);

    const userRef = useRef("");
    userRef.current=user



    const capitalize = (str, lower = true) =>
    (lower ? str.toLowerCase() : str).replace(/(?:^|\s|["'([{])+\S/g, match => match.toUpperCase());
    ;


    useEffect(()=>{
        if (fire.status){
        const unsubscribe = fire.store.collection("forum").where("show","==",true).orderBy(orderBy,seq).limit(dataLimit).onSnapshot(querySnapshot=>{
            let z=[];
            querySnapshot.forEach(queryDocumentSnapshot=>{
                z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});
            })
            setQuestions(z);
        });
        }

    },[fire.status,dataLimit,orderBy,seq])

    useEffect(()=>{
        if (fire.status){
        const unsubscribeTwo = fire.store.collection("users").orderBy("upvotes",seqTwo).limit(dataLimitTwo).onSnapshot(querySnapshot=>{
            let z=[];    
            querySnapshot.forEach(queryDocumentSnapshot=>{
                    z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});
                }) 
                setUser(z); 
                });
                }
        },[fire.status,dataLimitTwo,seqTwo])    

 

    useEffect(()=>{
			if (fire.status){
			const unsubscribe = fire.store.collection("forum").where("answerers","array-contains-any",uid).get().then(querySnapshot=>{
                let y=[];
                querySnapshot.forEach(queryDocumentSnapshot=>{		
					y.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});
                })
                setAns(y);
            });
        }
		},[fire.status,uid])

    useEffect(()=>{
			if (fire.status){
            const unsubscribe = fire.store.collection("stats").doc("number of users").onSnapshot(querySnapshot=>
                setNum(querySnapshot.data().number)
            );
        }
		},[fire.status])

    async function hideIt(id){
        await fire.store.collection("forum").doc(id).update({
            show:false
        });

    }

    async function editSubjectCode(id){
        await fire.store.collection("forum").doc(id).update({
            subjectCode:subjectCode
        })

        setEdit(false);
        setSubjectCode(null);
    }

if (fire.moderator){
    return(
    <Grid container 
    direction="column"
    justify="center"
    alignItems="center"
    spacing={3}
    style={{ minHeight: '100vh', maxWidth: '100%'}}
    >
  
{/*                         <Grid item>
                            <input value={emailOne} placeholder="Make Admin" onChange={e => setEmailOne(e.target.value)} type="text"/>
                            <button onClick={()=>{
                                fire.functions.httpsCallable('addAdminRole')({email:emailOne})
                                .then(result=>console.log(result));
                                }}>Submit</button>
                        </Grid>  */}
                       <Grid item>
                            <Typography>Before you add a moderator, ask permission from Raver De Guzman first.</Typography>
                            <label>Add a moderator:</label>
                            <input value={emailTwo} placeholder="Add Moderator's email address" onChange={e => setEmailTwo(e.target.value)} type="text"/>
                            <button onClick={()=>{
                                fire.functions.httpsCallable('addModeratorRole')({email:emailTwo})
                                .then(result=>console.log(result));
                                }}>Submit</button>
                        </Grid>
                        <Grid item>
                            Always <a href="#" onClick={()=>fire.signout()}>signout</a> after use!
                        </Grid>
{/*                         {fire.moderator  ? <Grid item>
                        <label>Add a tutor:</label>
                            <input value={emailThree} placeholder="Add Tutor's email address" onChange={e => setEmailThree(e.target.value)} type="text"/>
                            <input value={org} placeholder="Org" onChange={e => setOrg(e.target.value)} type="text"/>
                            <button onClick={()=>{
                                fire.functions.httpsCallable('addTutorRole')({email:emailThree, org:org})
                                .then(result=>console.log(result));
                                }}>Submit</button>
                        </Grid> :
                        <div></div>} */}
                        <Grid item>
                        <Typography variant="h4">Questions</Typography>
                        <Typography>This is in realtime. Please do not reload to save reads.</Typography>
                            <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                <TableRow>
                                    <TableCell align="right">Row</TableCell>
                                    <TableCell>URL to Question</TableCell>
                                    <TableCell>Subject Code</TableCell>
                                    <TableCell>UID</TableCell>
                                    <TableCell align="right"><Button onClick={()=>{setOrderBy("flagLength");setSeq("desc");}}>{orderBy=="flagLength"?"Sorting by Flags":"Flags"}</Button></TableCell>
                                    <TableCell align="right"><Button onClick={()=>{setSort(!sort);setOrderBy("answersLength");setSeq(sort?"desc":"asc");}}>{orderBy=="answersLength"?"Sorting by Answers":"Answers"}</Button></TableCell>
                                    <TableCell align="right"><Button onClick={()=>{setOrderBy("timestamp");setSeq("desc");}}>{orderBy=="timestamp"?"Sorting by Timestamp":"Timestamp"}</Button></TableCell>
                                    <TableCell align="right">Show?</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                {questions.map((row,i) => (
                                    <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {i+1}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        <Link to={`question/${row.id}`}>
                                        {row.content.question}
                                        </Link>
                                    </TableCell>
                                    <TableCell align="right">{edit===i?<input value={subjectCode} onChange={e=>setSubjectCode(e.target.value)} placeholder="Edit"/>:row.content.subjectCode}{Number.isInteger(edit)?<Button onClick={()=>editSubjectCode(row.id)}>Save</Button>:<Button onClick={()=>setEdit(i)}>Edit</Button>}</TableCell>
                                    <TableCell align="right">{row.content.flagCount}</TableCell>
                                    <TableCell align="right">{row.content.user}</TableCell>
                                    <TableCell align="right">{row.content.answersLength}</TableCell>
                                    <TableCell align="right">{(new Date(row.content.timestamp.seconds*1000)).toLocaleString()}</TableCell>
                                    <TableCell align="right"><Button onClick={()=>hideIt(row.id)}>{row.content.show?"HIDE":"false"}</Button></TableCell>
                                    </TableRow>
                                ))}
                                </TableBody>
                            </Table>
                            </TableContainer>
                            <Grid item container justify="center">
                                <Button disabled={questions.length<dataLimit?true:false} onClick={()=>setDataLimit(y=>y+5)}>Load 5 more</Button>
                            </Grid>
                        </Grid>


                        <Grid item>
                                <Typography variant="h4">Users ({num})</Typography>
                        <Typography>This is in realtime. Please do not reload to save reads.</Typography>
                            <TableContainer component={Paper} >
                            <Table aria-label="simple table">
                                <TableHead>
                                <TableRow>
                                    <TableCell>Row</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell>UID</TableCell>
                                    <TableCell>Answers</TableCell>
                                    <TableCell>Questions Answered</TableCell>
                                    <TableCell>Questions Asked</TableCell>
                                    <TableCell align="right"><Button onClick={()=>{setSortTwo(!sortTwo);setSeqTwo(sortTwo?"desc":"asc");}}>Upvotes</Button></TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                {user.map((row,i) => (
                                    <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {i+1}
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {row.content.email}
                                    </TableCell>
                                    <TableCell onClick={()=>setUid([row.content.uid])}role="button" component="th" scope="row">
                                        {row.content.uid}
                                    </TableCell>
                                    <TableCell align="right">{row.content.uAnswers.length}</TableCell>
                                    <TableCell align="right">{ans.length&&row.content.uid==uid?ans.map((el,i)=><Link to={`/question/${el.id}`}><Typography>{`[${i+1}] ${el.content.question}`}</Typography></Link>):""}</TableCell>
                                    {/* <TableCell align="right">{row.content.uAnswers.map(y=><TableCell>{y}</TableCell>)}</TableCell> */}
                                    <TableCell align="right">{row.content.uQuestions.length}</TableCell>
                                    <TableCell align="right">{row.content.upvotes}</TableCell>
                                    </TableRow>
                                ))}
                                </TableBody>
                            </Table>
                            </TableContainer>
                            <Grid item container justify="center">
                                <Button disabled={user.length<dataLimitTwo?true:false} onClick={()=>setDataLimitTwo(y=>y+5)}>Load 5 more</Button>
                            </Grid>
                            <Grid item>
                                
                            </Grid>
                        </Grid>
                    
            </Grid>
)
}

else{
    return(
        <div>You are not a moderator. Nothing to see here.</div>
    )
}
}