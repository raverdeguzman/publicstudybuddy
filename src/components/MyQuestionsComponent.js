import React, {useState,useContext,useEffect,useRef} from 'react';
import Topbar from './TopbarComponent.js';
import FooterNew from './FooterNewComponent';
import{FirebaseContext} from '../contexts/FirebaseContext';

import { Link } from 'react-router-dom';

import {SavedQuestions} from './MyDashboard';

import './styles/mydashboard.css';
import './styles/selection.css';


export default function MyQuestions(){
    const fire = useContext(FirebaseContext);
    const [name, setName]=useState("");
    const [loading,setLoading]=useState(false);

    const [orderBy, setOrderBy]=useState("timestamp");
    const [desc, setDesc]=useState("desc");
    const [select,setSelect]=useState("Newest");
    const [showSelect,setShowSelect]=useState(false)
    const wrapperRef=useRef(null);

    useEffect(() => {
        window.addEventListener("click", handleClickOutside);
        return () => {
          window.removeEventListener("click", handleClickOutside);
        };
      });
    
      const handleClickOutside = event => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
                setShowSelect(false);
          console.log(event.target);
        }
      };   


    useEffect(()=>{
        setLoading(true);
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        docQuery.onSnapshot(querySnapshot => { 
            setName(querySnapshot.data());
            setLoading(false);
        }
        )
        }
    
    },[fire.status]);

    return(
        <>
        <Topbar/>
            <div className="ownpage-savedquestions-wrapper">
                <div className="dashboard-content-intro">
                <div className="dashboard-heading">My Questions
                            <div className="dashboard-count">{name.uQuestions?name.uQuestions.length:"0"}</div>
                        </div>
                    <div className="dashboard-description">Questions you’ve made can be found here.</div>
                </div>
                <div ref={wrapperRef}className="sort-by-wrapper">
                        <div className="sort-by-label">Sort by</div>
                        <div role="button" onClick={()=>setShowSelect(!showSelect)} className="sort-by-display">
                            {select}
                            <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.4958 0.355336L6.7821 5.06909L2.06834 0.355336C1.84136 0.127846 1.53321 0 1.21185 0C0.890489 0 0.582333 0.127846 0.355354 0.355336C-0.118451 0.829141 -0.118451 1.59452 0.355354 2.06832L5.93168 7.64465C6.40548 8.11845 7.17086 8.11845 7.64466 7.64465L13.221 2.06832C13.6948 1.59452 13.6948 0.829141 13.221 0.355336C12.7472 -0.10632 11.9697 -0.118469 11.4958 0.355336Z" fill="#EAC646"/>
                            </svg>
                        </div>
                        <div className="sort-by-choices-wrapper" style={{display:`${showSelect?"flex":"none"}`}}>
{/*                             <div className={`sort-by-element ${select==="Most Likes"?"sort-by-element-selected":""}`} role="button" onClick={()=>{setOrderBy("likes"); setDesc(-1);setShowSelect(false);setSelect("Most Likes");}}>Most Likes</div>
                            <div className={`sort-by-element ${select==="Newest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc(1);setShowSelect(false);setSelect("Newest");}}>Newest</div>
                            <div className={`sort-by-element ${select==="Oldest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc(-1);setShowSelect(false);setSelect("Oldest");}}>Oldest</div>
 */}                     
                             <div className={`sort-by-element ${select==="Most Likes"?"sort-by-element-selected":""}`} role="button" onClick={()=>{setOrderBy("likes"); setDesc("desc");setShowSelect(false);setSelect("Most Likes");}}>Most Likes</div>
                            <div className={`sort-by-element ${select==="Newest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc("desc");setShowSelect(false);setSelect("Newest");}}>Newest</div>
                            <div className={`sort-by-element ${select==="Oldest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc("asc");setShowSelect(false);setSelect("Oldest");}}>Oldest</div>

                        </div>
                    </div>
                <SavedQuestions questionsaved={name.uQuestions} count={5} name={name} show={false} orderby={orderBy} desc={desc} sorter={[orderBy,desc,-desc]}/>
            </div>
        <FooterNew/>
        </>
    )
}


/* function SavedQuestions ({show}){

    const array = Array.from(Array(5).keys())

    const questions = array.map((element,i)=>
        <div className="savedquestions-wrapper">
            <div className="savedquestions-subject">SUBJ 00</div>
            <div className="savedquestions-question">Nam dictum eget augue eget dictum. Curabitur imperdiet neque purus, eu vestibulum ex vestibulum eu.</div>
            <div className="savedquestions-description">Nam dictum eget augue eget dictum. Curabitur imperdiet neque purus, eu vestibulum ex vestibulum eu. Phasellus sed odio mattis, ultricies nisi a, lobortis odio. Phasellus semper nunc dapibus odio pharetra, ac tristique libero feugiat. Donec sit amet commodo sem. Proin ut convallis augue. Duis dignissim venenatis accumsan. Donec et felis sit amet tortor pulvinar vestibulum eu ac lacus.</div>
            <div className="savedquestions-menu-wrapper">
                <div className="savedquestions-menu-like">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.74561 21H5.74561V9H1.74561V21ZM23.7456 10C23.7456 8.9 22.8456 8 21.7456 8H15.4356L16.3856 3.43L16.4156 3.11C16.4156 2.7 16.2456 2.32 15.9756 2.05L14.9156 1L8.33561 7.59C7.96561 7.95 7.74561 8.45 7.74561 9V19C7.74561 20.1 8.64561 21 9.74561 21H18.7456C19.5756 21 20.2856 20.5 20.5856 19.78L23.6056 12.73C23.6956 12.5 23.7456 12.26 23.7456 12V10Z" fill="#EAC646"/>
                    </svg>
                    <div className="savedquestions-menu-like-number">21</div>
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.7456 3H6.74561C5.91561 3 5.20561 3.5 4.90561 4.22L1.88561 11.27C1.79561 11.5 1.74561 11.74 1.74561 12V14C1.74561 15.1 2.64561 16 3.74561 16H10.0556L9.10561 20.57L9.07561 20.89C9.07561 21.3 9.24561 21.68 9.51561 21.95L10.5756 23L17.1656 16.41C17.5256 16.05 17.7456 15.55 17.7456 15V5C17.7456 3.9 16.8456 3 15.7456 3ZM19.7456 3V15H23.7456V3H19.7456Z" fill="#E0E0E0"/>
                    </svg>
                </div>
                <div className="savedquestions-menu-timestamp">01/01/1970, 12:00:00 AM (Edited by the Moderator)</div>
                <div className="savedquestions-menu-comments">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20.1685 2H4.16846C3.06846 2 2.16846 2.9 2.16846 4V16C2.16846 17.1 3.06846 18 4.16846 18H18.1685L22.1685 22V4C22.1685 2.9 21.2685 2 20.1685 2ZM18.1685 14H6.16846V12H18.1685V14ZM18.1685 11H6.16846V9H18.1685V11ZM18.1685 8H6.16846V6H18.1685V8Z" fill="black"/>
                    </svg>
                    <div className="savedquestions-menu-comments-number">21</div>
                </div>
            </div>
        </div>
    )

        return(
            <>
            <div className="dashboard-content-savedquestions">
                {questions}
                <div className="dashboard-content-savedquestions-viewall-wrapper">
                <div className="dashboard-content-savedquestions-viewall">See More</div>
            </div>
            </div>
            </>
        )
    } */
