import React, {useState,useContext,useEffect} from 'react';
import Topbar from './TopbarComponent.js';
import FooterNew from './FooterNewComponent';
import { Link } from 'react-router-dom';

import{FirebaseContext} from '../contexts/FirebaseContext';
import Latex from 'react-latex';

import './styles/mydashboard.css';
import './styles/selection.css';

import {Subjects} from './SelectionComponent';

import MoreVertIcon from '@material-ui/icons/MoreVert';

import pic from './img/profilepic.png';

import bag from './img/bag.png';
import book from './img/book.png';
import headphones from './img/headphones.png';
import pc from './img/pc.png';



export function MyDashboard (){
    const fire = useContext(FirebaseContext);
    const [name, setName]=useState("");
    const [loading,setLoading]=useState(false);

    useEffect(()=>{
        setLoading(true);
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        docQuery.onSnapshot(querySnapshot => { 
            setName(querySnapshot.data());
            setLoading(false);
        }
        )
        }
    
    },[fire.status]);

    return(
        <>
        <Topbar/>
            <div className="dashboard-wrapper">
                <div className="dashboard-content-1">
                    <div className="dashboard-content-intro">
                        <div className="dashboard-heading">Welcome{`${fire.status?`, ${fire.auth.currentUser.displayName}!`:"!"}`}</div>
                        <div className="dashboard-description">You can find all your saved subjects, questions, and answers here. Why not take a look?</div>
                    </div>
                    <div className="dashboard-content-1-body">
                        <div className="dashboard-heading-subjects">My Subjects
                        <Link to="/selection" style={{textDecoration:"none"}}>
                            <svg role="button" style={{marginLeft:16}} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0 12C0 5.4 5.4 0 12 0C18.6 0 24 5.4 24 12C24 18.6 18.6 24 12 24C5.4 24 0 18.6 0 12ZM13.5 13.5001H18V10.5H13.5V6.00001H10.5V10.5H6V13.5001H10.5V18.0001H13.5V13.5001Z" fill="#077955"/>
                            </svg>
                        </Link>
                        </div>
                        <Subjects show={true}/>
                    </div>
                </div>
                <div className="dashboard-content-2">
                    <div className="dashboard-content-intro">
                        <div className="dashboard-heading">My Questions
                            <div className="dashboard-count">{name.uQuestions?name.uQuestions.length:"0"}</div>
                        </div>
                        <div className="dashboard-description">Questions you’ve asked can be found here.</div>
                    </div>
                    <SavedQuestions questionsaved={name.uQuestions} show={true} count={2}/>
                </div>
                <div className="dashboard-content-3">
                <div className="dashboard-content-intro">
                        <div className="dashboard-heading">My Answers
                            <div className="dashboard-count">{name.uAnswers?name.uAnswers.length:"0"}</div>
                        </div>
                        <div className="dashboard-description">Answers you’ve made can be found here.</div>
                </div>
                    <SavedAnswers answersaved={name.uAnswers} pic={name.pic} count={2}/>
                </div>
            </div>
        <FooterNew/>
        </>
    )
}

export function SavedQuestions ({questionsaved,name,count=2,show,orderby="timestamp", desc="desc",sorter=["timestamp",-1,1]}){

    const array = Array.from(Array(2).keys())

    const fire = useContext(FirebaseContext);
    const [docs, setDocs]=useState([]);
    const [loading,setLoading]=useState(false);
    const [counter, setCounter]=useState(count);


/*     useEffect(()=>{
		if (questionsaved){
			setLoading(true);
			setDocs([]);
			questionsaved.map ((id,i)=>{
				fire.store.collection("forum").doc(id).get().then(docSnapshot=>{
					setDocs(y=>y.concat({id:id,content:docSnapshot.data()}));
				})
			})
			setLoading(false);
		}
    },[questionsaved]) */


    useEffect(()=>{
        if (fire.status){
            setLoading(true);
                let y=[]
                fire.store.collection("forum").where("user","==",fire.auth.currentUser.uid).orderBy(orderby,desc).limit(counter).onSnapshot(docSnapshot=>{
                    docSnapshot.forEach(queryDocumentSnapshot=>{
                    y.push({id:queryDocumentSnapshot.id,content:queryDocumentSnapshot.data()});
                    })
                    setDocs(y);
                })
                setLoading(false);
        }
    },[fire.status,orderby,desc,counter])
    /* docs.slice(0,counter).sort((a,b)=>a.content[sorter[0]]-b.content[sorter[0]]?sorter[1]:sorter[2]).map((element,i) */
    const questions = docs?docs.map((element,i)=>
        
        <div className="savedquestions-wrapper">
        <Link to={`/forum/${element.content.subjectCode}/${element.id}`} style={{textDecoration:"none", color:"inherit"}}>
            <div className="savedquestions-subject">{element.content.subjectCode}</div>
            <div className="savedquestions-question"><Latex>{element.content.question}</Latex></div>
            <div className="savedquestions-description"><Latex>{element.content.description}</Latex></div>
            <div className="savedquestions-menu-wrapper">
                <div className="savedquestions-menu-like">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.74561 21H5.74561V9H1.74561V21ZM23.7456 10C23.7456 8.9 22.8456 8 21.7456 8H15.4356L16.3856 3.43L16.4156 3.11C16.4156 2.7 16.2456 2.32 15.9756 2.05L14.9156 1L8.33561 7.59C7.96561 7.95 7.74561 8.45 7.74561 9V19C7.74561 20.1 8.64561 21 9.74561 21H18.7456C19.5756 21 20.2856 20.5 20.5856 19.78L23.6056 12.73C23.6956 12.5 23.7456 12.26 23.7456 12V10Z" 
                        fill={(element.content.likers?element.content.likers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                    <div className="savedquestions-menu-like-number">{element.content.likes?element.content.likes:"0"}</div>
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.7456 3H6.74561C5.91561 3 5.20561 3.5 4.90561 4.22L1.88561 11.27C1.79561 11.5 1.74561 11.74 1.74561 12V14C1.74561 15.1 2.64561 16 3.74561 16H10.0556L9.10561 20.57L9.07561 20.89C9.07561 21.3 9.24561 21.68 9.51561 21.95L10.5756 23L17.1656 16.41C17.5256 16.05 17.7456 15.55 17.7456 15V5C17.7456 3.9 16.8456 3 15.7456 3ZM19.7456 3V15H23.7456V3H19.7456Z" 
                        fill={(element.content.dislikers?element.content.dislikers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                </div>
                <div className="savedquestions-menu-timestamp">{element.content.timestamp?(new Date(element.content.timestamp.seconds*1000)).toLocaleString():"Loading Timestamp"}</div>
                <div className="savedquestions-menu-comments">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20.1685 2H4.16846C3.06846 2 2.16846 2.9 2.16846 4V16C2.16846 17.1 3.06846 18 4.16846 18H18.1685L22.1685 22V4C22.1685 2.9 21.2685 2 20.1685 2ZM18.1685 14H6.16846V12H18.1685V14ZM18.1685 11H6.16846V9H18.1685V11ZM18.1685 8H6.16846V6H18.1685V8Z" fill="black"/>
                    </svg>
                    <div className="savedquestions-menu-comments-number">{element.content.answersLength}</div>
                </div>
            </div>
        </Link>
        </div>
    ):<div></div>

    if (docs.length){
        return(
            <>
            <div className="dashboard-content-savedquestions">
                {questions}
                {show?<Link to="/dashboard/questions" style={{ textDecoration: 'none', color:"#161716"}}>
                    <div role="button" className="dashboard-content-savedquestions-viewall-wrapper">
                        <div className="dashboard-content-savedquestions-viewall">View all</div>
                        <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.355337 2.08032L5.06909 6.79408L0.355336 11.5078C0.127846 11.7348 1.82833e-08 12.043 1.44511e-08 12.3643C1.0619e-08 12.6857 0.127846 12.9938 0.355336 13.2208C0.829141 13.6946 1.59452 13.6946 2.06832 13.2208L7.64465 7.6445C8.11845 7.17069 8.11845 6.40531 7.64465 5.93151L2.06832 0.355186C1.59452 -0.118619 0.829142 -0.118619 0.355337 0.355186C-0.10632 0.828991 -0.118469 1.60652 0.355337 2.08032Z" fill="#EAC646"/>
                        </svg>
                    </div>
                </Link>:
                <div role="button" onClick={()=>setCounter(y=>y+5)} style={{display:`${name.length>5&&name.length%5?"none":"flex"}`}} className="dashboard-content-savedquestions-viewall">See More</div>
}
            </div>
            </>
        )
    }

    else{
    return(
        <div className="dashboard-content-body">
            <div>
            <svg width="302" height="405" viewBox="0 0 302 405" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M63.068 281.345C91.9492 281.345 115.362 276.887 115.362 271.388C115.362 265.889 91.9492 261.432 63.068 261.432C34.1868 261.432 10.7739 265.889 10.7739 271.388C10.7739 276.887 34.1868 281.345 63.068 281.345Z" fill="#D8BAA3" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M59.9748 281.348V351.311C59.9734 355.132 58.9027 358.876 56.8839 362.12C54.8651 365.363 51.9788 367.977 48.5515 369.666L29.9469 378.838C28.0379 379.78 26.4305 381.237 25.3065 383.045C24.1826 384.852 23.5869 386.938 23.5869 389.067" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M23.7565 401.997C27.327 401.997 30.2215 399.102 30.2215 395.532C30.2215 391.961 27.327 389.067 23.7565 389.067C20.186 389.067 17.2915 391.961 17.2915 395.532C17.2915 399.102 20.186 401.997 23.7565 401.997Z" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M48.2301 401.997C51.8006 401.997 54.6951 399.102 54.6951 395.532C54.6951 391.961 51.8006 389.067 48.2301 389.067C44.6596 389.067 41.7651 391.961 41.7651 395.532C41.7651 399.102 44.6596 401.997 48.2301 401.997Z" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M59.9749 343.096V365.016C59.9748 368.523 58.6746 371.906 56.3256 374.51L51.8828 379.45C49.5324 382.053 48.2311 385.436 48.2305 388.944" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M66.5752 281.348V351.311C66.5762 355.132 67.6468 358.876 69.6656 362.12C71.6844 365.364 74.571 367.978 77.9985 369.666L96.6 378.838C98.5098 379.779 100.118 381.236 101.243 383.044C102.367 384.852 102.963 386.938 102.963 389.067" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M102.797 401.997C106.367 401.997 109.261 399.102 109.261 395.532C109.261 391.961 106.367 389.067 102.797 389.067C99.226 389.067 96.3315 391.961 96.3315 395.532C96.3315 399.102 99.226 401.997 102.797 401.997Z" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M78.32 401.997C81.8905 401.997 84.7849 399.102 84.7849 395.532C84.7849 391.961 81.8905 389.067 78.32 389.067C74.7494 389.067 71.855 391.961 71.855 395.532C71.855 399.102 74.7494 401.997 78.32 401.997Z" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M66.5752 343.096V365.016C66.5752 368.523 67.8755 371.906 70.2245 374.51L74.6703 379.45C77.0199 382.054 78.3201 385.436 78.3196 388.944" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M111.163 357.628C111.163 357.628 111.527 369.799 104.192 374.896C97.2758 379.7 97.0195 383.565 97.0195 383.565C97.0195 383.565 108.545 394.757 111.382 395.915C114.219 397.072 124.553 400.638 129.906 400.638C129.906 400.638 133.528 397.999 125.235 390.759C113.985 380.913 126.751 363.225 126.751 363.225C126.751 363.225 115.025 359.715 111.163 357.628Z" fill="#FFC0B7"/>
                <path d="M90.5359 213.805C90.5359 213.805 149.082 220.709 172.441 245.068C185.062 258.233 135.392 337.588 133.061 364.241C133.061 364.241 115.04 361.87 103.361 352.858C103.361 352.858 105.522 279.091 132.074 265.42C132.074 265.42 81.5423 269.434 59.9183 258.412C38.2943 247.39 59.9183 217.26 59.9183 217.26C59.9183 217.26 78.7266 220.928 90.5359 213.805Z" fill="#104057"/>
                <path d="M106.044 372.117C106.044 372.117 96.3279 375.942 96.0933 382.948C96.0933 382.948 106.235 393.337 111.382 395.915C119.449 399.975 130.524 401.256 130.524 401.256C130.524 401.256 134.537 396.316 128.23 391.527C121.922 386.739 120.706 378.252 120.706 378.252C120.706 378.252 120.011 384.26 114.398 385.353C109.372 386.341 107.748 383.809 107.748 383.809C107.748 383.809 109.261 372.969 106.044 372.117Z" fill="#D6B03F"/>
                <path d="M25.2687 218.285C25.2687 218.285 14.6419 248.363 18.4764 256.76C25.6144 272.444 82.3482 277.118 124.398 269.44C124.398 269.44 109.712 321.157 127.066 363.54C127.066 363.54 138.748 365.556 155.099 363.232C155.099 363.232 154.766 312.169 162.108 257.773C163.568 246.84 100.672 221.175 72.9811 218.276C45.2903 215.377 25.2687 218.285 25.2687 218.285Z" fill="#144A64"/>
                <path d="M131.227 364.053C131.227 364.053 134.398 375.884 134.398 379.407C134.398 386.582 128.89 395.257 129.89 401.265H179.767C179.767 401.265 176.748 393.902 164.599 391.234C150.916 388.23 148.406 379.447 148.748 364.065C148.748 364.053 139.733 364.745 131.227 364.053Z" fill="#FFCFC9"/>
                <path d="M133.534 385.072C133.534 385.072 127.723 393.253 129.89 401.265H180.545C180.545 401.265 180.431 394.102 173.37 393.238C166.309 392.374 154.846 386.566 154.846 386.566C154.846 386.566 155.695 395.078 142.678 396.078C142.678 396.091 143.182 385.396 133.534 385.072Z" fill="#D6B03F"/>
                <path d="M97.9648 93.9596C90.8329 97.4977 86.5167 98.6462 88.7736 121.218C91.0305 143.79 107.44 170.162 107.44 170.162L128.78 149.125C128.78 149.125 114.399 107.291 111.386 102.66C105.516 93.6724 100.74 92.5826 97.9648 93.9596Z" fill="#EAC646"/>
                <path d="M134.609 155.911C134.609 155.911 130.237 146.031 128.082 145.93C125.927 145.828 110.669 157.161 108.527 167.189C107.838 170.425 104.637 175.834 123.071 178.19C128.786 178.918 134.609 155.911 134.609 155.911Z" fill="#D6B03F"/>
                <path d="M202.84 173.302C202.84 173.302 201.476 174.191 197.446 173.234C196.15 172.926 194.319 170.647 192.389 169.409C190.827 168.406 187.758 168.579 184.458 168.971C182.587 169.187 180.821 166.714 179.638 165.324C167.906 168.721 135.22 178.23 122.232 177.152C118.934 176.881 128.372 156.106 134.087 154.75C138.41 153.728 163.847 154.639 179.972 156.698C185.712 157.526 191.377 158.806 196.915 160.527C199.033 161.86 202.84 173.302 202.84 173.302Z" fill="#FFC0B7"/>
                <path d="M90.536 213.805C90.536 213.805 84.8769 218.495 72.9843 218.267V218.248C71.6444 195.544 68.8534 152.891 65.942 137.05C60.1099 105.346 44.2099 78.007 43.9629 77.584L74.4415 79.1957C74.4415 79.1957 90.2366 90.3627 93.6852 137.078C97.1338 183.793 90.536 213.805 90.536 213.805Z" fill="white"/>
                <path d="M74.4385 79.1957C77.3283 80.338 96.2138 89.9027 102.225 93.4625C108.236 97.0222 111.87 124.873 108.708 147.306C103.37 185.127 103.815 199.918 109.104 217.047C109.104 217.047 97.0474 214.525 90.5423 214.003C90.5423 214.003 87.2295 188.069 91.7772 161.768C100.99 108.418 74.4385 79.1957 74.4385 79.1957Z" fill="#EAC646"/>
                <path d="M43.9567 77.5903C43.9567 77.5903 13.8238 87.5564 3.90717 98.5845C-0.106422 103.033 22.8205 157.989 20.8168 174.006C18.8131 190.024 9.47064 225.389 9.47064 225.389C9.47064 225.389 23.7714 244.077 73.8055 232.947C73.8055 232.947 69.9864 159.082 65.9358 137.056C60.0605 105.108 43.9567 77.5903 43.9567 77.5903Z" fill="#EAC646"/>
                <path d="M107.674 128.615C106.294 138.381 104.985 161.342 105.779 168.838" stroke="#D6B03F" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                <path d="M10.5363 93.0457C3.90767 98.5722 -1.875 98.1276 0.573294 122.601C3.02159 147.075 20.8173 175.67 20.8173 175.67L43.9572 152.858C43.9572 152.858 32.0183 108.526 29.7182 103.021C26.3344 94.9043 17.1649 87.5317 10.5363 93.0457Z" fill="#EAC646"/>
                <path d="M44.2786 152.858C44.2786 152.858 36.9214 117.254 31.96 109.761" stroke="#D6B03F" stroke-width="6" stroke-miterlimit="10" stroke-linecap="round"/>
                <path d="M52.932 157.541C52.932 157.541 45.5222 149.489 43.1789 149.394C40.8356 149.298 24.9634 160.666 21.5672 172.302C20.5638 175.747 15.022 181.586 36.9331 187.208C49.119 190.341 52.932 157.541 52.932 157.541Z" fill="#D6B03F"/>
                <path d="M147.292 172.435C147.292 172.435 146.156 174.022 144.498 172.932C143.464 172.253 141.59 170.462 140.176 169.576C141.441 171.574 142.605 173.635 143.661 175.751C141.832 176.56 139.863 177.011 137.863 177.078C136.209 177.149 133.668 174.299 131.121 173.015C129.061 171.981 125.301 172.595 121.269 173.509C117.237 174.423 112.893 168.455 112.893 168.455V168.421C105.242 170.999 56.1033 184.784 37.8137 183.821C33.7105 183.605 44.646 159.19 51.6884 157.294C57.6347 155.692 96.5604 156.102 115.356 158.77C122.609 159.588 133.646 162.555 135.715 163.024C137.601 163.281 141.945 166.374 143.152 167.374C144.412 168.421 147.292 172.435 147.292 172.435Z" fill="#FFCFC9"/>
                <path d="M141.565 175.809C141.565 175.809 136.452 168.47 135.044 167.967C132.574 167.078 129.335 165.902 129.335 165.902" stroke="#F7B3AF" stroke-width="4" stroke-miterlimit="10" stroke-linecap="round"/>
                <path d="M139.116 176.501C139.116 176.501 134.485 170.382 133.096 169.878C130.626 168.989 127.384 167.813 127.384 167.813" stroke="#F7B3AF" stroke-width="4" stroke-miterlimit="10" stroke-linecap="round"/>
                <path d="M74.9569 75.2099L67.9763 94.8395C67.9763 94.8395 48.9951 96.9296 44.0337 77.5193L50.721 58.3775L74.9569 75.2099Z" fill="#FFCFC9"/>
                <path d="M74.96 75.2099L71.8047 84.0769C67.6059 85.988 62.2338 85.9293 56.9853 78.9549C52.2276 72.6196 51.2026 63.3235 50.3042 59.8749L50.7086 58.3899L74.96 75.2099Z" fill="#FFC0B7"/>
                <path d="M50.721 58.3898C50.721 58.3898 56.5438 71.7613 63.6602 73.9194C70.4524 75.9756 78.7606 75.4291 79.6219 73.9194C80.4833 72.4096 87.5318 54.5862 85.6609 42.8109C84.3672 34.6633 80.9155 33.5488 74.4444 34.1817C67.9732 34.8146 65.6978 35.5278 57.788 33.1042C55.5681 32.4249 59.3811 44.8794 52.6969 49.6248C52.6969 49.6248 47.683 47.939 46.6024 49.6649C45.5218 51.3907 50.721 58.3898 50.721 58.3898Z" fill="#FFCFC9"/>
                <path d="M49.8133 60.8567C49.8133 60.8567 32.7246 50.4152 33.5521 36.2811C33.9133 30.2299 46.729 13.9594 54.5802 13.2863C62.4314 12.6133 74.5463 16.6515 78.3531 20.0168C82.1598 23.382 87.3713 27.0005 86.0128 39.8872C85.936 40.5914 85.7679 41.2826 85.5127 41.9434C84.111 34.5831 80.6809 33.5581 74.4475 34.1817C67.964 34.8301 65.6979 35.5247 57.7757 33.1042C55.5558 32.4219 59.3718 44.8795 52.6846 49.6248C52.6846 49.6248 47.6707 47.9391 46.5932 49.6618C45.5157 51.3846 50.7086 58.3868 50.7086 58.3868L49.8133 60.8567Z" fill="#383C4D"/>
                <path d="M48.4981 15.9476C48.4981 15.9476 50.0943 3.42208 41.7923 3.86358C33.4904 4.30507 28.4425 12.1655 27.6614 17.2999C26.6611 23.8976 38.637 25.8426 38.637 25.8426L48.4981 15.9476Z" fill="#383C4D"/>
                <path d="M30.4956 25.7408C33.3082 29.0628 38.637 25.8427 38.637 25.8427" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
                <path d="M50.7889 15.4629C50.7889 15.4629 52.6197 1.55114 43.0951 2.01116C33.5706 2.47118 27.3711 13.8636 28.6585 17.3925C29.946 20.9214 39.4644 25.8427 39.4644 25.8427L50.7889 15.4629Z" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
                <path d="M80.8877 34.1199C79.1989 33.4468 77.01 35.0461 74.4444 35.6636C66.1084 37.6704 60.3844 36.5898 57.2939 32.6102" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
                <path d="M67.0102 58.0533C70.5994 58.0533 73.5091 55.1436 73.5091 51.5544C73.5091 47.9651 70.5994 45.0554 67.0102 45.0554C63.4209 45.0554 60.5112 47.9651 60.5112 51.5544C60.5112 55.1436 63.4209 58.0533 67.0102 58.0533Z" stroke="#383C4D" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M83.7776 58.0533C87.1384 58.0533 89.8628 55.1436 89.8628 51.5544C89.8628 47.9651 87.1384 45.0554 83.7776 45.0554C80.4168 45.0554 77.6924 47.9651 77.6924 51.5544C77.6924 55.1436 80.4168 58.0533 83.7776 58.0533Z" stroke="#383C4D" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M73.4634 51.1067H77.4368" stroke="#383C4D" stroke-width="5" stroke-miterlimit="10"/>
                <path d="M213.216 402L241.003 190.082H246.01L273.797 402" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
                <path d="M262.263 180.564H248.37L246.208 177.513C244.094 174.515 240.179 172.669 235.94 172.669H215.542C211.303 172.669 207.388 174.522 205.273 177.513L203.112 180.564H189.759L195.097 173.852C197.483 170.851 201.889 169.005 206.668 169.005H245.35C250.126 169.005 254.541 170.857 256.925 173.852L262.263 180.564Z" fill="#D1D0FF" stroke="#D8BAA3" stroke-width="6" stroke-linejoin="round"/>
                <path d="M262.519 180.564H256.884L254.723 177.513C252.605 174.515 248.69 172.669 244.454 172.669H215.807C211.571 172.669 207.656 174.522 205.538 177.513L203.377 180.564H190.015L195.353 173.852C197.739 170.851 202.145 169.005 206.927 169.005H245.609C250.385 169.005 254.797 170.857 257.181 173.852L262.519 180.564Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10"/>
                <path d="M270.318 150.097H139.524C136.051 150.097 133.547 146.646 134.51 143.185L154.019 73.2123C154.501 71.3799 155.571 69.7565 157.066 68.5917C158.56 67.427 160.396 66.7855 162.29 66.7659H293.084C296.558 66.7659 299.065 70.2176 298.098 73.6754L278.589 143.651C278.105 145.482 277.034 147.104 275.54 148.268C274.046 149.433 272.212 150.075 270.318 150.097Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10"/>
                <path d="M194.173 128.804L190.873 149.045H202.296L197.242 128.77C197.131 128.316 194.248 128.344 194.173 128.804Z" fill="#F7F7F7"/>
                <path d="M240.608 168.791H208.19L200.185 136.621C199.302 133.086 198.159 131.709 195.424 130.931V129.078H222.939C225.159 129.079 227.315 129.821 229.064 131.188C230.813 132.555 232.056 134.467 232.593 136.621L240.608 168.791Z" fill="#D1D0FF" stroke="#D8BAA3" stroke-width="6" stroke-linejoin="round"/>
                <path d="M243.825 168.791H211.407L203.401 136.621C202.296 132.191 200.79 129.078 196.22 129.078H226.168C228.385 129.082 230.538 129.827 232.285 131.193C234.031 132.559 235.272 134.47 235.81 136.621L243.825 168.791Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-miterlimit="10"/>
                <path d="M159.975 175.037L116.884 177.51V180.564H159.975V175.037Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-linejoin="round"/>
                <path d="M186.921 175.037H159.975V180.446H186.921V175.037Z" fill="#D1D0FF" stroke="#D8BAA3" stroke-width="6" stroke-linejoin="round"/>
                <path d="M70.4496 189.625H268.28V180.564H70.002L70.4496 189.625Z" fill="white" stroke="#D8BAA3" stroke-width="6" stroke-linejoin="round"/>
            </svg>
            </div>
            <div className="selection-warning">
            You currently have no saved<br/>questions.
            </div>
        </div>
    )
    }
}


export function SavedAnswers ({show=[2,true],orderBy="upvoteLength",desc="desc"}){
    const array = Array.from(Array(2).keys())

    const fire = useContext(FirebaseContext);
    const [name, setName]= useState([]);
    const [answerCards, setAnswerCards]=useState([]);
    const [loading,setLoading]=useState(false);
    const [counter, setCounter]=useState(show[0]);

    const [pict,setPict] = useState([bag,book,headphones,pc]);



    useEffect(()=>{
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        docQuery.onSnapshot(querySnapshot => { 
            setName(querySnapshot.data());
        }
        )
        }
    
    },[fire.status]);

    //getanswers
    useEffect(()=>{
        if (fire.status){
        setLoading(true);
        
        const unsubscribe=fire.store.collectionGroup("answers").where("user","==",fire.auth.currentUser.uid).orderBy(orderBy,desc).limit(counter).get().then(querySnapshot=>{
            let y=[];
            querySnapshot.forEach(queryDocumentSnapshot=>{		
                y.push({id:queryDocumentSnapshot.id,content:queryDocumentSnapshot.data()});
            })
            setAnswerCards(y);
        })

        setLoading(false);
        }

    },[fire.status, fire.store, fire.auth.currentUser,counter,orderBy,desc])

    console.log(counter)

    const answers = answerCards?answerCards.map((element,i)=>
        <div key={i} className="savedquestions-wrapper">
            <Link to={`/forum/${element.content.subjectCode}/${element.content.questionId}`} style={{textDecoration:"none", color:"inherit"}}>
            <div className="savedquestions-subject">{element.content.subjectCode}</div>
            <div className="savedquestions-question"><Latex>{element.content.question}</Latex></div>
            <div className="savedanswers-wrapper">
                <div className="savedanswers-subwrapper">
                    <img className="my-answers-prof-pic" src={pict[name.pic]} width="32" height="32"/>
                    <div className="savedanswers-name">My Answer</div>
                </div>
                <div className="savedanswers-content"><Latex>{element.content.answer}</Latex></div>
            </div>
            <div className="savedquestions-menu-wrapper">
                <div className="savedquestions-menu-like">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.74561 21H5.74561V9H1.74561V21ZM23.7456 10C23.7456 8.9 22.8456 8 21.7456 8H15.4356L16.3856 3.43L16.4156 3.11C16.4156 2.7 16.2456 2.32 15.9756 2.05L14.9156 1L8.33561 7.59C7.96561 7.95 7.74561 8.45 7.74561 9V19C7.74561 20.1 8.64561 21 9.74561 21H18.7456C19.5756 21 20.2856 20.5 20.5856 19.78L23.6056 12.73C23.6956 12.5 23.7456 12.26 23.7456 12V10Z" 
                        fill={(element.content.likers?element.content.likers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                    <div className="savedquestions-menu-like-number">{element.content.upvoteLength}</div>
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.7456 3H6.74561C5.91561 3 5.20561 3.5 4.90561 4.22L1.88561 11.27C1.79561 11.5 1.74561 11.74 1.74561 12V14C1.74561 15.1 2.64561 16 3.74561 16H10.0556L9.10561 20.57L9.07561 20.89C9.07561 21.3 9.24561 21.68 9.51561 21.95L10.5756 23L17.1656 16.41C17.5256 16.05 17.7456 15.55 17.7456 15V5C17.7456 3.9 16.8456 3 15.7456 3ZM19.7456 3V15H23.7456V3H19.7456Z" 
                        fill={(element.content.dislikers?element.content.dislikers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                </div>
                <div className="savedquestions-menu-timestamp">{element.content.timestamp?(new Date(element.content.timestamp.seconds*1000)).toLocaleString():"Loading Timestamp"}</div>
{/*                 <div className="savedquestions-menu-comments">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20.1685 2H4.16846C3.06846 2 2.16846 2.9 2.16846 4V16C2.16846 17.1 3.06846 18 4.16846 18H18.1685L22.1685 22V4C22.1685 2.9 21.2685 2 20.1685 2ZM18.1685 14H6.16846V12H18.1685V14ZM18.1685 11H6.16846V9H18.1685V11ZM18.1685 8H6.16846V6H18.1685V8Z" fill="black"/>
                    </svg>
                    <div className="savedquestions-menu-comments-number">21</div>
                </div> */}
            </div>
            </Link>
        </div>
    ):<div></div>


    if (answerCards.length){
        return(
            <>
            <div className="dashboard-content-savedquestions">
                {answers}
            {show[1]?<Link to="/dashboard/answers" style={{ textDecoration: 'none', color:"#161716"}}>
            <div className="dashboard-content-savedquestions-viewall-wrapper">
                <div className="dashboard-content-savedquestions-viewall">View all</div>
                <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.355337 2.08032L5.06909 6.79408L0.355336 11.5078C0.127846 11.7348 1.82833e-08 12.043 1.44511e-08 12.3643C1.0619e-08 12.6857 0.127846 12.9938 0.355336 13.2208C0.829141 13.6946 1.59452 13.6946 2.06832 13.2208L7.64465 7.6445C8.11845 7.17069 8.11845 6.40531 7.64465 5.93151L2.06832 0.355186C1.59452 -0.118619 0.829142 -0.118619 0.355337 0.355186C-0.10632 0.828991 -0.118469 1.60652 0.355337 2.08032Z" fill="#EAC646"/>
                </svg>
            </div>
            </Link>:<div role="button" onClick={()=>setCounter(y=>y+5)} style={{display:`${answerCards.length>5&&answerCards.length%5?"none":"flex"}`}} className="dashboard-content-savedquestions-viewall">See More</div>
}
            </div>
            </>
    )
    }

    else{    
    return(
    <div className="dashboard-content-body">
        <div>
            <svg width="280" height="404" viewBox="0 0 280 404" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M17.8511 184.841H142.999C143.433 184.841 143.85 185.014 144.158 185.322C144.465 185.629 144.638 186.046 144.638 186.481V196.125C144.638 196.56 144.465 196.977 144.158 197.285C143.85 197.592 143.433 197.765 142.999 197.765H77.8872V220.999" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M32.619 11.2877L26.3909 56.908C24.8263 68.3256 26.5897 79.9522 31.4694 90.3923L40.573 109.794C46.6217 122.686 57.0638 132.756 69.8498 138.03L97.3366 149.369L32.619 11.2877Z" fill="white" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M53.3622 184.886H16.0321C7.54976 184.886 1.33129 176.561 3.39984 167.963L20.6912 109.451C22.2922 102.784 28.0368 98.1125 34.6203 98.1125" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M99.7093 151.943C99.3545 151.94 99.008 151.836 98.7106 151.642C98.4133 151.449 98.1776 151.174 98.0314 150.851L32.1322 10.1893C31.5238 8.89249 32.43 7.3811 33.8101 7.3811C34.1669 7.38218 34.5158 7.48564 34.8155 7.6792C35.1152 7.87276 35.3531 8.14827 35.5008 8.47302L101.387 149.135C101.996 150.432 101.089 151.943 99.7093 151.943Z" fill="white" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M58.5755 184.063C61.2371 184.063 63.3947 181.906 63.3947 179.244C63.3947 176.583 61.2371 174.425 58.5755 174.425C55.914 174.425 53.7563 176.583 53.7563 179.244C53.7563 181.906 55.914 184.063 58.5755 184.063Z" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M53.7718 179.891C53.6854 176.951 55.6226 174.511 58.575 174.425L59.0425 174.409C60.2955 174.373 61.548 174.496 62.7697 174.777L101.762 183.801" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M277.515 136.281L246.561 250.34C244.991 256.131 241.56 261.244 236.795 264.891C232.03 268.537 226.197 270.513 220.198 270.513H115.348" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M123.824 270.501L208.34 398.719" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M216.346 270.501L124.145 400" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M125.214 354.962C125.214 354.962 120.818 361.687 113.184 360.745L108.022 353.358C113.347 352.213 118.557 350.591 123.591 348.51C124.116 350.966 124.663 353.217 125.214 354.962Z" fill="#FFCFC9"/>
        <path d="M234.031 237.676C230.938 244.602 220.633 262.598 196.931 263.344C167.549 264.269 113.491 261.496 113.491 261.496C113.491 261.496 125.854 310.348 126.12 347.367C126.12 347.367 116.376 352.311 101.912 354.482C101.912 354.482 50.0383 257.459 60.3395 233.737C68.0053 216.091 126.271 209.229 152.608 207.013C163.553 213.359 199.97 233.247 234.031 237.676Z" fill="#144A64"/>
        <path d="M125.214 354.962C125.214 354.962 130.555 360.835 136.45 365.497C140.183 368.45 138.717 374.143 135.537 378.927C130.756 386.101 125.449 392.911 119.661 399.302C119.661 399.302 111.39 400.176 95.2933 399.516C95.2933 399.516 94.6913 393.804 100.381 391.223C113.35 385.341 116.27 369.769 113.19 360.761C113.183 360.745 119.661 361.901 125.214 354.962Z" fill="#144A64"/>
        <path d="M138.989 367.166C140.539 370.454 138.233 375.398 135.858 378.927C135.858 378.927 127.026 391.239 120.302 399.606L94.9731 399.821" stroke="#D8BAA3" stroke-width="7" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M111.544 386.919C111.544 386.919 105.7 383.096 109.943 380.759" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M114.746 383.397C114.746 383.397 107.621 379.574 111.864 377.236" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M201.171 76.4376C201.171 76.4376 228.004 69.044 229.903 47.0872C230.227 43.3695 201.171 76.4376 201.171 76.4376Z" fill="#D6B03F"/>
        <path d="M201.494 33.5231C201.494 33.5231 213.076 57.7405 223.733 66.7801C223.733 66.7801 206.72 78.0931 192.439 78.0867C192.439 78.0867 183.088 61.6215 179.214 55.323L201.494 33.5231Z" fill="#FFCFC9"/>
        <path d="M187.972 70.2704C185.016 65.1246 181.311 58.7428 179.214 55.323L196.166 38.7457L197.328 41.0608C198.564 52.1593 195.468 64.2696 187.972 70.2704Z" fill="#FFC0B7"/>
        <path d="M197.776 38.3646C197.776 38.3646 197.725 47.6507 194.193 52.7901C189.749 59.2615 178.436 66.2389 169.825 64.9581C161.215 63.6772 153.706 20.0679 160.164 17.487C166.623 14.9061 184.667 12.5365 190.046 20.4937C195.426 28.4509 197.776 38.3646 197.776 38.3646Z" fill="#FFCFC9"/>
        <path d="M206.813 43.7474C206.813 43.7474 203.063 44.3493 197.613 41.8485C197.783 39.812 197.86 35.63 195.753 34.426C193.454 33.1132 192.141 36.818 191.734 38.2205C191.243 37.8405 190.747 37.4317 190.245 36.9941C181.28 29.2034 183.614 21.576 183.614 21.576C183.614 21.576 170.274 29.4212 157.565 28.9024C147.017 28.4733 141.852 13.7117 150.744 9.31199C150.744 9.31199 150.104 13.3562 157.866 11.419C165.628 9.4817 164.119 1.60134 174.997 0.214832C188.401 -1.47908 191.171 7.40034 191.171 7.40034C191.171 7.40034 197.591 4.02213 204.2 10.167C211.242 16.7152 202.144 34.9928 206.813 43.7474Z" fill="#383C4D"/>
        <path d="M184.984 20.1543C184.984 20.1543 172.515 30.1001 158.964 31.8644C146.216 33.5231 140.632 17.0611 149.294 11.0924" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
        <path d="M190.018 8.0631C190.018 8.0631 194.376 1.86384 202.627 5.50142" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
        <path d="M236.849 238.009C246.839 228.224 259.565 166.51 256.241 126.944C256.033 122.44 255.373 117.968 254.272 113.595C253.705 111.02 252.946 108.492 252.001 106.031C249.119 98.6667 242.529 90.9816 233.365 85.7334C224.025 79.0442 211.837 74.4172 195.686 77.1197C171.645 81.1416 166.063 105.141 164.77 116.067L164.75 116.048L158.228 152.718L110.632 154.514V170.083C110.632 170.083 149.688 193.907 177.354 187.003L177.597 186.939C164.469 199.779 149.316 205.063 149.316 205.063C149.316 205.063 195.417 233.664 236.849 238.009Z" fill="#EAC646"/>
        <path d="M200.729 136.845C200.729 136.845 191.616 182.747 174.674 188.604L200.729 136.845Z" fill="#EAC646"/>
        <path d="M200.729 136.845C200.729 136.845 191.616 182.747 174.674 188.604" stroke="white" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M201.171 76.4377C208.597 75.477 228.273 62.9664 229.887 46.498C229.887 46.498 259.471 76.4377 248.636 100.361C248.636 100.361 242.603 89.1532 229.558 82.3423C213.944 74.1994 201.171 76.4377 201.171 76.4377Z" fill="#EAC646"/>
        <path d="M251.252 101.657C251.252 101.657 242.286 89.7392 229.558 82.3423C211.738 71.9836 189.816 77.4111 189.816 77.4111" fill="#EAC646"/>
        <path d="M251.252 101.657C251.252 101.657 242.286 89.7392 229.558 82.3423C211.738 71.9836 189.816 77.4111 189.816 77.4111" stroke="white" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M110.785 164.55C110.785 164.55 106.056 172.027 101.022 170.954C95.9882 169.881 91.3035 169.145 88.7514 170.458C85.5974 172.081 82.4657 175.664 80.4068 175.597C77.9142 175.532 75.4574 174.988 73.17 173.996C74.4656 171.355 75.8934 168.782 77.448 166.285C75.7029 167.39 73.3878 169.657 72.1069 170.515C70.0576 171.892 68.623 169.932 68.623 169.932C68.623 169.932 72.1646 164.902 73.7144 163.592C75.2034 162.334 80.5765 158.431 82.9204 158.085C85.7351 157.415 99.6226 155.382 110.817 154.617L110.785 164.55Z" fill="#FFCFC9"/>
        <path d="M75.7827 174.066C75.7827 174.066 82.062 164.87 83.8072 164.223C86.8812 163.086 90.8902 161.585 90.8902 161.585" stroke="#F7B3AF" stroke-width="4" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M78.8408 174.899C78.8408 174.899 84.5149 167.214 86.2569 166.573C89.3309 165.437 93.3431 163.932 93.3431 163.932" stroke="#F7B3AF" stroke-width="4" stroke-miterlimit="10" stroke-linecap="round"/>
        </svg>
        </div>
        <div className="selection-warning">
        You have not answered a question<br/>from the forum yet.
        </div>
    </div>
   
    )
    }
}