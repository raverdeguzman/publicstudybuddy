import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import Main from './components/MainComponent';
import FirebaseContextProvider from './contexts/FirebaseContext';
import ClickContextProvider from './contexts/ClickContext';
import FilterContextProvider from './contexts/FilterContext';

import {useEffect,useContext, useState} from 'react';


import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';


function App() {
  


const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#1bb3f4",
      
    },
    secondary: {
      main: "#ff6602",
    },
  },
});

//console.log("Theme",theme.palette)


  return (
    <div>
      {console.log("App Rendered. This message fires on every render")}
      <FirebaseContextProvider>
        <ClickContextProvider>

            <ThemeProvider theme={theme}>
        <BrowserRouter>
            <Main className="hide"/>
        </BrowserRouter>
            </ThemeProvider>

        </ClickContextProvider>
      </FirebaseContextProvider>
      
    </div>
  );
}

export default App;
