  
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.addAdminRole = functions.https.onCall((data, context) => {
  // get user and add admin custom claim
  return admin.auth().getUserByEmail(data.email).then(user => {
    return admin.auth().setCustomUserClaims(user.uid, {
      admin: true
    })
  }).then(() => {
    return {
      message: `Success! ${data.email} has been made an admin.`
    }
  }).catch(err => {
    return err;
  });
});

exports.addModeratorRole = functions.https.onCall((data, context) => {
  // get user and add admin custom claim
  return admin.auth().getUserByEmail(data.email).then(user => {
    return admin.auth().setCustomUserClaims(user.uid, {
      moderator: true
    })
  }).then(() => {
    return {
      message: `Success! ${data.email} has been made a moderator.`
    }
  }).catch(err => {
    return err;
  });
});

exports.addTutorRole = functions.https.onCall((data, context) => {
  // get user and add admin custom claim
  return admin.auth().getUserByEmail(data.email).then(user => {
    return admin.auth().setCustomUserClaims(user.uid, {
      tutor: true,
      org: data.org
    })
  }).then(() => {
    return {
      message: `Success! ${data.email} has been made a tutor from ${data.org}.`
    }
  }).catch(err => {
    return err;
  });
});

//auth trigger (new user signup)
exports.newUserSignup = functions.auth.user().onCreate(user=>{
  return admin.firestore().collection('users').doc(user.uid).set({
    email: user.email,
    uid: user.uid,
    pic:1,
    upvotes:0,
    uSubjects:[],
    uQuestions:[],
    uAnswers:[]
  });

});

exports.newUserSignupTwo = functions.auth.user().onCreate(user=>{
  return admin.firestore().collection('stats').doc("number of users").update({
    number: firebase.firestore.FieldValue.increment(1)
  });

});

//auth trigger (new user deleted)
exports.userDeleted = functions.auth.user().onDelete(user=>{
  const doc = admin.firestore().collection('users').doc(user.uid);
  return doc.delete();
});

/* //onrequest 1

exports.randomNumber = functions.https.onRequest((request, response)=>{
    const number = Math.round(Math.random()*100);
    console.log("Random Number:",number);
    response.send(number.toString())
});

//onrequest 2
exports.toTheDojo = functions.https.onRequest((request, response)=>{
  response.redirect('https://www.thenetninja.co.uk')
});


//http callable function

exports.sayHello = functions.https.onCall((data,context)=>{
  const name = data.name;
  return `hello, ninjas. the name's ${name}`;
}); */